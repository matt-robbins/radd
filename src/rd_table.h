#ifndef RD_TABLE_H
#define RD_TABLE_H

enum table_error {
    TABLE_FULL = -1,
    TABLE_ENTRY_EXISTS = -2
};
    
typedef struct node {
    char * name;
    void * data;
    void (* free)(void *);
} rd_table_node_t;

typedef struct table {
    int N;
    unsigned int mask;
    rd_table_node_t * values;
} rd_table_t;

rd_table_t * table_create(int N);

int table_insert(rd_table_t * t, char * name, void * data, void (*data_free)(void *));
int table_replace(rd_table_t * t, char * name, void * data, void (*data_free)(void *));
int table_insert_str(rd_table_t * t, char * name, char * data);
int table_insert_strp(rd_table_t * t, char * name, char * data);

int table_remove(rd_table_t * t, char * name);

void ** table_getp(rd_table_t * t, char * name);
void * table_get(rd_table_t * t, char * name);

char * table_get_str(rd_table_t * t, char * name);
char * table_get_strp(rd_table_t * t, char * name);

int table_get_all(rd_table_t * t, char *** names, void *** values);

void table_free(rd_table_t * t);

#endif
