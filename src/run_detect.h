/**\file
 * This file contains headers and types for running detector processes.
 */

#include <pthread.h>
#include <signal.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include "detector.h"
#include "classifier.h"

/**
 * an argument to rd_jack_client to determine whether to start or stop (cleanup)
 * the client
 */

typedef enum {
    RD_CLIENT_START,
    RD_CLIENT_STOP,
} rd_client_task_t;

/**
 * all of the information necessary for the detector thread to run its detector
 * on the input buffer.
 */

typedef struct {
    detector_t * detector;
    classifier_t * classifier;
    sound_buffer_t * sound_buffer;
    event_buffer_t * event_buffer;
    config_t * config;
    int xruns;
    rd_time_t timestamp;
    int engine_sample_rate;
    int desired_sample_rate;
    pthread_mutex_t sb_lock;
    pthread_cond_t sb_ready;
    pthread_mutex_t eb_lock;
    pthread_cond_t eb_ready;
    pthread_t main_tid;
    int processing;
} detector_thread_info_t;

void detector_handler(int sig);

void detector_stop(int sig);
int stop_detector(char * det_name);
int restart_detector(char * det_name);
int launch_detector(char * det_name, int fork);



