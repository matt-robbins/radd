#include "rd_memory.h"
#include "rd_log.h"

#include <stdarg.h>

void rd_oom(void)
{
	rd_log_message("*** out of memory! ***\n");
	exit(1);
}

void rd_asprintf(char **strp, const char * fmt,  ...) {
    va_list args;
    va_start(args, fmt);
    int n = vasprintf(strp, fmt, args);
    va_end(args);
    
    if (n < 0)
		rd_oom();
		
	return;
}

void *rd_malloc(size_t size)
{
	void * p = malloc(size);
	
	if (p == NULL && size != 0)
		rd_oom();
		
	return p;
}

void *rd_realloc(void *ptr, size_t size)
{
	void * p = realloc(ptr, size);
	
	if (p == NULL && size != 0)
		rd_oom();
		
	return p;
}

void *rd_calloc(int nmemb, size_t size)
{
	void * p = calloc(nmemb, size);
	
	if (p == NULL && size != 0 && nmemb != 0)
		rd_oom();
		
	return p;
}

char * rd_strdup(const char * s)
{
	char * p;
	
	if (!s)
		return NULL;
		
	p = strdup(s);
	
	if (p == NULL)
		rd_oom();
	
	return p;
}
