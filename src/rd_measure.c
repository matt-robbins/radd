/**\file
 * main program for measuring audio, using the run_detect 
 * resampling audio driver
 */

#include "rd_util.h"
#include "rd_log.h"
#include "rd_error.h"
#include "rd_db.h"
#include "rd_process.h"
#include "rd_audio_driver.h"
#include "configure.h"
#include "config.h"
#include "defaults.h"
#include "rd_memory.h"


#if HAVE_LIBFPC==1
#include <fpc3.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <stdarg.h>
#include <sys/wait.h>
#include <syslog.h>
#include <time.h>
#include <sndfile.h>
#include <math.h>

#define xstr( s ) stringize(s)
#define stringize( x ) #x

#define BUFLEN 1024
#define SRATE 8000
#define DEFAULT_PATH /tmp
#define DEFAULT_NAME record

/**
 * signal handlers
 */
 
int Done = 0; 
float Fpgain = 0.0;
float Dbdf = 0.0;

static void shutdown_handler(int sig)
{
    rd_log_message("recieved signal %d, shutting down.\n", sig);
    Done = 1;
    exit(0);
}

typedef struct frac_node_s {
    fpc_prod_t data;
    struct frac_node_s * next;
    struct frac_node_s * prev;
} frac_node_t;

typedef struct spec_avg_s {
    frac_node_t ** s;
    fpc_prod_t * out;
    fpc_prod_t mean_sq;
    float * out_db;
    int Nta;
    int N;
    int K;
    int ix;
    int fs;
} spec_avg_t;

typedef struct psd_s {
    void * fft_state;
    int N;
    fpc_frac_t * in;
    fpc_frac_t * out;
    fpc_cpx_t * fout;
    fpc_frac_t * h;
    fpc_frac_t hfactor;
} psd_t;

typedef struct spec_thresh_s {
    int flo;
    int fhi;
    fpc_prod_t thresh;
} spec_thresh_t;

spec_thresh_t * spec_thresh_create(float flo, float fhi, float df, fpc_prod_t thresh)
{
    spec_thresh_t * st = rd_calloc(1, sizeof(spec_thresh_t));
    
    st->flo = (int) (flo / df);
    st->fhi = (int) (fhi / df);
    st->thresh = thresh;
    
    return st;
}

spec_thresh_t * spec_thresh_parse(char * arg, float df, float db_gain)
{
    float flo, fhi, thresh;
    if (arg == NULL)
        return NULL;
    
    int n = sscanf((char *)arg, "%f:%f:%f", &flo, &fhi, &thresh);
    
    if (n != 3)
        return NULL;
    
    fprintf(stderr, "creating threshold object: %f:%f, %fdB\n", flo, fhi, thresh);
    
    /*
    return spec_thresh_create(
        flo, fhi, df, (fpc_prod_t) exp10f((db_gain + thresh)/10));
        */
        
    return spec_thresh_create(
        flo, fhi, df, (fpc_prod_t) 1000000000);
        
}

int spec_thresh_test(spec_thresh_t * st, spec_avg_t * state)
{
    int k;
    prod_t sum = 0;
    if (st == NULL)
        return 0;
    
    for (k = st->flo; k < st->fhi; k++)
    {
        sum += state->out[k];
    }
    
    sum /= (state->K * (st->fhi - st->flo + 1));
    
    return st->thresh > 0 && sum > st->thresh;
}

psd_t * psd_init(int N, fpc_window_t type)
{
    int k;
    prod_t sumsq = 0ll;
    psd_t * p = rd_calloc(1, sizeof(psd_t));
    p->fft_state = kiss_fftr_alloc(N, 0, 0, 0);
    p->h = fpc_freq_window(N, type);
    p->in = rd_calloc(N, sizeof(fpc_frac_t));
    p->fout = rd_calloc(N/2 + 1, sizeof(fpc_cpx_t));
    p->N = N;
    
    for (k = 0; k < N; k++)
    {
        sumsq += frac_mul(p->h[k], p->h[k]);
    }
    
    p->hfactor = sumsq / N;
    return p;
}

int psd_compute(psd_t * p, fpc_frac_t * out, fpc_frac_t * in)
{
    int k;
        
    vec_mul(p->in, in, p->h, p->N);
    kiss_fftr(p->fft_state, p->in, p->fout);
    vec_mag_sq(out, p->fout, p->N/2 + 1);
    
    for (k = 0; k < p->N/2 + 1; k++)
    {
        if (k != 0 && k != p->N/2)
            out[k] *= 2;
        
        out[k] = frac_div(out[k], p->hfactor);
    }
        
    return p->N/2 + 1;
}

spec_avg_t * spec_avg_init(int N, int K, int fs)
{
    int j, k;
    spec_avg_t * state = rd_malloc(sizeof(spec_avg_t));
    
    state->s = rd_malloc(N * sizeof(frac_node_t *));
    state->out = rd_calloc(N, sizeof(fpc_prod_t));
    
    for (j = 0; j < N; j++)
    {
        state->s[j] = rd_malloc(K * sizeof(frac_node_t));
        
        for (k = 0; k < K; k++)
        {
            state->s[j][k].data = 0;
            state->s[j][k].next = &state->s[j][k + 1];
            state->s[j][k].prev = &state->s[j][k - 1];
        }
        
        state->s[j][0].prev = &state->s[j][K-1];
        state->s[j][K-1].next = &state->s[j][0];
        state->ix = 0;
    }
    
    state->N = N;
    state->K = K;
    state->fs = fs;
    
    return state;
}

int spec_avg_update(spec_avg_t * state, fpc_frac_t * spec, int N)
{
    int k;
    
    if (state->N != N)
        return -1;
    
    if (state->K == 1)
    {
        for (k = 0; k < state->N; k++)
        {
            state->out[k] = spec[k];
        }
        
        return N;
    }
    
    for (k = 0; k < state->N; k++)
    {
        state->s[k]->data = spec[k];
        
        state->out[k] += (state->s[k]->data - state->s[k]->next->data);
        state->s[k] = state->s[k]->next;
    }
    
    return N;
}


int spec_avg_print(char ** out, spec_avg_t * state, float df, int rl, int rh)
{
    int k;
    char * p;
    float value, dbdf;
    
    if (rl < 0)
        rl = 0;
    if (rh < 0)
        rh = state->N;
    
    if (*out == NULL)
        *out = rd_calloc(10 * (rh - rl), sizeof(char));
    
    dbdf = 10*log10f(df);
    
    p = *out;
    for (k = rl; k < rh; k++)
    {
        if (state->out[k] == 0)
            value = -500.0;
        else
            value = 10*log10f((float) state->out[k] / state->K) - Fpgain - dbdf; 
        p += sprintf(p, "%2.3f ", value); 
    }
    *p = '\0';
        
    return 0;
}

/**
 * put a file in the filesystem to tell the reporting process to go now
 */

int notify(float score)
{
    FILE * fp = fopen(DEFAULT_REPORT_FLAG, "w");
    
    if (fp == NULL)
        return RD_ERROR_FILE;
    
    fclose(fp);
    
    return RD_ERROR_NO_ERROR;
}

int spec_avg_store(spec_avg_t * state, float df)
{
    int status;
    char * spectrum_str = NULL;
    rd_db_t * db;
    rd_db_stmt_t * stmt;
    
    spec_avg_print(&spectrum_str, state, df, -1, -1);
    fprintf(stdout, "%s\n", spectrum_str);
    
    //free(spectrum_str);
    //return 0;
    
    db = rd_db_create("/tmp/save/dump.db");
    
    status = rd_db_exec(db, 
        "CREATE TABLE IF NOT EXISTS spectra(\
        id INTEGER NOT NULL PRIMARY KEY, time DATETIME, units TEXT, \
        format TEXT, df REAL, spectrum TEXT);",
        0, 0, 0
    );
    
    if (status != SQLITE_OK)
        fprintf(stderr,"problem creating table!\n");
    
    stmt = rd_db_add_stmt(db, "insert", "row",
        "INSERT INTO spectra VALUES \
        (NULL, datetime('now'), 'dB Re:1 V^2/Hz','ssv',?,?);");
    
    rd_db_stmt_bind(
        stmt, 1, 2, 
        RD_TYPE_DBL, (double) df,
        RD_TYPE_STR, spectrum_str
    );
    
    rd_db_stmt_exec(stmt, NULL);
    rd_db_free(db);
    
    free(spectrum_str);
    return 0;
}

/**
 * process one buffer
 */

void process_one_buffer(
    psd_t * psd, spec_avg_t * state, fpc_frac_t * buf, fpc_frac_t * out,
    int buflen, int sample_rate, spec_thresh_t * st, int * n, int N)
{
    psd_compute(psd, out, buf);
    spec_avg_update(state, out, buflen/2 + 1);
    
    if (++*n >= N)
    {
        fprintf(stderr, ".");
        
        if (st && spec_thresh_test(st, state))
        {
            rd_log_message("noise is above threshold, triggering report\n");
            notify(0.0);
        }
    
        spec_avg_store(state, (float) sample_rate / buflen);
        
        *n = 0;
    }

    return;
}


/**
 */

int main(int argc, char * argv[])
{
    rd_audio_driver_t * driver;
    char * atype = "jack";
    char * thresharg = NULL;
    sound_buffer_t * buffer;
    rd_time_t t;
    sample_t * buf;
    int c, daemon = 1, channel = 0;
    int sample_rate = SRATE;
    int buflen = BUFLEN;
    psd_t * psd;
    fpc_frac_t * out;
    spec_avg_t * state;
    spec_thresh_t * st = NULL;
    rd_time_t interval = 60*1000000ll;
    float window = 60.0, df;
    int navg, nint, n, space;
        
    char * usage = 
    "Usage: rd_measure [-d] [-a driver type] [-c channel] [-r sample rate] [-t storage interval (seconds)]\n"
    "   [-n fft/buffer length] [-w averaging window length (seconds)]\n";
        
    /*
     * handle input
     */
    
    opterr = 0;
     
    while ((c = getopt (argc, argv, "hvdLc:r:t:T:m:p:n:w:a:g:")) != -1)
    {
        switch (c)
        {
            case 'v':
                puts(PACKAGE_VERSION); 
                return 0;
            case 'd':
                daemon = 0;
                break;
            case 'c':
                channel = atoi(optarg);
                break;
            case 'r':
                sample_rate = atoi(optarg);
                break;
            case 't':
                interval = (rd_time_t) atoi(optarg) * 1000000ll;
                break;
            case 'T':
                thresharg = optarg;
                break;
            case 'n':
                buflen = atoi(optarg);
                break;
            case 'w':
                window = atof(optarg);
                break;
            case 'h':
                puts(usage); 
                return 0;
            case 'a':
                atype = optarg;
                break;
            case '?':
            default:
                puts(usage);
                return 1;
        }
    }
    
    signal(SIGTERM, shutdown_handler);
    signal(SIGINT, shutdown_handler);
    
    if (daemon)
        daemonize("rd_measure");
    
    out = rd_calloc(buflen/2 + 1, sizeof(fpc_frac_t));
    psd = psd_init(buflen, FPC_WIN_HANNING);
        
    navg = (int) (window * (float) sample_rate / buflen);
    
    if (navg < 1)
        navg = 1;
    
    nint = (int) ((float) (interval / 1000000ll) * (float) sample_rate / buflen);
    
    if (nint < 1)
        nint = 1;
    
    state = spec_avg_init(buflen/2+1, navg, sample_rate);
    
    Fpgain = 10*log10f((float) (1ll << (62 - FPC_DEFAULT_POINT_POS)));
    df = (float) sample_rate / buflen;
    
    st = spec_thresh_parse(thresharg, df, Fpgain + 10*log10f(df));
    
    fprintf(stderr, "rd_measure running: \n\
        sample rate: %d Hz\n\
        fft size: %d\n\
        averaging window: %d (%2.2f sec)\n\
        storage interval: %d windows (%f seconds)\n\
        fixed point gain (dB): %f\n",
        sample_rate,
        buflen, 
        navg, window,
        nint, (float) nint * buflen / sample_rate,
        Fpgain
    );
        
    driver = rd_audio_driver_create(
        atype, "rd_measure", channel, sample_rate, buflen, 1, 1, 5);
    
    if (driver == NULL)
    {
        rd_log_message("couldn't start audio driver!\n");
        exit(1);
    }
    
    if (driver->rate != sample_rate)
    {
        rd_log_message(
            "warning: wanted rate %d, got %d\n", 
                sample_rate, driver->rate);
        sample_rate = driver->rate;
    }
    
    if (driver->channel != channel)
    {
        rd_log_message(
            "warning: wanted channel %d, got %d\n", 
                channel, driver->channel);
        channel = driver->channel;
    }

    /*
     * start driver
     */
    
    buffer = driver->sound_buffer;
    
    rd_audio_driver_start(driver);
    
    n = 0;
        
    while (!Done)
    {    
        sound_buffer_read_wait(buffer);
        
        while ((space = sound_buffer_read_space(buffer)) > 0)
        {
            buf = sound_buffer_peek(buffer, &t);
            
            process_one_buffer(
                psd, state, buf, out, buflen, sample_rate, st,
                &n, nint);
            
            sound_buffer_increment_read_pointer(buffer);
            sound_buffer_write_signal(buffer);
        }
    }
    
    rd_audio_driver_stop(driver);
    rd_audio_driver_free(driver);
        
    free(state);
    
    return 0;
}

#else

int main(int argc, char * argv[])
{
    fprintf(stderr, "RADd built without libfpc support.  rd_measure disabled\n");
}

#endif

