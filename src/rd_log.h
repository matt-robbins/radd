#ifndef RD_LOG_H
#define RD_LOG_H

#include <stdlib.h>
#include <string.h>

#define RD_MAX_MESSAGE 4096

void rd_log_init(char * name);

void rd_log_message(char * fmt, ...);

void * log_thread(void * arg);

int rd_log_stderr(void);

#endif
