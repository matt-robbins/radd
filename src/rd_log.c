/**\file
 * Logging utilities that allow us to write to the syslog depending on 
 * whether or not we are daemonized, and from which process we're called
 */
 
#include "rd_error.h"
#include "process_table.h"
#include "rd_process.h"
#include "rd_log.h"
#include "configure.h"

#include <limits.h>
#include <stdio.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <fcntl.h>

int RunDetectDaemonized = 0;

void rd_log_init(char * name)
{
    closelog();
    openlog(name, LOG_PID | LOG_CONS | LOG_NOWAIT, LOG_USER);
}

int rd_log_stderr()
{
    int pfd[2], pid;
    FILE * fp;
    char line[LINE_MAX];
        
    if (pipe(pfd))
        return -1;
        
    if ((pid = fork()) < 0)
        return -1;
    
    if (pid == 0)
    {
        close(pfd[1]);
        fp = fdopen(pfd[0], "r");
    
        if (fp == NULL)
        {
            fprintf(stderr, "couldn't open log fd\n");
            return -1;
        }
    
        while (fgets(line, LINE_MAX, fp) != NULL) 
        {
            rd_log_message("detector: %s", line);
        }
        
        return 0;
    }
    
    close(pfd[0]);
    dup2(pfd[1], STDERR_FILENO);
    close(pfd[1]);
    
    return pid;
}


/**
 * print a message, either to stdout if we're running in the foreground
 * or to syslog if we're a daemon
 */

void rd_log_message(char * fmt, ...)
{
    va_list arglist;
    int n_written;
    char message[RD_MAX_MESSAGE];
    
    /*
     * write message to a string using the va_list stuff
     */
    
    va_start(arglist, fmt);
    n_written = vsnprintf(message, RD_MAX_MESSAGE, fmt, arglist);
    va_end(arglist);
    
    if (n_written >= RD_MAX_MESSAGE){
        rd_log_message("log message truncated!!");
    }
    
    /*
     * output to stderr or the syslog
     */
    
    if (RunDetectDaemonized)
        syslog(LOG_INFO, "%s", message);
    else
        fprintf(stderr, "%s", message);
}

/**
 * log a generic message to the syslog, prefixed with information on this pid
 */

int rd_log_error(char * message, rd_error_t err)
{
    char * error_str = rd_error_str(err);
        
    if (err == RD_ERROR_NO_ERROR)
        error_str = "";

    rd_log_message("%s: %s\n", message, error_str);
    
    return RD_ERROR_NO_ERROR;
}
