#include "rd_signal.h"
#include "configure.h"
#include "defaults.h"
#include "run_detect.h"
#include "rd_util.h"
#include "rd_log.h"

#include <stdio.h>
#include <stdlib.h>

sigset_t RunDetectSignalMask;
pthread_t sig_thread_id;

/**
 * generic thread_compatible signal handler
 */

static void * signal_thread (void *arg)
{
    int sig_caught, err;
    void (*handler)(int) = arg;

    while (1)
    {
        err = sigwait (&RunDetectSignalMask, &sig_caught);
        
        if (err != 0) {
            fprintf(stderr, "couldn't wait for signals!\n");
        }
        
        if (handler)
            handler(sig_caught);
    }
    
    return NULL;
}

/**
 * send a signal to the signal thread
 */

void rd_raise(int sig)
{
    pthread_kill(sig_thread_id, sig);
}

/**
 * set up run_detect signal handling
 */

int rd_setup_signals(void (*handler)(int))
{
    int err;
    
    /*
     * block all signals we care about, and wait for them in our signal thread
     */
    
    sigemptyset (&RunDetectSignalMask);
    
    sigaddset (&RunDetectSignalMask, SIGCHLD);
    sigaddset (&RunDetectSignalMask, SIGTERM);
    sigaddset (&RunDetectSignalMask, SIGINT);
    sigaddset (&RunDetectSignalMask, SIGHUP);
    
    err = pthread_sigmask(SIG_SETMASK, &RunDetectSignalMask, NULL);
    
    if (err != 0){
        fprintf(stderr, "couldn't set pthread sigmask!\n"); return -1;
    }
    
    err = pthread_create(&sig_thread_id, NULL, signal_thread, handler);
    
    if (err != 0){
        fprintf(stderr, "couldn't create signal-catching thread!\n"); return -1;
    }
    
    return 0;
}
