/**\file
 * This file describes the detector API for run_detect.  It needs to be #included
 * in the detector source files in order to have knowledge of event_t and others.
 */

#ifndef DETECT_H
#define DETECT_H

#include <sys/types.h>
/**
 * the maximum number of events that a detector is allowed to write during one
 * cycle.
 */

#define RD_MAX_EVENTS 128

#define RD_ABI_VERSION 1
#define RD_API_VERSION "1.3.0"
#define event_size sizeof(event_t)
#define RD_LABEL_MAX 100

#ifdef FIXED_POINT
typedef int32_t sample_t;
#else
typedef float sample_t;
#endif

typedef int offset_t;
typedef float score_t;
typedef float freq_t;

/**
 * a detected "event".  A pointer to an array of these is passed to the detector's
 * "detect" function.
 */

typedef struct { 
    offset_t t_in; 
    offset_t t_dt;
    freq_t f_lo;
    freq_t f_df;
    score_t score;
    char label[RD_LABEL_MAX];
    void * data;
} event_t;

/**
 * requested sample rate and buffer size information for run_detect.  A detector
 * may request values for these settings and thusfar, the request will be granted.
 */

typedef struct {
    int rate;
    int N;
    int buffer_history;
    int buffer_latency;
} sound_setup_t;

/**
 * Take the detector's config file and generate a set of parameters, cast to void *.
 */

typedef void * (*rd_parameter_initialize_fcn)(const char * config_file);

/** 
 * Generate an initial state using parameters that will be generated using
 * rd_parameter_initialize_fcn.
 */

typedef void * (*rd_state_initialize_fcn)(void * parameters, sound_setup_t * setup);

/**
 * run the detector on a buffer of (mono) data. Using previously defined parameters and state. 
 * @param events a pointer to an allocated buffer of events.  The detector should not
 * attempt to write more than RD_MAX_EVENTS per buffer.
 * @param buf the audio data input buffer
 * @param nbuf the length of the buffer
 * @param parameters the detector parameters, as set up by rd_parameter_initialize_fcn.
 * @param state the detector state, as set up by rd_state_initialize.
 * @param setup the sound engine parameters
 */

typedef int (*rd_detect_fcn)(
    event_t * events, sample_t * buf, int nbuf, 
    void * parameters, void * state, sound_setup_t * setup, struct timeval time
);

/**
 * run the classifier using previously defined parameters and state. 
 * @param buf the audio data
 * @param nbuf the length of the buffer
 * @param parameters the parameters, as set up by rd_parameter_initialize_fcn.
 * @param state the state, as set up by rd_state_initialize.
 * @param data a pointer to a classifier data string, to be allocated by
 *  the classification function
 * @param labels a pointer to a NULL-terminated string vector.  to be allocated by 
 *  the classification function.
 */

typedef float (*rd_classify_fcn)(
    sample_t * buf, int nbuf, int rate, void * parameters, void * state,
    char ** data, char *** labels
);

/**
 * clean up a detector by de-allocating storage and closing any open file descriptors
 */

typedef void (*rd_cleanup_fcn)(void * parameters, void * state);

/**
 * set up the sound engine for a detector based on its requirements given the parameters
 */

typedef sound_setup_t * (*rd_setup_fcn)(void * parameters);

/**
 * convert event data into a string
 */

typedef char * (*rd_data_to_str_fcn)(void * data);

/**
 * free event data
 */
 
typedef void (*rd_data_free_fcn)(void * data);

/**
 * return the API version (version of run_detect) that the detector was written for.
 */

typedef char * (*rd_api_version_fcn)(void);

/**
 * return the version of the detector.
 */

typedef char * (*rd_version_fcn)(void);


#endif
