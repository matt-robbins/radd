#ifndef RD_VECTOR_H
#define RD_VECTOR_H

typedef enum {
    RD_TYPE_INT = 0,
    RD_TYPE_DBL,
    RD_TYPE_STR,
} rd_value_type_t;

typedef struct {
    union {
        int i;
        double f;
        char * s;
    } d;
    rd_value_type_t type;
} rd_value_t;

typedef struct rd_vector {
    rd_value_t * data;
    int nrows;
    int ncols;
} rd_vector_t;

rd_vector_t * rd_vector_create(int nrows, int ncols);
void rd_vector_free(rd_vector_t * v);

int rd_vector_assign(rd_vector_t * v, int ix, int N, ...);

#endif
