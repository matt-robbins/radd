/**\file
 * run_detect native time representation as a 64-bit integer.
 */

#include "rd_time.h"
#include <stdlib.h>
#include <math.h>

/**
 * get the system time
 */

rd_time_t rd_get_time()
{
    struct timeval time;
    rd_time_t out;
    
    gettimeofday(&time, NULL);
    out = timeval_to_rd_time(time);
    
    return out;
}

/**
 * convert a run_detect time into a timeval struct
 */

struct timeval rd_time_to_timeval(rd_time_t t)
{
    struct timeval out;
    
    out.tv_sec = (int) (t / ONE_SEC_USECS_L);
    out.tv_usec = (int) (t % ONE_SEC_USECS_L);
    
    return out;
    
}

/**
 * convert a timeval struct to an rd_time
 */

rd_time_t timeval_to_rd_time(struct timeval t)
{
    rd_time_t out = (rd_time_t) t.tv_sec * ONE_SEC_USECS_L + t.tv_usec;
    return out;
}

/**
 * convert a number of samples to an rd_time_t
 */

rd_time_t rd_samples_to_time(int samples, int rate)
{
    return ((rd_time_t) samples * ONE_SEC_USECS) / rate;
}

/**
 * convert an rd_time to a number of samples given a sample rate
 */

int rd_time_to_samples(rd_time_t t, int rate)
{
    return (int) (t * (rd_time_t) rate / ONE_SEC_USECS);
}

/**
 * initialize a delay-locked loop
 */

void rd_dll_init(rd_dll_t * d, rd_time_t t, float bw)
{
    float omega;
    d->per = t;
            
    omega = (2.0 * M_PI * bw);
        
    d->b = 2 * omega;
    d->c = omega * omega;
    
    d->t1 = 0;
}

rd_time_t rd_dll_reset(rd_dll_t * d, rd_time_t t_in)
{
    d->e2 = 1.0;
    d->e = 0.0;
    d->t0 = t_in;
    d->t1 = d->t0 + d->per;
    return t_in;
}

rd_time_t rd_dll_update(rd_dll_t * d, rd_time_t t_in)
{
    if (d->t1 == 0)
        return rd_dll_reset(d, t_in);
        
    d->e = (t_in - d->t1) / d->per;
        
    d->t0 = d->t1;
    d->t1 += lrintf((d->b * d->e + d->e2) * d->per);
    d->e2 += d->c * d->e;
                    
    return d->t0;
}
