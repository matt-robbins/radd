/**\file
 * various "utility" functions for run_detect
 */

#include "rd_util.h"
#include "rd_error.h"
#include "rd_memory.h"

#include <errno.h>
#include <sys/file.h>

/**
 * check for file existence using stat()
 */

int fexist(char * filename) 
{
    struct stat buffer;
        
    if (stat(filename, &buffer))
        return 0;
    else
        return 1;
}

/**
 * create a named file
 */

int ftouch(char * filename) 
{
    FILE * fp = fopen(filename, "w");
    
    if (fp == NULL)
        return RD_ERROR_FILE;
    
    fclose(fp);
    return RD_ERROR_NO_ERROR;    
}

/**
 * get exclusive access to lock file
 */

int f_lock(char * file)
{
    int lfp;
    
    /*
     * get lock
     *
     * NOTE: lock is removed automatically on termination of this process
     */
    
    lfp = open(file,O_RDWR|O_CREAT,0640);
    
    if (lfp < 0) 
        return -1; 
        
    if (flock(lfp, LOCK_EX | LOCK_NB)){
        
        if (errno != EWOULDBLOCK)
            fprintf(stderr, "failed to get lock on '%s': %s\n", file, strerror(errno));
        
        close(lfp); 
        return -2; 
    }
    
    return lfp;
}

int f_lock_block(char * file)
{
    int lfp;
    lfp = open(file, O_RDWR|O_CREAT,0640);
    
    if (lfp < 0)
        return -1;
    
    if (flock(lfp, LOCK_EX))
    {
        fprintf(stderr, "failed to get lock on '%s': %s\n", file, strerror(errno));
        close(lfp); 
        return -2; 
    }
    
    return lfp;
}

int f_ulock(int fd)
{
    int result;
    result = flock(fd, LOCK_UN);
    
    if (result)
        perror("couldn't unlock file!\n");
    
    result = close(fd);
    
    if (result)
        perror("couldn't close file descriptor.\n");
    
    return result;
}

int next_pow2(int in)
{
    int shift = 1;
    int out = in;
    
    out--;
    
    while (shift < sizeof(in) * 4)
    {
        out = out | out >> shift;
        shift *= 2;
    }
    
    out++;
    return out;
}

/**
 * test if a directory is empty
 */

int dir_empty(char * path)
{
    DIR *dp; 
    struct dirent *ep;
    int result = 1;
    
    if (NULL == (dp = opendir (path)))
        return -1;
    
    /*
     * check emptiness
     */
    
    while ((ep = readdir (dp)))
    {
        if (ep->d_type == DT_REG){
            result = 0; break;
        }
    }
    
    closedir (dp);
    return result;
}

/**
 * copy contents of one directory to another
 */

int dir_move(char * source, char * dest)
{
    DIR * dp;
    struct dirent * ep;
    char fullsource[PATH_MAX], fulldest[PATH_MAX];
    int err = 0;
    
    if (NULL == (dp = opendir(dest)))
        return -1;
    
    closedir(dp);
    
    if (NULL == (dp = opendir(source)))
        return -1;
    
    while ((ep = readdir(dp)))
    {
        if (ep->d_type != DT_REG)
            continue;
        
        sprintf(fullsource, "%s/%s", source, ep->d_name);
        sprintf(fulldest, "%s/%s", dest, ep->d_name);
        
        if ((err = rename(fullsource, fulldest)))
        {
            fprintf(stderr, "error renaming file \"%s\" to \"%s\"\n", fullsource, fulldest);
            err = errno; break;
        }
    }
    
    closedir(dp);
    return err;
}

/**
 * run a callback function for every token found in an input string
 * return the number of times that the callback ran successfully. 
 */

int token_iterate(int * n_tokens, char * tokens, char * delimiter, int (* callback)(char *, void *), void * arg)
{
    int result, callback_result = 0, n = 0;
    char * token, * t, * buf;
    char * delim = delimiter;
    
    if (delim == NULL)
        delim = " ";
    
    result = 0;
    t = rd_strdup(tokens);
    token = strtok_r(t, delim, &buf);
    
    while (token != NULL)
    {
        callback_result = callback(token, arg);
        result += (callback_result >= 0); 
        
        if (callback_result < 0)
        {
            free(t);
            return callback_result;
        }
        
        n++;
        token = strtok_r(NULL, delim, &buf);
    }
    
    free(t);
    
    if (n_tokens)
        *n_tokens = n;
        
    return result;
}

/**
 * determine if a test string is a part of a larger base string, or member
 * of a set represented by the base string, delimited by any of the characters
 * in DELIM
 */

int str_ismember(char * test, char * base, char * delim)
{
    char * token, * b, * buf;
    
    if (NULL == base || NULL == test)
        return 0;
    
    if (NULL == delim)
    {
        return (strstr(base,test) == 0);
    }
    
    b = rd_strdup(base);
    token = strtok_r(b, delim, &buf);
    
    while (token != NULL)
    {
        if (!strcmp(token, test))
        {
            free(b);
            return 1;
        }
        
        token = strtok_r(NULL, delim, &buf);
    }
    
    free(b);
    return 0;
}

/**
 * separate a path string into parts and return the core name
 */

char * file_name(char * path)
{
    char * start, * end, * name;
    int len;
    
    start = strrchr(path, '/');
    end = strrchr(path, '.');
    
    if (start == NULL)
        start = path;
    else
        start = start + 1;
    
    len = end - start;
    
    if (end == NULL || len < 0)
        return rd_strdup(path);
    
    name = rd_calloc(len + 1, sizeof(char));
    memcpy(name, start, len * sizeof(char));
    
    return name;
}

/**
 * get the hardware id associated to this machine
 */

void get_id(char * id_str)
{
    FILE * fp;
    char line[LINE_MAX];
    char * sp, * id = NULL;
    
    fp = fopen("/proc/cpuinfo", "r");
    
    if (fp == NULL){
        sprintf(id_str, "unknown"); return;
    }
    
    /*
     * take the first line that starts with "Serial"
     */
    
    while (fgets(line, LINE_MAX, fp) != NULL)
    { 
        if (strlen(line) < 7 || strncmp(line, "Serial", 6) != 0){
            continue;
        }
        
        if ((sp = strrchr(line, ':')))
            id = sp+2;
    
        /*
         * replace carriage return with string terminator
         */
        
        if ((sp = strrchr(line, '\n')))
            * sp = '\0';
        
        break;
        
    }

    fclose(fp);
    
    if (id == NULL)
        id = "unknown";
    
    strcpy(id_str, id);
    
}
