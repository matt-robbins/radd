#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "rd_memory.h"
#include "rd_table.h"

/**
 * free a single node in the table
 */

static void node_free(rd_table_node_t * n)
{
    free(n->name);
    
    if (n->free)
        n->free(n->data);
    
    n->free = NULL;
    n->name = NULL;
    n->data = NULL;
}

/**
 * the hash function
 */

static inline unsigned int hash(char * name)
{
    char * p = name;
    unsigned int sum = 0;
    
    while (*p != '\0')
    {
        sum = 1 + (sum << 5) + (unsigned int) *(p++);
    }
    
    return sum;
}

/**
 * create a new hash table
 */

rd_table_t * table_create(int N)
{
    rd_table_t * t = rd_calloc(1, sizeof(rd_table_t));
    
    t->N = 1;
    t->mask = 0;
    
    while (t->N < N)
    {
        t->mask |= t->N;
        t->N = t->N << 1;
    }
    
    t->values = rd_calloc(t->N, sizeof(rd_table_node_t));
    
    return t;
}

/**
 * insert an item into a table. Each item comes with its own destructor function.
 * the user must specify this as the fourth argument.  The function must return
 * void and take the item as a void pointer.
 */

int table_insert(rd_table_t * t, char * name, void * data, void (*data_free)(void *))
{
    int k;
    int ix = hash(name) & t->mask;
    
    for (k = 0; k < t->N; k++)
    {
        if (t->values[ix].name && !strcmp(t->values[ix].name, name))
            return -2;
        
        if (t->values[ix].name == NULL)
        {
            t->values[ix].name = rd_strdup(name);
            t->values[ix].data = data;
            t->values[ix].free = data_free;
            
            return ix;
        }
        
        ix = (ix >= t->N - 1) ? 0 : ix + 1;
    }
    
    /*
     * table is full if we get here
     */
    
    return -1;
}

int table_replace(rd_table_t * t, char * name, void * data, void (*data_free)(void *))
{
    int k;
    unsigned int ix = hash(name) & t->mask;
        
    for (k = 0; k < t->N; k++)
    {
        if (t->values[ix].name && !strcmp(t->values[ix].name, name))
            node_free(&t->values[ix]);
        
        if (t->values[ix].name == NULL)
        {
            t->values[ix].name = rd_strdup(name);
            t->values[ix].data = data;
            t->values[ix].free = data_free;
            
            return ix;
        }
        
        ix = (ix >= t->N - 1) ? 0 : ix + 1;
    }
    
    /*
     * table is full if we get here
     */
    
    return -1;
}

/**
 * convenience function.  insert a copy of a string
 */

int table_insert_str(rd_table_t * t, char * name, char * data)
{
    return table_insert(t, name, (void *) rd_strdup(data), free);
}

/**
 * insert a string pointer to a string that's gonna hang around
 */

int table_insert_strp(rd_table_t * t, char * name, char * data)
{
    return table_insert(t, name, (void *) data, NULL);
}

/**
 * remove an item from a table
 */

int table_remove(rd_table_t * t, char * name)
{
    int k;
    unsigned int ix = hash(name) & t->mask;
    
    for (k = 0; k < t->N; k++)
    {
        if ((t->values[ix].name != NULL) && !strcmp(t->values[ix].name, name))
        {
            node_free(&t->values[ix]); return 0;
        }
        
        ix = (ix == t->N - 1) ? 0 : ix + 1;
    }
    
    return -1;
}

static inline void ** table_getp_internal(rd_table_t * t, char * name)
{
    int k;
    unsigned int ix = hash(name) & t->mask;
        
    for (k = 0; k < t->N; k++)
    {
        if ((t->values[ix].name != NULL) && !strcmp(t->values[ix].name, name))
        {
            return &t->values[ix].data;
        }
        
        ix = (ix >= t->N - 1) ? 0 : ix + 1;
    }
    
    return NULL;
}

/**
 * retrieve an item from a table
 */

void ** table_getp(rd_table_t * t, char * name)
{
    return table_getp_internal(t, name);
}


void * table_get(rd_table_t * t, char * name)
{
    void ** out = table_getp_internal(t, name);
        
    if (out == NULL)
        return NULL;
    
    return *out;
}

char * table_get_str(rd_table_t * t, char * name)
{
    return rd_strdup((char *) table_get(t, name));
}

char * table_get_strp(rd_table_t * t, char * name)
{
    return (char *) table_get(t, name);
}

int table_get_all(rd_table_t * t, char *** names, void *** values)
{
    int k, n = 0;
    
    *names = (char **) NULL;
    *values = NULL;
    
    for (k = 0; k < t->N; k++)
    {
        if (!t->values[k].name)
            continue;
        
        *names = rd_realloc(*names, (n + 1) * sizeof(char *));
        *values = rd_realloc(*values, (n + 1) * sizeof(void *));
        
        if (*names == NULL || *values == NULL)
            return n;
                
        (*names)[n] = t->values[k].name;
        (*values)[n] = t->values[k].data;
        
        n++;
    }
    
    return n;
}

/**
 * free all memory associated with a table
 */

void table_free(rd_table_t * t)
{
    int ix;
    
    for (ix = 0; ix < t->N; ix++)
    {
        node_free(&t->values[ix]);
    }
    
    free(t->values);
    free(t);
}

#ifdef TEST
int main(int argc, char * argv[])
{
    int k;
    rd_table_t * t = table_create(50);
    
/*
    table_insert_strp(t, "poopy", "youpy");
    
    printf("%x\n", table_get_strp(t, "oeihg"));
    printf("%s\n", table_get_strp(t, "poopy"));
  */  
    table_free(t);

    for (k = 0; k < 100; k++)
    {
        t = table_create(50);
        table_free(t);
    }
}
#endif
