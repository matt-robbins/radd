#ifndef RD_FILESTREAM_H
#define RD_FILESTREAM_H

#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <fnmatch.h>
#include <sndfile.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "rd_time.h"
#include "detect.h"

typedef struct {
    char * path;
    char ** names;
    SNDFILE * fp;
    SF_INFO info;
    int file_ix;
    int64_t ix;
    int N;
    int rate;
} file_stream_t;

file_stream_t * file_stream_create(char * dir, char * type);

void file_stream_free(file_stream_t * fs);

int file_stream_read(file_stream_t * fs, sample_t * out, int N, rd_time_t * stamp);

#endif
