#include <stdlib.h>
#include <stdio.h>
#include "./detect.h"
#include "./detector.h"
#include "./configure.h"
#include "./rd_vector.h"
#include <sys/time.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <poll.h>
#include <errno.h>
#include <dirent.h>
#include <fnmatch.h>
#include <sys/wait.h>
#include <pthread.h>
#include <sndfile.h>

#include "rd_memory.h"
#include "./rd_server.h"
#include "./rd_util.h"

#define DETECTOR_PATH "/usr/lib/detectors"
#define MIN(a, b) ((a) < (b) ? (a) : (b))

#define MAX_DETECTORS 5

int Done = 0;
int SockFd = 0;
int NewSockFd = 0;

char * DetList = NULL;

struct server_data_s {
    int fd;
    detector_t * det[MAX_DETECTORS];
    int ndet;
    sound_buffer_t * buf;
    pthread_t read_thread;
    pthread_t det_thread[MAX_DETECTORS];
    int stop;
    int page;
} ServerData;


void term_handler(int sig)
{
    if (sig == SIGSEGV)
        fprintf(stderr, "segfault. Attempting clean shutdown, but no guarantees!\n");
    
    if (NewSockFd)
    {
        shutdown(NewSockFd, SHUT_RDWR);
        close(NewSockFd);
    }
    
    shutdown(SockFd, SHUT_RDWR);
    close(SockFd);
       
    exit(sig);
}

void child_handler(int sig)
{
    int status;
    int pid = wait(&status);
    
    fprintf(stderr,"child with pid %d finished 0x%x!\n", pid, status);
}

void error (char * msg)
{
    perror (msg);
    exit (1);
}

static int check_header(int fd, char * header)
{
    char buf[4];
    int n;
    
    n = recv(fd, &buf, 4, MSG_PEEK);
    
    if (n < 0)
        perror("couldn't read magic from socket!\n");
        
    return strncmp(header, buf, 4) != 0;    
}


static void * _read_thread(void * arg)
{
    int sockfd = *((int *) arg);
    char magic[4];
    rd_server_data_header_t header;
    rd_time_t stamp, bstamp;
    int n, k, bix = 0, hix = 0, nread, ntoread;
    unsigned int ix = 0, tix = 0;
    header.valid = 0;
    sample_t * b;
            
    while (1)
    {
        bzero(magic, sizeof(magic));
        bzero(&header, sizeof(header));
        
        n = recv(sockfd, magic, sizeof(magic), MSG_PEEK);
        
        if (n < 4)
        {
            fprintf(stderr, "couldn't read header from socket!\n");
            exit(1);
            return NULL;
        }
        
        if (!strncmp(magic, RD_SERVER_PAGE_MAGIC, 4))
        {
            recv(sockfd, magic, sizeof(magic), 0);
            
            /*
             * page break marker is a non-zero userdata
             */
             
            sound_buffer_write_wait(ServerData.buf);
             
            sound_buffer_write_userdata(ServerData.buf, (void *) 0xf);
            sound_buffer_increment_write_pointer(ServerData.buf, (rd_time_t) 0);
            
            continue;
        }
        
        if (!strncmp(magic, RD_SERVER_DATA_MAGIC, 4))
        {
            recv(sockfd, &header, sizeof(header), 0);
            
            header.size = ntohl(header.size);
            if (header.size == 0)
            {
                ServerData.stop = 1;
                sound_buffer_read_signal(ServerData.buf);
            }
        }
        else
        {
            fprintf(stderr, "ERROR: got bad header, %.4s\n", magic);
                
            shutdown(sockfd, SHUT_RDWR);
            close(sockfd);
            exit(1);
        }
        
        stamp = ((rd_time_t) header.timestamp_h << 32) + (rd_time_t) header.timestamp_l;
        tix = ix;
        hix = 0;
                        
        while (hix < header.size)
        {            
            sound_buffer_write_wait(ServerData.buf);
            b = sound_buffer_get_write_pointer(ServerData.buf);
            
            ntoread = MIN(ServerData.buf->K - bix, header.size - hix);
            nread = recv(sockfd, b + bix, ntoread * sizeof(sample_t), 0 /*MSG_WAITALL*/);
            
            if (nread <= 0)
            {
                fprintf(stderr, "data read failed! %d\n", nread);
                ServerData.stop=1;
                return NULL;
            }
            
            nread /= sizeof(sample_t);
                        
            if (nread != ntoread)
            {
                fprintf(stderr, "recieved %d samples out of %d\n", nread, ntoread);
                ServerData.stop=1;
                return NULL;
            }
            
            /*
             * endianness fix
             */
            
            for (k = 0; k < nread; k++)
            {
                b[bix + k] = ntohl(b[bix + k]);
            }
            
            ix += nread;
            hix += nread;
                        
            if (nread < ServerData.buf->K - bix)
            {
                bix += nread; break;
            }
            
            bix = 0;
                        
            bstamp = stamp + rd_samples_to_time(
                (int) (ix - tix) - (int) nread, header.samplerate);
            
            sound_buffer_write_userdata(ServerData.buf, 0x0);
            sound_buffer_increment_write_pointer(ServerData.buf, bstamp);
        }
    }
}

void * _det_thread(void * arg)
{
    struct server_data_s * d = &ServerData;
    detector_t * det = arg;
    int k, nevents, strl, out;
    rd_event_t event;
    char event_str[4096];
    
    static event_t events[RD_MAX_EVENTS];
    rd_time_t stamp;
    
    sample_t * buf = sound_buffer_peek(d->buf, &stamp);
    nevents = detector_run_buffer(det, events, buf, d->buf->K, stamp);
    
    for (k = 0; k < nevents; k++)
    {
        detector_event_convert(det, events + k, &event, 1, stamp);
        
        strl = event_to_xml(event_str, sizeof(event_str), &event);
        
        out = send(d->fd, event_str, strl, 0);
        if (out != strl)
        {
            fprintf(stderr, "failed to write event %d != %d! %s\n", out, strl, strerror(errno));
        }
        
        event_free(event);
        out = write(d->fd, "\n", 1);
        if (out != 1)
        {
            fprintf(stderr, "failed to write line ending!\n");
        }
    }
    
    return NULL;
}

int socket_server(int port)
{
    int sockfd, optval;
    struct sockaddr_in serv_addr;
    
    if ((sockfd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
        return sockfd;
    
    memset (&serv_addr, 0, sizeof (serv_addr));
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port);
    
    optval = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval,sizeof(int));
    
    if (bind (sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0)
        return -1;
    
    return sockfd;
}

int socket_wait(int sockfd)
{
    unsigned int clilen;
    int NewSockFd;
    struct sockaddr_in cli_addr;
    
    listen (sockfd, 5);
    clilen = sizeof (cli_addr);
    
    NewSockFd = accept (sockfd, (struct sockaddr *) &cli_addr, &clilen);
    
    return NewSockFd;
}

static int send_error(int sock, char * error)
{
    char buffer[LINE_MAX];
    int n, out;
    
    n = snprintf(buffer, sizeof(buffer), "<error>%s</error>", error);
    out = write(sock, buffer, n);
    return out;
}

static int send_ok(int sock, char * msg)
{
    char buffer[LINE_MAX];
    int n, out;
    
    n = snprintf(buffer, sizeof(buffer), "<ok>%s</ok>", msg);
    out = write(sock, buffer, n);
    return out;
}

static int write_conf(int sockfd, int n, char * file)
{
    int nr;
    char buf[4096];
    
    if (n == 0)
        return 0;
    
    FILE * fp = fopen(file, "w");
    
    if (fp == NULL)
        return -1;
    
    nr = recv(sockfd, (void *) buf, n, MSG_WAITALL);
    fwrite(buf, 1, nr, fp);
    
    fclose(fp);
    return 0;
}

static int process_conf(int sockfd, int n, int * bufsize)
{
    int k, nb, nd;
    rd_server_conf_header_t header;
    char conf_file[] = "/tmp/confXXXXXX";
    char * path = DETECTOR_PATH;
    
    nd = n > MAX_DETECTORS ? MAX_DETECTORS : n;
    
    ServerData.ndet = nd;
    
    if (check_header(sockfd, "RDCF"))
        return -1;
    
    for (k = 0; k < nd; k++)
    {
        bzero(&header, sizeof(header));
        nb = recv(sockfd, &header, sizeof(header), MSG_WAITALL);
    
        if (nb < 0){
            perror("couldn't read cfg header from socket!\n"); return -1;
        }
            
        if (strncmp(header.magic, RD_SERVER_CONF_MAGIC, 4))
        {
            fprintf(stderr,"failed to get configuration header!\n");
            return -1;
        }
                
        if (header.confsize > 0)
        {
            int out = mkstemp(conf_file);
            if (out < 0)
            {
                fprintf(stderr, "failed to make temp file for writing conf!\n");
            }
            write_conf(sockfd, header.confsize, conf_file);
        }
        else
            conf_file[0] = 0;
        
		fprintf(stderr, "initializing detector with config file '%s'\n", conf_file);
    
        ServerData.det[k] = detector_create(path, header.det_name, conf_file);
        
        if (ServerData.det[k] == NULL)
        {
            fprintf(stderr, "ERROR: failed to create detector!\n");
            return -1;
        }
        
        detector_initialize(ServerData.det[k]);
        
        *bufsize = next_pow2(ServerData.det[k]->sound_setup.N);
        
        if (header.confsize > 0)
            unlink(conf_file);
    }

    return 0;
}

static int list_detectors(char ** list, char * path, char * file_ext)
{
    int N, k, ix;
    struct dirent ** flist;
    char pat[64];
    
    sprintf(pat, "*.%s", file_ext);
    N = scandir (path, &flist, NULL, alphasort);
    
    if (N < 0)
    {
        fprintf(stderr, "couldn't read detector directory %s\n", path);
        return -1;
    }
    
    *list = rd_malloc(N*NAME_MAX); *list[0] = 0;
    
    for (k = 0, ix = 0; k < N; k++)
    {
        if (0 != fnmatch(pat, flist[k]->d_name, 0))
            continue;
        
        strncat(*list, flist[k]->d_name, 
            strlen(flist[k]->d_name) - strlen(file_ext) - 1
        );
        
        strcat(*list, k < N - 1 ? "," : "");
            
        ix++;
    }
    
    free(flist);
        
    return 0;
}

static int process_query(int sockfd)
{
    int nb, nq, out;
    rd_server_query_header_t header;
    char * query;
    
    bzero(&header, sizeof(header));
    nb = recv(sockfd, &header, sizeof(header), MSG_WAITALL);
    
    if (nb < 0){
        perror("couldn't read query header from socket!\n"); return -1;
    }
    
    nq = header.qsize;
    query = rd_calloc(nq, sizeof(char));
    
    nb = recv(sockfd, query, nq, 0);
    
    if (!strcmp(query, "detlist"))
    {
        out = write(sockfd, DetList, strlen(DetList));
        
        if (out != strlen(DetList))
        {
            fprintf(stderr, "failed to send detlist!\n");
        }
    }
    
    free(query);
    return 0;
}


static int process_main(int sockfd)
{
    int n, k, bufsize, result, ret = 0;
    rd_server_main_header_t header;
    char buf[5];
            
    n = recv(sockfd, &buf, 4, MSG_PEEK);
    
    if (n < 0)
        perror("couldn't read magic from socket!\n");
        
    if (buf[0] != 'R' || buf[1] != 'D')    
    {
        fprintf(stderr, "not talking RD protocol?\n");
        ret = -1;
        goto done;
    }
    
    if (buf[2] == 'Q' && buf[3] == 'Y')
    {
        ret = process_query(sockfd);
        goto done;
    }
    
    bzero(&header, sizeof(header));
    n = recv(sockfd, &header, sizeof(header), MSG_WAITALL);
    
    if (n < 0)
        perror("couldn't read header from socket!\n");
            
    if (strncmp(header.magic, RD_SERVER_MAIN_MAGIC, 4))
    {
        fprintf(stderr,"failed to get main header!\n");
        ret = -1;
        goto done;
    }        
        
    send_ok(sockfd, "conf");

    result = process_conf(sockfd, header.det_num, &bufsize);
    
    if (result != 0)
    {
        send_error(sockfd, "bad configuration");
        goto done;
    }
        
    send_ok(sockfd, "ready");
    
    ServerData.buf = sound_buffer_create(bufsize, 1, 1, 1, header.samplerate);
        
    n = recv(sockfd, &buf, 4, MSG_PEEK);
    
    if (strncmp(buf, RD_SERVER_DATA_MAGIC, 4))
    {
        fprintf(stderr, "data doesn't follow conf!\n");
        ret = -1;
        goto done;
    }
    
    pthread_create(&ServerData.read_thread, NULL, _read_thread, &sockfd);
        
    while (1)
    {    
        if ((sound_buffer_read_space(ServerData.buf) == 0) && ServerData.stop)
        {
            fprintf(stderr, "STOP!\n");
            goto done;
        }
                        
        if (sound_buffer_read_userdata(ServerData.buf) != 0)
        {
            fprintf(stderr, "got a non-zero userdata, sending page break\n");
            n = write(sockfd, "\r", 1);
            if (n != 1)
            {
                fprintf(stderr, "ERROR: failed to send page break!\n");
            }
        }
    
        sound_buffer_read_wait(ServerData.buf);
                
        for (k = 0; k < ServerData.ndet; k++)
        {
            pthread_create(
                &ServerData.det_thread[k], 
                NULL, _det_thread, 
                ServerData.det[k]
            );
        }
        
        for (k = 0; k < ServerData.ndet; k++)
        {
            pthread_join(ServerData.det_thread[k], NULL);
        }
        
        sound_buffer_increment_read_pointer(ServerData.buf);
    }
    
done:
    fprintf(stderr, "closing socket!\n");
    shutdown(sockfd, SHUT_RDWR);
    close(sockfd);

    return ret;
}

/**
 * the program
 */

int main (int argc, char * argv[])
{
    int pid, result;
    
    signal(SIGPIPE, SIG_IGN);
    signal(SIGSEGV, term_handler);
    signal(SIGTERM, term_handler);
    signal(SIGINT, term_handler);
    signal(SIGCHLD, child_handler);
    
    if ((SockFd = socket_server(RD_SERVER_DEFAULT_PORT)) < 0)
        error("couldn't create socket!");
        
    list_detectors(&DetList, DETECTOR_PATH, "so");
    
    fprintf(stderr, "rd_server starting, available detectors:\n%s\n",DetList);
    
    /*
     * open new fd's for each request for new data.  Note that we 
     * are keeping track of state internally
     */
    
    while (!Done)
    {
        fprintf(stderr, "waiting for connection...\n");
        
        if ((NewSockFd = socket_wait(SockFd)) < 0)
            break;
        
        if ((pid = fork()) < 0)
        {
            perror("failed to fork");
            exit(1);
        }
        
        /*
         * parent continues
         */
        
        if (pid > 0)
        {
            close(NewSockFd);
            continue;
        }
        /*
         * child does stuff
         */
                 
        ServerData.fd = NewSockFd;
        
        fprintf(stderr, "connected!\n");

        result = process_main(NewSockFd);
        exit(result ? 1 : 0);
    }
        
    return 0;
}
