#ifndef RD_UTIL_H
#define RD_UTIL_H

#include <sys/stat.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <dirent.h>

typedef enum {
    RD_FALSE = 0,
    RD_TRUE,
} rd_bool_t;

void get_id(char * id_str);
int ftouch(char * filename);
int fexist(char * filename);
int f_lock(char * file);
int f_lock_block(char * file);
int f_ulock(int fd);
int dir_empty(char * path);
int dir_move(char * source, char * dest);

int token_iterate(
    int * n_tokens, char * tokens, char * delimiter, 
    int (* callback)(char *, void *), void * arg
);

int str_ismember(char * test, char * base, char * delim);

int next_pow2(int in);

#endif
