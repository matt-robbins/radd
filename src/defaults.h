
/** 
 * @file this file contains all default values for parameters that are used by
 * run-detect
 */

#define DEFAULT_CONFIG_FILE "/etc/run_detect.conf"
#define DEFAULT_LOG_FILE "run_detect.log"
#define DEFAULT_LOCK_FILE "run_detect.lock"
#define DEFAULT_RUNNING_DIR "/tmp/"
#define DEFAULT_REPORT_FLAG "/tmp/report"
#define DEFAULT_PLUGIN_PATH "/tmp"
#define DEFAULT_DETECTOR_PATH "/var/lib/run_detect/detectors/"
#define DEFAULT_CLASSIFIER_PATH "/var/lib/run_detect/classifiers/"
#define DEFAULT_STORE_PATH "/var/lib/run_detect/detections/"
#define DEFAULT_FORWARD_PATH "/tmp/store/"
#define DEFAULT_DB_FILENAME "dump.db"
#define DEFAULT_DETECTOR_SEP ",;"
#define DEFAULT_DETECTOR_THREAD_STACK_SIZE 0x100000

#define DEFAULT_EVENT_BUFFER_SIZE 10
#define DEFAULT_EVENT_WRITE_PERIOD 1
#define DEFAULT_REPORT_PERIOD 3600
#define DEFAULT_REPORT_HOLD 300
#define DEFAULT_REPORT_TIMEOUT 1600
#define DEFAULT_REPORT_RETRIES 3

#define DEFAULT_FILE_TYPE "aiff"

#define DEFAULT_FPC_POINT_POS 25

