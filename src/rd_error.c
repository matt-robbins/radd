/**\file 
 * functions dealing with run_detect error codes and string representations
 */

#include "rd_error.h"

char * rd_error_str(rd_error_t err)
{
    switch (err)
    {
        case RD_ERROR_NO_ERROR:
            return "no error";
        case RD_ERROR_FILE:
            return "filesystem error";
        case RD_ERROR_SYSTEM:
            return "system call error";
        case RD_ERROR_PLUGIN:
            return "couldn't load plugin";
        case RD_ERROR_DB:
            return "problem with database";
        case RD_ERROR_GPS:
            return "problem with GPS";
        case RD_ERROR_IO:
            return "I/O error";
        case RD_ERROR_LOCK:
            return "resource is locked";
        case RD_ERROR_MALLOC:
            return "error allocating memory";
        case RD_ERROR_MISC:
        default:
            return "error that the developer was too lazy to specify";
    }
    
}
