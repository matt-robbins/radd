
#ifndef RD_EVENT_BUFFER_H
#define RD_EVENT_BUFFER_H

#include <sys/types.h>
#include <stdlib.h>

#include "detect.h"
#include "event.h"

/**
 * a ring buffer of events
 */

typedef struct {
    rd_event_t * buf;
    size_t N;
    off_t pr;
    off_t pw;
    int dropped;
    int debug_count;
    int eof;
    int realtime;
    pthread_mutex_t rcond_lock;
    pthread_mutex_t wcond_lock;
    pthread_cond_t rcond;
    pthread_cond_t wcond;
} event_buffer_t;

/**
 * Make a new event buffer
 */

event_buffer_t * event_buffer_create(size_t N, int realtime);

/**
 * Clean up the event buffer.  All constituent events must already be cleaned up
 */

void event_buffer_destroy(event_buffer_t * eb);

/**
 * Find out how many events are available for reading
 */

size_t event_buffer_read_avail(event_buffer_t * eb);

/**
 * Find out how many events are available for writing
 */

size_t event_buffer_write_avail(event_buffer_t * eb);

/**
 * Read events into an output buffer.  Output buffer must be allocated by the user.
 */

size_t event_buffer_read(event_buffer_t * eb, rd_event_t * out, size_t N);

/**
 * Read all available events into an allocated array.  the caller is responsible for freeing.
 */

size_t event_buffer_read_all(event_buffer_t * eb, rd_event_t ** out);

/**
 * Write events into the ringbuffer.  This function is not responsible for freeing input.
 */

size_t event_buffer_write(event_buffer_t * eb, rd_event_t * in, size_t N);

int event_buffer_write_signal(event_buffer_t * b);
int event_buffer_read_signal(event_buffer_t * b);


/**
 * Get (and reset) a count of dropped events due to insufficient buffer space.
 */

int event_buffer_check_dropped(event_buffer_t * eb);

/**
 * get the time of the oldest event in the buffer
 */
 
rd_time_t event_buffer_get_time(event_buffer_t * eb);


#endif

