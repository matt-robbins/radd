#ifndef RD_SNDFILE_H
#define RD_SNDFILE_H

#include <sndfile.h>

#if FIXED_POINT == 32
    #define sf_write_fcn sf_write_int
    #define sf_writef_fcn sf_writef_int

    #define sf_read_fcn sf_read_int
    #define sf_readf_fcn sf_readf_int
#elif FIXED_POINT == 16
    #define sf_write_fcn sf_write_short
    #define sf_writef_fcn sf_writef_short

    #define sf_read_fcn sf_read_short
    #define sf_readf_fcn sf_readf_short
#else
    #define sf_write_fcn sf_write_float
    #define sf_writef_fcn sf_writef_float

    #define sf_read_fcn sf_read_float
    #define sf_readf_fcn sf_readf_float
#endif

#endif
