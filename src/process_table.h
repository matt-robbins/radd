#ifndef RD_PROCESS_TABLE_H
#define RD_PROCESS_TABLE_H

#include <sys/wait.h>
#include <string.h>
#include <pthread.h>
#include "rd_process.h"

typedef struct {
    pid_t pid;
    char * info;
    int comm[2];
    int status;
    pthread_cond_t status_ready;
} process_table_entry_t;

typedef struct {
    process_table_entry_t * entries;
    pthread_mutex_t inuse;
    int N;
} process_table_t;

enum {
    TABLE_GET,
    TABLE_CREATE,
    TABLE_CLEAR,
};

enum {
    STATUS_ERROR = -1,
    STATUS_TIMEOUT = -2,
};

process_table_t * process_table(void);

void process_table_free(process_table_t * table);

int process_table_init(void);

int process_table_find_entry(process_table_t * t, pid_t pid);

int process_table_lock(process_table_t * t);
int process_table_unlock(process_table_t * t);

int process_table_add(pid_t pid, char * info, int read, int write);

int process_table_remove(pid_t pid);

char * process_table_get_info(pid_t pid);

int process_table_get_pid(char * info);

int process_table_get_status(pid_t pid, int timeout);

int process_table_set_status(pid_t pid, int status);

char * process_table_dump(void);

int process_table_get_count(void);

#endif

