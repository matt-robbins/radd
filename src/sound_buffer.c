#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>

#include "rd_time.h"
#include "detect.h"
#include "sound_buffer.h"
#include "rd_memory.h"

struct segment {
    sample_t * data;
    int N;
    rd_time_t time;
    void * userdata;
};

typedef struct segment segment_t;

static int next_poweroftwo(int in)
{
    int k = 1;
    
    while (k < in)
        k *= 2;
    
    return k;
}

sound_buffer_t * sound_buffer_create(
    int K, int latency, int history, int free, int rate)
{
    sound_buffer_t * new = NULL;
    int n = latency + history + free, N = n, k;
    
    /*
     * if either is not a power of two...
     */
    
    if (N & (N - 1))
        N = next_poweroftwo(n);
    
    if (K & (K - 1))
    {
        fprintf(stderr, 
            "sound buffer size is not a power of two! (N=%d, K=%d)\n", N, K
        );
        
        return NULL;
    }
        
    new = rd_malloc(sizeof(sound_buffer_t));
    
    new->N = N; new->K = K; new->rate = rate;
    new->smask = N - 1u;
    new->dmask = N * K - 1u;
    
    new->eof = 0;
    new->rate = rate;
    new->ready = 0;
    new->count = 0;
    new->latency = (unsigned) latency;
    new->history = (unsigned) history;
    new->rp = 0u; new->wp = new->latency & new->smask;
    
    new->segments = rd_calloc(N,  sizeof(segment_t));
    new->data = rd_calloc(N * K, sizeof(sample_t));
        
    /*
     * each segment has an aliased pointer
     */
    
    for (k = 0; k < N; k++)
    {
        new->segments[k].data = new->data + k * K;
        new->segments[k].N = K;
    }
    
    /*
     * set up synchronization
     */
        
    pthread_mutex_init(&new->rcond_lock, NULL);
    pthread_cond_init(&new->rcond, NULL);
    pthread_mutex_init(&new->wcond_lock, NULL);
    pthread_cond_init(&new->wcond, NULL);

    return new;
}

int sound_buffer_read_signal(sound_buffer_t * sb)
{
    pthread_mutex_lock(&sb->rcond_lock);
    pthread_cond_signal(&sb->rcond);
    pthread_mutex_unlock(&sb->rcond_lock);
    return 0;
}

int sound_buffer_write_signal(sound_buffer_t * sb)
{
    pthread_mutex_lock(&sb->wcond_lock);
    pthread_cond_signal(&sb->wcond);
    pthread_mutex_unlock(&sb->wcond_lock);
    return 0;
}

void sound_buffer_destroy(sound_buffer_t * sb)
{
    free(sb->segments);
    free(sb->data);
    
    free(sb);
}


int sound_buffer_write_space(sound_buffer_t * b)
{
    return (unsigned) (b->rp - b->wp - b->history) & b->smask;
}

int sound_buffer_read_space(sound_buffer_t * b)
{
    return (unsigned) (b->wp - b->rp - b->latency) & b->smask;
}

int sound_buffer_read_wait(sound_buffer_t * sb)
{
    int ret = 0, state;
    pthread_mutex_lock(&sb->rcond_lock);
    
    if (!sound_buffer_read_space(sb))
    {
        ret = 1;
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &state);
        pthread_cond_wait(&sb->rcond, &sb->rcond_lock);
        pthread_setcancelstate(state, NULL);
    }
        
    pthread_mutex_unlock(&sb->rcond_lock);
    return ret;
}

int sound_buffer_write_wait(sound_buffer_t * sb)
{
    int ret = 0, state;
    pthread_mutex_lock(&sb->wcond_lock);
    
    if (!sound_buffer_write_space(sb))
    {
        ret = 1;
        
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &state);
        pthread_cond_wait(&sb->wcond, &sb->wcond_lock);
        pthread_setcancelstate(state, NULL);
    }
        
    pthread_mutex_unlock(&sb->wcond_lock);
    return ret;
}

int sound_buffer_write(sound_buffer_t * b, sample_t * in, rd_time_t time)
{
    segment_t * s = &b->segments[b->wp];
    
    sound_buffer_write_wait(b);
    
    memcpy(s->data, in, s->N * sizeof(sample_t));
    sound_buffer_increment_write_pointer(b, time);
    
    return 1;
}

int sound_buffer_increment_read_pointer(sound_buffer_t * b)
{
    pthread_mutex_lock(&b->wcond_lock);
    b->rp = (b->rp + 1u) & b->smask;

    if (b->count < b->latency)
        b->count++;
    else
        b->ready = 1;
        
    pthread_cond_signal(&b->wcond);
    pthread_mutex_unlock(&b->wcond_lock);
    
    return b->rp;
}

sample_t * sound_buffer_get_write_pointer(sound_buffer_t * b)
{
    segment_t * s = &b->segments[b->wp];
    
    if (sound_buffer_write_space(b) < 1)
        return NULL;
    
    return s->data;
}

void sound_buffer_write_userdata(sound_buffer_t * b, void * userdata)
{
    segment_t * s = &b->segments[b->wp];
    s->userdata = userdata;
}


int sound_buffer_increment_write_pointer(sound_buffer_t * b, rd_time_t time)
{
    segment_t * s = &b->segments[b->wp];
    s->time = time;

    pthread_mutex_lock(&b->rcond_lock);
    b->wp = (b->wp + 1u) & b->smask;
    pthread_cond_signal(&b->rcond);
    pthread_mutex_unlock(&b->rcond_lock);
    
    return b->wp;
}

segment_t * sound_buffer_get_read_segment(sound_buffer_t * b)
{
    return &b->segments[b->rp];
}

sample_t * sound_buffer_peek(sound_buffer_t * b, rd_time_t * time)
{
    segment_t * s = &b->segments[b->rp];
    
    *time = s->time;
    return s->data;
}

int sound_buffer_read(sound_buffer_t * b, sample_t * out, rd_time_t * time)
{
    sample_t * buf = sound_buffer_peek(b, time);
    
    if (buf == NULL)
        return 0;
    
    memcpy(out, buf, b->K * sizeof(sample_t));
    
    sound_buffer_increment_read_pointer(b);
    return 1;
}

void * sound_buffer_read_userdata(sound_buffer_t * b)
{
    segment_t * s = &b->segments[b->rp];
    return s->userdata;
}


static int sound_buffer_read_(
    sound_buffer_t * b, sample_t ** out, unsigned start, unsigned stop
){
    sample_t * data; 
    
    int n = (stop - start) & b->dmask;
        
    data = rd_malloc(n * sizeof(sample_t));
    
    if (start < stop)
    {
        memcpy(data, b->data + start, n * sizeof(sample_t));
    }
    else
    {
        memcpy(data, b->data + start, ((b->N * b->K) - start) * sizeof(sample_t));
        memcpy(data + (b->N * b->K) - start, b->data, stop * sizeof(sample_t));
    }
    
    *out = data;
    return n;
}

int sound_buffer_read_ix(
    sound_buffer_t * b, sample_t ** out, int ix1, int ix2)
{
    off_t ix = b->segments[b->rp].data - b->data;
    unsigned int start, stop;
    
    if ((ix2 - ix1) > b->K * b->N)
    {
        *out = NULL;
        return 0;
    }
    
    start = (ix + ix1) & b->dmask;
    stop = (ix + ix2) & b->dmask;
            
    return sound_buffer_read_(b, out, start, stop);
}

/**
 * read samples from the sound buffer based on real time selection
 */

int sound_buffer_read_time(
    sound_buffer_t * b, sample_t ** out, rd_time_t time, rd_time_t dur, int * corrupt)
{
    unsigned int oldwp = b->wp;
    unsigned int p = (b->rp - 1u) & b->smask;
    unsigned int start, stop, startseg;
    int n;
    
    segment_t * s = &b->segments[p];
    
    if (s->time == 0)
        return 0;
    
    off_t ix = s->data - b->data;
    rd_time_t t_offset = time - s->time;   
    
    int ob = rd_time_to_samples(t_offset, b->rate);
    int oe = rd_time_to_samples(t_offset + dur, b->rate);
    
    if (oe > b->K || ob < -(b->K * (b->N - 1)))
        return 0;
        
    start = (ix + ob) & b->dmask;
    stop = (ix + oe) & b->dmask;
    startseg = (start / b->K) & b->smask;
    
    n = sound_buffer_read_(b, out, start, stop);
    
    /*
     * check if the write pointer overlapped our section during reading
     * if the difference increases between the write pointer and the start segment
     * then we know the data has been corrupted.
     */
    
    if (corrupt)
        *corrupt = ((startseg - b->wp) & b->smask) - ((startseg - oldwp) & b->smask);
    
    return n;
}

void print_array(sample_t * x, int N)
{
    int k;
    for (k = 0; k < N; k++)
#ifdef FIXED_POINT
        printf("%d, ", x[k]);
#else
        printf("%f, ", x[k]);
#endif
    printf("\n");
}

void print_sound_buffer(sound_buffer_t * sb)
{
    int k;
    
    char * str = rd_calloc(sb->N+1, sizeof(char));
    
    //printf("%d segments\n", sb->N);
    
    for (k = 0; k < sb->N; k++)
    {
        if (sb->wp == k)
            str[k] = 'w';
        else if (sb->rp == k)
            str[k] = 'r';
        else if ((((sb->wp < sb->rp) && (k > sb->wp) && (k < sb->rp))) ||
                ((sb->wp > sb->rp) && ((k > sb->wp) || (k < sb->rp)))) 
            str[k] = '.';
        else
            str[k] = '#';
            
        //~ printf("segment[%d] at %p:%p %s %s\n   Time: %f\n", 
            //~ k, 
            //~ (void *) &sb->segments[k], 
            //~ (void *) sb->segments[k].data,
            //~ sb->wp == k ? "<w" : "",
            //~ sb->rp == k ? "<r" : "",
            //~ (double) sb->segments[k].time
        //~ );
    }
    
    str[sb->N] = 0;
    
    printf("sound buffer: %s\r", str);
    
    free(str);
}

#ifdef SOUND_BUFFER_TEST


void * reader(void * arg)
{
    sound_buffer_t * b = arg;
    int k, r;
    
    for (k = 0; k < 1000; k++)
    {
        r = sound_buffer_read_wait(b);
        if (r)
        {
            fprintf(stderr,"had to wait to read!\n");
            //usleep(rand()/10000);
        }
        fprintf(stderr, "read space = %d\n", sound_buffer_read_space(b));
        sound_buffer_increment_read_pointer(b);
    }
}
void * writer(void * arg)
{
    sound_buffer_t * b = arg;
    rd_time_t time;
    int k, r;
    
    for (k = 0; k < 1000; k++)
    {
        r = sound_buffer_write_wait(b);
        if (r)
        {
            fprintf(stderr,"had to wait to write!\n");
            //usleep(rand()/10000);
        }
        fprintf(stderr, "write space = %d\n", sound_buffer_write_space(b));

        sound_buffer_increment_write_pointer(b,time);
    }
    
}

int main(int argc, char * argv[])
{
    int j, k, ix, s;
    rd_time_t t;
    sound_buffer_t * b = sound_buffer_create(4, 8, 1, 10, 2000);
    sample_t buf[4];
    sample_t * out;
    pthread_t read, write;
    
    j = 0;
    
    printf("sound buffer has %d segments\n", b->N);
    
    printf("read space = %d\n", sound_buffer_read_space(b));
    printf("write space = %d\n", sound_buffer_write_space(b));
    
    pthread_create(&read, NULL, reader, b);
    pthread_create(&write, NULL, writer, b);
    
    pthread_join(read,NULL);
    pthread_join(write,NULL);
    
    printf("read space = %d\n", sound_buffer_read_space(b));
    printf("write space = %d\n", sound_buffer_write_space(b));

    print_array(b->data, b->N * b->K);
    
    printf("-------\n");
    
    ix = sound_buffer_read_ix(b, &out, &t, -7, 1);
    
    print_array(out, ix);
    free(out);

    sound_buffer_destroy(b);
    return 0;
}
#endif

