
#ifndef RD_CONFIGURE_H
#define RD_CONFIGURE_H

#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <sqlite3.h>

#include "rd_table.h"
#include "rd_vector.h"

#define CONFIG_TABLE_SIZE 256
#define CONFIG_MAX_ENTRY_LEN 64
#define CONFIG_NAMESPACE_SEP ':'

typedef enum {
    CONFIG_ERROR_NO_ERROR = 0,
    CONFIG_ERROR_BAD_FILE = -1,
    CONFIG_ERROR_ENTRY_EXISTS = -2,
    CONFIG_ERROR_NO_SUCH_ENTRY = -3,
    CONFIG_ERROR_FAILED_SANITY = -4,
    CONFIG_ERROR_TABLE_FULL = -5,
} config_error_t;

typedef struct {
    rd_table_t * table;
    char * name;
    config_error_t error;
    char * error_entry;
} config_t;

typedef enum {
    CONFIG_ADD_MODE_UPDATE,
    CONFIG_ADD_MODE_UNIQUE,
} config_add_mode_t;

typedef enum {
    CONFIG_ACCESS_GET,
    CONFIG_ACCESS_CLEAR,
} config_access_type_t;

typedef enum {
    CONFIG_FAIL_ON_INSANE,
    CONFIG_WARN_ON_INSANE,
} config_sanity_t;

char * config_error_str(config_t * c);

int config_find_entry(config_t * c, char * name);

rd_value_t * config_get_value(config_t * c, char * key);

char * config_get_value_str(config_t * c, char * format, ...);
char * config_get_value_strc(config_t * c, char * format, ...);

int config_get_value_int(config_t * c, char * format, ...);
float config_get_value_float(config_t * c, char * format, ...);

int config_add_entry(config_t * c, char * name, char * value, config_add_mode_t mode);

int config_read(config_t * c, char * config_file, char * names);

char * config_dump(config_t *c);

void config_print(config_t *c);

config_t * config_create(void);

config_t * config_create_from_file(char * file, char * names);

int config_free(config_t * c);

config_t * config_access(config_access_type_t mode);

config_t * config_get(config_t * c);

int config_init(char * file, char * names, config_sanity_t sanity);

int config_sanity_check(config_t * c, char * names);

#endif
