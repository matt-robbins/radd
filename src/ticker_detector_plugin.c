#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include "detect.h"
#include "rd_memory.h"

typedef int parameters_t;
typedef struct {
    float * chirp;
    float * buf;
    double sumsq;
    int nchirp;
    int ix;
}state_t;

char * api_version()
{
    static char * v = RD_API_VERSION;
    return v;
}

int detect(event_t * events, sample_t * buf, int nbuf, void * parameters, void * state, sound_setup_t * setup, struct timeval time)
{
    int j, k = 0;
    double corr_m, in_m, corr, mean;
    
    state_t * s = state;
    
    int max_ix = 0;
    double max_corr = 0;
    
    for (k = 0;k < nbuf;k++)
    {
        #ifdef FIXED_POINT
        s->buf[s->ix++] = (float)buf[k] / (float)INT_MAX;
        #else
        s->buf[s->ix++] = buf[k];
        #endif
        if (s->ix == s->nchirp)
            s->ix = 0;
        
        corr_m = 0; in_m = 0; mean = 0;
        
        for (j = 0; j < s->nchirp; j++)
        {
            mean += s->buf[j];
        }
        
        mean = mean / s->nchirp;
        
        for (j = 0; j < s->nchirp; j++)
        {
            int bufix = (j + s->ix + 1) % s->nchirp;
            corr_m += (s->buf[bufix] - mean) * s->chirp[j];
            in_m += (s->buf[j] - mean) * (s->buf[j] - mean);
        }
        
        corr = corr_m / (sqrt(in_m / s->nchirp) * s->nchirp);
        
        if (corr > max_corr)
        {
            max_corr = corr;
            max_ix = k;
        }
    }
        
    if (max_corr < 0.8)
        return 0;
    
    events[0].t_in = max_ix - s->nchirp;
    events[0].t_dt = s->nchirp;
    events[0].f_lo = 0.0;
    events[0].f_df = (float)setup->rate/2.0;
    events[0].score = 10.0;
    events[0].data = NULL;
    sprintf(events[0].label, "tick");
    
    return 1;
}

void * parameter_initialize() 
{
    parameters_t * p = rd_malloc(sizeof(parameters_t));
    printf("in dummy parameter_initialize()\n");
    return p;
}


void * state_initialize(void * parameters, sound_setup_t * ss)
{
    state_t * s = rd_malloc(sizeof(state_t));
    int k;
    
    s->nchirp = 100;
    s->chirp = rd_calloc(s->nchirp, sizeof(sample_t));
    s->buf = rd_calloc(s->nchirp, sizeof(sample_t));
    s->ix = 0;
    
    s->sumsq = 0;
    float  mean = 0;
    for (k = 0; k < s->nchirp; k++)
    {
        s->chirp[k] = sin(M_PI_2 * k * k / s->nchirp);
        mean += s->chirp[k];
    }
    
    mean = mean / s->nchirp;

    for (k = 0; k < s->nchirp; k++)
    {
        s->sumsq += (s->chirp[k] - mean) * (s->chirp[k] - mean);
    }
    
    for (k = 0; k < s->nchirp; k++)
    {
        s->chirp[k] = (s->chirp[k] - mean) / sqrt(s->sumsq / s->nchirp);
    }
        
    return s;
}

sound_setup_t * setup(void * parameters)
{
    static sound_setup_t setup;
    
    setup.rate = 48000;
    setup.N = 512;
    setup.buffer_latency = 1;
    setup.buffer_history = 1;
    
    return &setup;
}

void cleanup(void * parameters, void * state){
    parameters_t * p = (parameters_t *) parameters;
    state_t * s = (state_t *) state;
    
    free(s->buf);
    free(s->chirp);

    free(p); free(s);
}

