
/**\file
 * run_detect convenience functions.
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <detect.h>
#include <limits.h>

/**
 * open a config file
 */

FILE * config_file_open(char * config_file);

/**
 * close a config file
 */

void config_file_close(FILE * fp);

/**
 * get a string value from a config file pointer
 */

int get_parameter_value_str(char * out, FILE * fp, char * p_name);

/**
 * get a float value from a config file pointer
 */

int get_parameter_value_float(float * out, FILE * fp, char * p_name);

/**
 * get an int value from a config file pointer
 */

int get_parameter_value_int(int * out, FILE * fp, char * p_name);
int get_parameter_value_short(short int * out, FILE * fp, char * p_name);

/**
 * get an frac value from a config file pointer
 */

int get_parameter_value_frac(float * out, FILE * fp, char * p_name);
