/**\file
 * functions dealing with the event buffer, a ring buffer of events.
 * each detector gets its own event buffer, which is used to pass events
 * to the event storage methods, which can be heavy with i/o, from the 
 * main detection thread, which is processing-intensive.
 */ 

#include "event_buffer.h"
#include "detector.h"
#include "rd_memory.h"

#include <time.h>

/*
 * private functions
 */

static inline int min(int a, int b);

/**
 * allocate a new event buffer, and initialize the read and write pointers
 * to 0 and the overrun (drop) count to 0.
 */

event_buffer_t * event_buffer_create(size_t N, int realtime)
{
    if (N < 2){
        fprintf(stderr, "can't have an event buffer size < 2\n"); return NULL;
    }
    
    event_buffer_t * eb = rd_calloc(1, sizeof(event_buffer_t));
    
    if (eb == NULL)
        return NULL;
    
    eb->N = N;
    eb->buf = rd_calloc(N, sizeof(rd_event_t));
    
    if (eb->buf == NULL){
        free(eb);
        return NULL;
    }
    
    eb->pr = 0; eb->pw = 0;
    eb->dropped = 0;
    eb->realtime = realtime != 0;
    eb->eof = 0;
    
    pthread_mutex_init(&eb->rcond_lock, NULL);
    pthread_cond_init(&eb->rcond, NULL);
    pthread_mutex_init(&eb->wcond_lock, NULL);
    pthread_cond_init(&eb->wcond, NULL);
    
    return eb;
}

int event_buffer_read_signal(event_buffer_t * b)
{
    pthread_mutex_lock(&b->rcond_lock);
    pthread_cond_signal(&b->rcond);
    pthread_mutex_unlock(&b->rcond_lock);
    return 0;
}

int event_buffer_write_signal(event_buffer_t * b)
{
    pthread_mutex_lock(&b->wcond_lock);
    pthread_cond_signal(&b->wcond);
    pthread_mutex_unlock(&b->wcond_lock);
    return 0;
}

int event_buffer_set_eof(event_buffer_t * b)
{
    b->eof = 1;
    return 0;
}

int event_buffer_read_wait(event_buffer_t * b)
{
    int ret = 0;
    struct timeval tv;
    struct timespec ts;
    int state;
    
    pthread_mutex_lock(&b->rcond_lock);
    
    gettimeofday(&tv, NULL);
    ts.tv_sec = tv.tv_sec + 1;
    ts.tv_nsec = tv.tv_usec * 1000;
    
    if (!event_buffer_read_avail(b))
    {
        ret = 1;
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &state);
        
        if (b->realtime)
            pthread_cond_timedwait(&b->rcond, &b->rcond_lock, &ts);
        else
            pthread_cond_wait(&b->rcond, &b->rcond_lock);
            
        pthread_setcancelstate(state, NULL);
    }
        
    pthread_mutex_unlock(&b->rcond_lock);
    return ret;
}

int event_buffer_write_wait(event_buffer_t * b)
{
    int ret = 0;
    int state;
    pthread_mutex_lock(&b->wcond_lock);
    
    if (!event_buffer_write_avail(b))
    {
        ret = 1;
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &state);
        pthread_cond_wait(&b->wcond, &b->wcond_lock);
        pthread_setcancelstate(state, NULL);

    }
        
    pthread_mutex_unlock(&b->wcond_lock);
    return ret;
}

static int _increment_read_pointer(event_buffer_t * b)
{
    pthread_mutex_lock(&b->wcond_lock);
        
    if (++b->pr >= b->N)
        b->pr = 0;
        
    pthread_cond_signal(&b->wcond);
    pthread_mutex_unlock(&b->wcond_lock);
    
    return b->pr;
}

static int _increment_write_pointer(event_buffer_t * b)
{
    pthread_mutex_lock(&b->rcond_lock);
    
    if (++b->pw >= b->N)
        b->pw = 0;
        
    pthread_cond_signal(&b->rcond);
    pthread_mutex_unlock(&b->rcond_lock);
    
    return b->pw;
}

/** 
 * free storage for an event buffer.
 */

void event_buffer_destroy(event_buffer_t * eb)
{
    int k, n;
    n = event_buffer_read_avail(eb);
        
    for (k = 0; k < n; k++)
    {
        event_free(eb->buf[eb->pr]);
        
        if (++eb->pr >= eb->N)
            eb->pr = 0;
    }
    
    free(eb->buf);
    free(eb);
}

/**
 * get the number of events available for reading in an event buffer.
 */

size_t event_buffer_read_avail(event_buffer_t * eb)
{
    int N = eb->pw - eb->pr;
    
    if (eb->pw == eb->pr)
        return 0;
    
    while (N < 0)
        N += eb->N;
    
    return N;
}

/**
 * returns the number of events available to write to in an
 * event buffer.
 */

size_t event_buffer_write_avail(event_buffer_t * eb)
{
    int N = eb->pr - eb->pw - 1;
    
    while (N < 0)
        N += eb->N;
    
    return N;
}

static inline int min(int a, int b)
{
    
    if (a < b)
        return a;
    else
        return b;
    
}

int event_buffer_check_dropped(event_buffer_t * eb)
{
    int n;
    
    n = eb->dropped;
    eb->dropped = 0;
    
    return n;
}

size_t event_buffer_read(event_buffer_t * eb, rd_event_t * out, size_t N)
{
    int n, k;
    
    if (eb->eof && event_buffer_read_avail(eb) == 0)
    {   
        return -1;
    }
    
    if (!eb->eof)
        event_buffer_read_wait(eb);

    n = min(event_buffer_read_avail(eb), N);
    
    for (k = 0; k < n; k++)
    {
        out[k] = eb->buf[eb->pr];
        _increment_read_pointer(eb);
    }
    
    eb->debug_count -= n;
    
    return n;
}

size_t event_buffer_read_all(event_buffer_t * eb, rd_event_t ** out)
{
    int n = event_buffer_read_avail(eb);
    
    if (n > 0)
        *out = rd_malloc(n * sizeof(rd_event_t));
    
    n = event_buffer_read(eb, *out, n);
    
    return n;
}

/**
 * write N events into the event ringbuffer eb.
 */

size_t event_buffer_write(event_buffer_t * eb, rd_event_t * in, size_t N)
{
    int n, k;
    
    if (eb->realtime)
    {
        n = min(event_buffer_write_avail(eb), N);
        eb->dropped += N - n;
        
        /*
         * copy the events we have space for into the ringbuffer
         */
            
        for (k = 0; k < n; k++)
        {
            eb->buf[eb->pw] = in[k];
            _increment_write_pointer(eb);
        }
        
        /*
         * dump the storage for events that we've dropped
         */
        
        for (k = n; k < N; k++)
            event_free(in[k]);
    }
    
    else
    {
        n = N;
        
        for (k = 0; k < N; k++)
        {
            event_buffer_write_wait(eb);
            eb->buf[eb->pw] = in[k]; _increment_write_pointer(eb);
        }
    }
    
    event_buffer_read_signal(eb);
    
    eb->debug_count += n;
    
    return n;
}

rd_time_t event_buffer_get_time(event_buffer_t * eb)
{
    rd_event_t * e = &eb->buf[eb->pr];
    return e->time;
}

