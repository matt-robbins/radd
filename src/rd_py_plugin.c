#include "rd_py_plugin.h"
#include "rd_log.h"
#include "detect.h"
#include "rd_memory.h"
#include <time.h>

typedef struct rd_py_state_s {
    PyObject *pName, *pModule, 
        *pFuncInit, *pFuncProcess, *pFuncSetup,
        *pArgs, *pValue, *pSetup;
        char * name;
} rd_py_state_t;

static PyObject *makearray(sample_t * data, size_t size) 
{
    npy_intp dims[1];
    dims[0] = size;
    int k;
    
    PyObject * a = PyArray_SimpleNew(1, dims, NPY_FLOAT);
        
    float * fdata = PyArray_DATA((PyArrayObject *) a);
    
    assert(fdata);
    
    for (k = 0; k < size; k++)
    {
        fdata[k] = (float) data[k];
    }
        
    return a;
}

extern int rd_py_detect(event_t * events, sample_t * buf, int nbuf,
    void * parameters, void * state, sound_setup_t * setup, struct timeval time)
{
    Py_ssize_t k;
    int n_events = 0;
    Py_ssize_t pevent_size; 
    rd_py_state_t * s = state;
    
    PyObject * pevents = NULL, * pevent = NULL;

    /*
     * handle Python threading
     */
     
    PyGILState_STATE gstate;
    gstate = PyGILState_Ensure();
    
    /*
     * convert C data into Python objects
     */

    PyObject * data = makearray(buf, nbuf);
    
    struct tm * times = gmtime(&time.tv_sec);
        
    PyDateTime_IMPORT;

    PyObject * pDt = PyDateTime_FromDateAndTime(
        times->tm_year, times->tm_mon, times->tm_mday, times->tm_hour, 
        times->tm_min, times->tm_sec, time.tv_usec);
        
    if (!pDt)
    {
        fprintf(stderr, "datetime creation failed\n");
    }
    
    PyObject * pArgs = PyTuple_New(3);
    PyTuple_SetItem(pArgs, 0, data);
    PyTuple_SetItem(pArgs, 1, s->pValue);
    PyTuple_SetItem(pArgs, 2, pDt);
    
    /*
     * Call detect function
     */
    
    pevents = PyObject_CallObject(s->pFuncProcess, pArgs);
    
    Py_INCREF(s->pValue);
    Py_DECREF(pArgs);
    
    if (!pevents)
    {
        fprintf(stderr, "python function failed to give output!\n"); 
        PyErr_Print(); exit(1);
    }
    
    /*
     * process events
     */
    
    if (!PyTuple_Check(pevents))
        rd_log_message("detect() must return a tuple!\n");
    
    pevent_size = PyTuple_Size(pevents);
    
    for (k = 0; k < pevent_size; k++)
    {
        pevent = PyTuple_GetItem(pevents, k);
        
        int tsize = PyTuple_Size(pevent);
        
        if (!PyTuple_Check(pevent) || tsize < 5)
        {
            rd_log_message("events must be 5- or 6-tuples!\n");
            exit(1);
        }
            
        PyObject * pTin = PyTuple_GetItem(pevent, 0);
        PyObject * pTdt = PyTuple_GetItem(pevent, 1);
        PyObject * pFlo = PyTuple_GetItem(pevent, 2);
        PyObject * pFdf = PyTuple_GetItem(pevent, 3);
        PyObject * pScore = PyTuple_GetItem(pevent, 4);
        PyObject * pLabel = PyTuple_GetItem(pevent, 5);
        
        if (!PyInt_Check(pTin))
        {
            rd_log_message("t_in must be an int\n");
            exit(1);
        }
        
        if (!PyInt_Check(pTdt))
        {
            rd_log_message("t_dt must be an int\n");
            exit(1);
        }
        
        if (!PyFloat_Check(pFlo))
        {
            rd_log_message("f_lo must be a float\n");
            exit(1);
        }
        
        if (!PyFloat_Check(pFdf))
        {
            rd_log_message("f_df must be a float\n");
            exit(1);
        }
        
        if (!PyFloat_Check(pScore))
        {
            rd_log_message("score must be a float\n");
            exit(1);
        }
        
        events[n_events].t_in = PyInt_AsLong(pTin);
        events[n_events].t_dt = PyInt_AsLong(pTdt);
        events[n_events].f_lo = PyFloat_AsDouble(pFlo);
        events[n_events].f_df = PyFloat_AsDouble(pFdf);
        events[n_events].score = PyFloat_AsDouble(pScore);
        
        if (tsize > 5)
        {
            if (!PyString_Check(pLabel))
            {
                rd_log_message("label must be a string\n");
                exit(1);
            }
            
            char * label = PyString_AsString(pLabel);
            sprintf(events[n_events].label, "%s", label);
        }
        
        events[n_events].data = NULL;
        
        n_events++; 
    }
    
    Py_DECREF(pevents);
    
    /*
     * release Python thread
     */
    
    PyGILState_Release(gstate);
    
    return n_events;
}

float rd_py_classify(
    sample_t * buf, int nbuf, int rate, void * parameters, void * state,
    char ** data, char *** labels)
{
    float score = 0;
    
    rd_py_state_t * s = state;
    
    PyGILState_STATE gstate;
    gstate = PyGILState_Ensure();

    PyObject * pydata = makearray(buf, nbuf);
    
    PyObject * poutput = NULL;
    PyObject * prate = PyFloat_FromDouble((double) rate);
    
    if (!pydata || !prate || !s->pValue)
    {
        fprintf(stderr, "pydata = %p, prate = %p, s->pValue = %p\n", pydata,prate,s->pValue);
        exit(1);
    }
    
    s->pArgs = PyTuple_New(3);
    PyTuple_SetItem(s->pArgs, 0, pydata);
    PyTuple_SetItem(s->pArgs, 1, prate);
    PyTuple_SetItem(s->pArgs, 2, s->pValue);
        
    poutput = PyObject_CallObject(s->pFuncProcess, s->pArgs);
    Py_INCREF(s->pValue);
    Py_DECREF(s->pArgs);
        
    if (!poutput)
    {
        rd_log_message("classify() function failed to give output!\n"); 
        PyErr_Print(); exit(1);
    }
    
    if (!PyTuple_Check(poutput))
    {
        rd_log_message("classify() must return a tuple!\n");
        PyErr_Print(); exit(1);
    }
    
    if (PyTuple_Size(poutput) != 3)
    {
        rd_log_message("classify() must return a 3-tuple!\n");
        PyErr_Print(); exit(1);
    }
    
    PyObject * pScore = PyTuple_GetItem(poutput, 0);
    if (!PyFloat_Check(pScore))
    {
        rd_log_message("first output must be a float!\n");
        exit(1);
    }
    score = PyFloat_AsDouble(pScore);    
    
    PyObject * pData = PyTuple_GetItem(poutput, 1);
    
    if (!pData || !PyString_Check(pData))
    {
        rd_log_message("second output must be a string!\n");
        exit(1);
    }
    
    *data = rd_strdup(PyString_AsString(pData));
        
    PyObject * pLabel = PyTuple_GetItem(poutput, 2);
    
    if (!PyString_Check(pLabel))
    {
        rd_log_message("third output must be a string!\n");
        exit(1);
    }
    
    *labels = rd_calloc(2, sizeof(char *));
    *labels[0] = rd_strdup(PyString_AsString(pLabel));;
    
    Py_DECREF(poutput);
    
    PyGILState_Release(gstate);

    return score;
}


extern void * rd_py_state_initialize(void * parameters, sound_setup_t * setup) 
{
    rd_py_state_t * s = parameters;

    PyGILState_STATE gstate;
    gstate = PyGILState_Ensure();
    
    /*
     * setup will be non-NULL for detectors, and NULL for classifiers,
     * Update the setup parameters in the module if necessary.
     */
     
    if (setup)
    {
        int res;
        
        res = PyObject_SetAttrString(s->pModule, "radd_rate", PyInt_FromLong(setup->rate));
        if (res < 0)
        {
            PyErr_Print();
            exit(1);
        }
        res = PyObject_SetAttrString(s->pModule, "radd_N", PyInt_FromLong(setup->N));
        if (res < 0)
        {
            PyErr_Print();
            exit(1);
        }
        res = PyObject_SetAttrString(s->pModule, "radd_history", PyInt_FromLong(setup->buffer_history));
        if (res < 0)
        {
            PyErr_Print();
            exit(1);
        }
        res = PyObject_SetAttrString(s->pModule, "radd_latency", PyInt_FromLong(setup->buffer_latency));
        if (res < 0)
        {
            PyErr_Print();
            exit(1);
        }   
    }   
    
    s->pArgs = PyTuple_New(1);
    PyTuple_SetItem(s->pArgs, 0, s->pName);
    
    s->pValue = PyObject_CallObject(s->pFuncInit, s->pArgs);
            
    if (!s->pValue)
    {
        PyErr_Print();
        rd_log_message("init() returned NULL!\n");
        exit(1);
    }
    
    PyGILState_Release(gstate);
    
    return (void *) parameters;
}

void init_numpy(void)
{
    import_array();
}

static void * rd_py_parameter_initialize_(const char * module_name, char * type)
{
    rd_py_state_t * s = rd_calloc(1,sizeof(rd_py_state_t));
    static int python_initialized = 0;
    
    if (!python_initialized)
    {
        Py_Initialize();
        PyEval_InitThreads();
        PyEval_ReleaseLock();
        python_initialized = 1;
    }
    PyGILState_STATE gstate;
    gstate = PyGILState_Ensure();
    init_numpy();
    
    char * pyargv[1]; pyargv[0] = "radd";
    PySys_SetArgv(1,pyargv);
        
    s->pModule = PyImport_ImportModule(module_name);
    s->pName = PyString_FromString(module_name);
    s->name = rd_strdup(module_name);
    
    if (s->pModule == NULL)
    {
        rd_log_message("couldn't load python module \"%s\"!\n", module_name);
        PyErr_Print();
        exit(1);
    }
        
    s->pFuncInit = PyObject_GetAttrString(s->pModule, "init");
    
    if (!(s->pFuncInit && PyCallable_Check(s->pFuncInit))) {
        rd_log_message("couldn't get init() function from module\n");
        exit(1);
    }
    
    s->pFuncProcess = PyObject_GetAttrString(s->pModule, type);
    
    if (!(s->pFuncProcess && PyCallable_Check(s->pFuncProcess))) {
        rd_log_message("couldn't get %s() function from module\n", type);
        exit(1);
    }
    
    PyGILState_Release(gstate);

    return s;
}


void * rd_py_parameter_initialize_detector(const char * module_name)
{
    return rd_py_parameter_initialize_(module_name, "detect");
}

void * rd_py_parameter_initialize_classifier(const char * module_name)
{
    return rd_py_parameter_initialize_(module_name, "classify");
}

sound_setup_t * rd_py_setup(void * parameters)
{
    static sound_setup_t setup;
    rd_py_state_t * s = (rd_py_state_t *) parameters;

    PyObject * py_rate = PyObject_GetAttrString(s->pModule, "radd_rate");
    PyObject * py_N = PyObject_GetAttrString(s->pModule, "radd_N");
    PyObject * py_history = PyObject_GetAttrString(s->pModule, "radd_history");
    PyObject * py_latency = PyObject_GetAttrString(s->pModule, "radd_latency");

    if (!py_rate || !py_N || !py_history || !py_latency)
    {
        rd_log_message("module must contain integer values for "
            "\"radd_rate\", \"radd_N\",\"radd_history\", and \"radd_latency\"\n");
        exit(1);
    }

    if (PyInt_Check(py_rate))
        setup.rate = PyInt_AsLong(py_rate);
        
    if (PyInt_Check(py_N))
        setup.N = PyInt_AsLong(py_N);
        
    if (PyInt_Check(py_history))
        setup.buffer_history = PyInt_AsLong(py_history);
        
    if (PyInt_Check(py_latency))
        setup.buffer_latency = PyInt_AsLong(py_latency);
    
    Py_DECREF(py_rate);
    Py_DECREF(py_N);
    Py_DECREF(py_history);
    Py_DECREF(py_latency);
    
    return &setup;
}

extern void rd_py_cleanup(void * parameters, void * state)
{
    rd_py_state_t * p = (rd_py_state_t *) parameters;
    
    Py_Finalize();
    
    free(p->name);
    free(p);
}

extern char * rd_py_api_version(void)
{
    static char * v = RD_API_VERSION;
    return v;
}

extern void rd_py_data_free(void * data)
{
    free(data);
}

#ifdef TEST

int main(int argc, char * argv[])
{
    int cnt = 1000;
    
    if (argc < 2)
    {
        fprintf(stderr, "Usage: rd_py_test N.\n  N is the # of iterations.\n\n"  
        "Use high N (e.g. 10000) with top to check memory\n");
        exit(1);
    }
    if (argc > 1)
        cnt = atoi(argv[1]);
        
    fprintf(stderr, "%d\n", cnt);
    
    int i;
    
    event_t events[RD_MAX_EVENTS];
    int nbuf = 1024;
    sample_t * buf = rd_calloc(nbuf, sizeof(sample_t));
    
    for (i = 0; i < 100; i++)
    {
        buf[i] = sin(M_PI_2 * i * i /100);
    }
    
    void * dp;
    void * ds;
        
    dp = rd_py_parameter_initialize_detector("py_tick_check");
    sound_setup_t * setup = rd_py_setup(dp);
    
    ds = rd_py_state_initialize(dp, setup);

    struct timeval time;
    time.tv_sec = 0;
    time.tv_usec = 0;
    
    for (i = 0; i < cnt; i++)
    {
        int res = rd_py_detect(events, buf, nbuf, dp, ds, setup, time);
        fprintf(stderr, "%d: %d, %s\r", i, res, events[0].label);
    }
    
    fprintf(stderr,"\n");

    void * p = rd_py_parameter_initialize_classifier("py_tick_check");
    void * s = rd_py_state_initialize(p, NULL);
    
    for (i = 0; i < cnt; i++)
    {
        char * data = NULL;
        char ** labels = NULL;
        float score = rd_py_classify(buf, nbuf, 48000, p, s, &data, &labels);
        
        fprintf(stderr, "%d: %f [", i, score);
        int j = 0;
        while (labels[j] != NULL)
        {
            fprintf(stderr, "%s,", labels[j]);
            free(labels[j]);
            j++;
        }
        
        free(labels);
        
        fprintf(stderr, "], %s\r", data);
        free(data);
    }
    
    fprintf(stderr, "\n");
    
    free(buf);
    rd_py_cleanup(p, s);
        
    return 0;
}


#endif


