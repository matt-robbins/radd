#include <sys/types.h>
#include <stdint.h>
#include <limits.h>

#define RD_SERVER_DEFAULT_PORT 8421

#define RD_SERVER_MAIN_MAGIC "RDSV"
#define RD_SERVER_QURY_MAGIC "RDQY"
#define RD_SERVER_CONF_MAGIC "RDCF"
#define RD_SERVER_DATA_MAGIC "RDAD"
#define RD_SERVER_PAGE_MAGIC "RDPG"
#define RD_SERVER_PAGE_BREAK '\r'

typedef struct rd_server_main_header_s {
    char magic[4];
    int32_t command;
    int16_t det_num;
    int32_t samplerate;
    int32_t reserved;
} rd_server_main_header_t;

#define RD_SERVER_MAIN_INIT { RD_SERVER_MAIN_MAGIC, 0, 0, 0 }
#define RD_SERVER_CONF_INIT { RD_SERVER_CONF_MAGIC, 0, 0, 0 }

typedef struct rd_server_conf_header_s {
    char magic[4];
    char det_name[PATH_MAX];
    int16_t rate;
    int32_t confsize;
} rd_server_conf_header_t;

typedef struct rd_server_query_header_s {
    char magic[4];
    int32_t qsize;
} rd_server_query_header_t;

typedef struct rd_server_data_header_s {
    char magic[4];
    int32_t valid;
    int16_t samplesize;
    int32_t samplerate;
    uint32_t timestamp_h;
    uint32_t timestamp_l;
    int32_t size;
    int32_t reserved;
} rd_server_data_header_t;

typedef union {
    rd_server_main_header_t m;
    rd_server_conf_header_t c;
    rd_server_data_header_t d;
} rd_server_header_t;




