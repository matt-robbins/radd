/**\file
 * main program for reporting, using the run_detect reporting interface.
 */

#include "rd_util.h"
#include "rd_log.h"
#include "rd_process.h"
#include "configure.h"
#include "config.h"
#include "defaults.h"
#include "rd_memory.h"

#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <stdarg.h>
#include <sys/wait.h>
#include <syslog.h>
#include <time.h>

#ifdef DEFAULT_CONFIG_FILE
    #undef DEFAULT_CONFIG_FILE
    #define DEFAULT_CONFIG_FILE "/tmp/report.conf"
#endif

#define REPORT_FLAG_FILE "/tmp/report"
#define REPORT_DISPATCH "/usr/bin/brpbuoy/report"
#define REPORT_ZIP "/usr/bin/brpbuoy/zip"

#define xstr( s ) stringize(s)
#define stringize( x ) #x

/*
 * this string sets the report precedence
 */

#define MODEMS "cell,gm862,raven,iridium,none"
#define DEFAULT_MODEM "iridium"
#define A_WHILE 1
 
/*
 * global variables
 */
 
struct global_s {
    int ReportInterval, HoldoffInterval, CommandTimeout;
    config_t * Config;
} Global;

/*
 * functions (all static)
 */

static int 
report_attempt(config_t * config, char * modem_override);

/**
 * signal handlers
 */

static void shutdown_handler(int sig)
{
    rd_log_message("recieved signal %d, shutting down.\n", sig);
    config_free(Global.Config);
    exit(0);
}

/**
 * this is the SIGALRM handler, it basically puts the trigger file in the file system
 * when scheduled.
 */

static void alarm_handler(int sig)
{    
    if (sig != SIGALRM)
    {
        fprintf(stderr, "alarm_handler called for non-alarm signal!\n");
        return;
    }
    
    ftouch(REPORT_FLAG_FILE);
    alarm(Global.ReportInterval);
}


static int signal_safe_sleep(int seconds)
{
    fd_set set;
    struct timeval timeout;

    /* Initialize the file descriptor set. */
    FD_ZERO (&set);

    /* Initialize the timeout data structure. */
    timeout.tv_sec = seconds;
    timeout.tv_usec = 0;

    /* select returns 0 if timeout, 1 if input available, -1 if error. */
    return select (FD_SETSIZE, &set, NULL, NULL, &timeout);
}

static int 
run_command_timeout(int timeout, char * command, ...)
{
    int status, result, timed_out = 0, command_pid = 0;
    int sig, wpflags = WNOHANG;
    int count = 0;
    char ** args = rd_malloc(sizeof(char *));
    int nargs = 1;
    va_list arglist;
    
    args[0] = command;
    va_start(arglist, command);
    
    do
    {
        args = rd_realloc(args, ++nargs * sizeof(char *));
        args[nargs - 1] = va_arg(arglist, char *);
    }
    while (args[nargs - 1] != NULL);
    
    va_end(arglist);
    
    if ((command_pid = fork()) < 0)
    {
        free(args);
        rd_log_message("failed to fork() for command.\n");
        return -1;
    }
        
    if (command_pid == 0)
    {
        signal(SIGINT, SIG_DFL);
        execvp(command, args);
        _exit(127);
    }
    
    free(args);
    
    rd_log_message("started command %d, \"%s\"\n", command_pid, command);
    sig = SIGINT;
    
    while ((result = waitpid(command_pid, &status, wpflags)) == 0)
    {
        if ((count++) < timeout)
        {
            signal_safe_sleep(1); continue;
        }
        
        timed_out++;
        
        if (timed_out < 5)
            rd_log_message("command %d timed out, interrupting it\n", command_pid);
        
        else if (timed_out < 10)
        {
            rd_log_message("command %d didn't go down, terminating it\n", command_pid);
            sig = SIGTERM;
        }
        else
        {
            rd_log_message("command %d still didn't go down, killing it\n", command_pid);
            sig = SIGKILL;
            wpflags = 0;
        }
        
        kill(command_pid, sig);
        
        if (wpflags != 0)
            signal_safe_sleep(1);
    }
    
    if (result != command_pid)
        rd_log_message("got surprising pid from waitpid(): %d\n", result);
        
    if (WIFEXITED(status))
        status =  WEXITSTATUS(status);
    
    else if (WIFSIGNALED(status))
        status = -WTERMSIG(status);
    
    return status;
}

char * read_report_file(char * path)
{
    static char line[LINE_MAX] = "";
    int n;
    sprintf(line, "%s", "");
    FILE * fp = fopen(path, "r");
    
    if (!fp)
        return NULL;
    
    n = fscanf(fp, "%s", line);
    fclose(fp);
        
    if (n < 1 || strlen(line) < 2)
        return NULL;
    
    if (!str_ismember(line, config_get_value_str(Global.Config, "modems"), ","))
        return NULL;
    
    return line;
}


static int 
_report_attempt_cb(char * modem, void * arg)
{
    config_t * config = arg;
    int retries, timeout, result, k;
        
    timeout = config_get_value_int(config, "%s:timeout", modem);
    retries = config_get_value_int(config, "%s:retries", modem);
    
    rd_log_message("attempting a report on modem \"%s\" ...\n", modem);
        
    for (k = 0; k <= retries; k++)
    {
        result = run_command_timeout(timeout, REPORT_DISPATCH, modem, NULL);
        
        if (result == 0)
        {
            rd_log_message("\"%s\" report succeeded!\n", modem);
            return -1;
        }
        
        rd_log_message(
            "report command ended with %s %d\n", 
            result < 0 ? "signal" : "error code",
            result < 0 ? -result : result
        );
    }
    
    rd_log_message("too many retries on modem \"%s\"\n", modem);
    return 0;
}

static int 
report_attempt(config_t * config, char * modem_override)
{
    int result;
    char * modems;

    modems = modem_override ? modem_override 
    : config_get_value_str(config, "modems");
    
    /*
     * after discussion, we decided that this really is the right
     * place to put the packet zip.  we can always get call-related stats
     * in real-time from the server.
     */
    
    if (!config_get_value_int(config, "zip_disable"))
    {
        result = run_command_timeout(10, REPORT_ZIP, NULL);
        
        if (result)
            rd_log_message("failed to zip data!\n");
    }
    
    /*
     * retry the report disptacher for each modem
     */
    
    if (token_iterate(NULL, modems, ",", _report_attempt_cb, config) < 0)
        return 0;
    
    return -1;
}

static float 
config_check_range(
    config_t * cfg, char * name, float low, float high, float def)
{
    float value = config_get_value_float(cfg, name);
    char strvalue[20];
    
    if (value < low)
    {
        rd_log_message(
            "\"%s\" is out of range, using default = %f.\n", name, low
        );
        
        sprintf(strvalue, "%f", low);
        config_add_entry(cfg, name, strvalue, CONFIG_ADD_MODE_UPDATE);
        return low;
    }
    
    if (value > high)
    {
        rd_log_message(
            "\"%s\" is out of range, using default = %f.\n", name, high
        );
        
        sprintf(strvalue, "%f", high);
        config_add_entry(cfg, name, strvalue, CONFIG_ADD_MODE_UPDATE);
        return high;
    }
    
    return value;
}

static int 
_config_check_set_cb(char * val, void * set)
{
    int retval = str_ismember(val, (char *) set, ",");
    return retval == 0 ? -1 : 0;    
}

static void
config_check_set(config_t * cfg, char * name, char * set)
{
    int result;
    char * val = config_get_value_str(cfg, name);
    char newval[LINE_MAX];
    
    if (val == NULL)
    {
        config_add_entry(cfg, name, set, CONFIG_ADD_MODE_UNIQUE);
        return;
    }
    
    if ((result = token_iterate(NULL, val, ",", _config_check_set_cb, (void *) set)) <= 0)
    {
        sprintf(newval, "%s,%s", val, set);
        
        config_add_entry(cfg, name, newval, CONFIG_ADD_MODE_UPDATE);
        rd_log_message("configuration value for \"%s\" invalid or not found. Appending defaults.\n", name);
    }
    
    return;
}

static int 
config_update(config_t ** config, char * config_file)
{
    config_t * cfg = *config;
    struct stat stat_buf;
    static time_t config_last_mtime = 0;
    
    /*
     * check to see if configuration file has changed.
     */
    
    if (!stat(config_file, &stat_buf))
    {
        if (config_last_mtime == stat_buf.st_mtime)
            return 0;
    
        config_last_mtime = stat_buf.st_mtime;
        rd_log_message("configuration file changed\n");     
    
        /*
         * reload configuration
         */
    
        config_free(cfg);
        cfg = config_create_from_file(config_file, NULL);
    }
    else 
    {
        if (cfg != NULL)
            return 0;
        
        fprintf(stderr, "couldn't stat config file. did you move it?\n");
        
        config_free(cfg);    
        cfg = config_create();
    }
    
    /*
     * check critical parameters are in range
     */
    
    config_check_set(cfg, "modems", MODEMS);
    
    Global.ReportInterval = config_check_range(
        cfg, "report_period", 0, 86400, DEFAULT_REPORT_PERIOD
    );
    Global.HoldoffInterval = config_check_range(
        cfg, "report_hold", 0, 86400, DEFAULT_REPORT_HOLD
    );
    Global.CommandTimeout = config_check_range(
        cfg, "timeout", 600, 86400, DEFAULT_REPORT_TIMEOUT
    );
    
    config_check_range(cfg, "retries", 1, 10, DEFAULT_REPORT_RETRIES);
        
    alarm(Global.ReportInterval);
    
    *config = cfg;
    return 1;
}

/**
 * call the specified report command on a schedule, and also in response 
 * to a filesystem-based trigger.
 */

int main(int argc, char * argv[])
{
    char * config_file = DEFAULT_CONFIG_FILE;
    char * modem = NULL;
    int result;
    
    signal(SIGALRM, alarm_handler);
    signal(SIGTERM, shutdown_handler);
    signal(SIGINT, shutdown_handler);
    
    if (argc > 1 && strchr(argv[1], 'd'))
        daemonize("report");
        
    result = mkdir(DEFAULT_STORE_PATH, S_IRWXU);
    
    if (result && errno != EEXIST)
        perror("couldn't create data directory.");
    
    result = mkdir(DEFAULT_FORWARD_PATH, S_IRWXU);
    
    if (result && errno != EEXIST)
        perror("couldn't create output directory.");
    
    config_update(&Global.Config, config_file);
    
    if (argc > 1 && strchr(argv[1], 'z'))
        config_add_entry(Global.Config, "zip_disable", "1", CONFIG_ADD_MODE_UPDATE);
    
    raise(SIGALRM);
    
    /*
     * from this point forward, reporting is handled by the
     * SIGALRM handler, which will reschedule itself.
     *
     * if the configuration file changes, reschedule the alarm.
     * This has the effect of starting the new schedule at the 
     * moment the configuration file changes.
     */
    
    while (1)
    {
        config_update(&Global.Config, config_file);
            
        if (!fexist(REPORT_FLAG_FILE))
        {
            sleep(1); continue;
        }
        
        modem = read_report_file(REPORT_FLAG_FILE);
        
       /*
        * Attempt a report.  If it fails, we don't remove the trigger file
        */

        if ((result = report_attempt(Global.Config, modem) != 0))
        {
            rd_log_message(
                "report failed! holding off for %d seconds\n", 
                Global.HoldoffInterval
            );
        }    
        
        remove(REPORT_FLAG_FILE);
        config_update(&Global.Config, config_file);
        
        signal_safe_sleep(Global.HoldoffInterval);
    }
    
    return 0;
}

