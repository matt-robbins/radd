/**\file 
 * Functions dealing with external processes
 */

#include "rd_process.h"
#include "rd_log.h"
#include "rd_util.h"
#include "process_table.h"
#include "rd_memory.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>

/**
 * make the process a daemon
 */

int daemonize(char * name)
{
    extern int RunDetectDaemonized;
    int pid, fd, lfp;
    char lockfile[PATH_MAX];
    char str[NAME_MAX];
    
    if (getppid() == 1) 
        return -1; /* already a daemon */
        
    if ((pid = fork()) < 0) 
        exit(1); /* fork error */
    
    if (pid > 0) 
        exit(0); /* parent exits */
    
    /* child (daemon) continues */
    
    umask(0); /* set newly created file permissions */    
    setsid(); /* obtain a new process group */
    
    RunDetectDaemonized = 1;
    
    /* close all descriptors */
    
    for (fd = getdtablesize(); fd >= 0; fd--) 
        close(fd); 
    
    fd = open("/dev/null", O_RDWR);
    
    dup2(fd, STDIN_FILENO);
    dup2(fd, STDOUT_FILENO);
    dup2(fd, STDERR_FILENO);
    
    /*
     * initialize logging
     */
    
    rd_log_init(name);
    
    /* 
     * record pid to lock file
     */
    
    sprintf(lockfile, "/tmp/%s.lock", name);
    lfp = f_lock(lockfile);
    
    if (lfp < 0){
        rd_log_message("%s already running, %d\n", name, lfp); exit(1);
    }
    
    sprintf(str,"%d\n",getpid());
    int out = write(lfp,str,strlen(str));
    if (out != strlen(str))
    {
        rd_log_message("couldn't write pid to lockfile!\n");
    }
    
    return lfp;
}

int rd_comm_init(rd_comm_duplex_t * p)
{
    if (pipe((int *) &p->child) || pipe((int *) &p->parent))
        return -1;
    return 0;
}

int rd_cue_init(rd_comm_t * p)
{    
    return pipe(p->pfd);
}

void rd_cue_send(rd_comm_t * p)
{
    close(p->pipe.read); 
    int res = write(p->pipe.write, "c", 1);
    if (res != 1)
    {
        rd_log_message("failed to send cue!\n");
    }
    close(p->pipe.write);
}

int rd_cue_wait(rd_comm_t * p)
{
    int err;
    char result;
    
    /*
     * this will block until we call rd_cue_send()
     */
    
    err = read(p->pipe.read, &result, 1); 
    
    if (err < 0)
        fprintf(stderr, "failed to wait for cue\n");
    
    close(p->pipe.read);
    close(p->pipe.write);
    
    return err;
}

int rd_system(char * command, int timeout)
{
    int pid, status;
    rd_comm_t cue;
    
    if (rd_cue_init(&cue) < 0){
        fprintf(stderr, "rd_system couldn't create rd_cue.\n"); return -1;
    }
    
    if ((pid = fork()) < 0)
        return pid;
    
    /* 
     * child waits for parent, then runs the command, and returns exit 
     * code 127 if execl fails.
     */
    
    if (pid == 0)
    {
        rd_cue_wait(&cue);
            
        execl("/bin/sh", "sh", "-c", command, (char *) 0);
        _exit(127);
    }
        
    /*
     * parent adds process to table so that the sigchld handler knows what do
     * do, then signal the child to go ahead.
     */
    
    status = process_table_add(pid, "rd_system", -1, -1);
    
    if (status)
    {
        fprintf(stderr, "couldn't add process table entry for rd_system.\n");
        return -1;
    }
    
    rd_cue_send(&cue);
    status = process_table_get_status(pid, timeout);
    
    if (status >= 0)
        return status;
    
    if (status == STATUS_ERROR)
    {
        rd_log_message("couldn't get status for rd_system() call\n");
    }
    
    if (status == STATUS_TIMEOUT)
    {
        rd_log_message("rd_system timing out, killing the process\n");
        kill(pid, SIGKILL);
    }
    
    return 0;
}

/**
 * run a command using fork() and exec() instead of system, to make sure that
 * we trap the SIGCHILD signal and act appropriately.
 */

rd_pipe_t * rd_popen(char * command, const char * mode)
{
    int pid, status, direction, devnull;
    int pfd[2];
    rd_pipe_t * p = NULL;
    rd_comm_t cue;

    if (strcmp(mode, "r") && strcmp(mode, "w"))
        return NULL;
    
    direction = !strcmp(mode, "w");
    
    if (rd_cue_init(&cue) < 0)
    {
        fprintf(stderr, "rd_popen couldn't create rd_cue.\n"); return NULL;
    }
    
    /*
     * create pipe
     */
    
    if ((status = pipe(pfd)) != 0){
        perror("couldn't open pipe.\n");
        return NULL;
    }
    
    /*
     * fork() and return a NULL stream if we fail
     */
    
    if ((pid = fork()) < 0)
        return NULL;
    
    /* 
     * parent waits for child to finish
     */
    
    if (pid > 0)
    {
        status = process_table_add(pid, "rd_popen", -1, -1);
        
        if (status){
            fprintf(stderr, "couldn't add process table entry for rd_popen.\n");
        }
        
        rd_cue_send(&cue);
        
        close(pfd[!direction]);
        
        p = rd_calloc(1, sizeof(rd_pipe_t));
        p->fp = fdopen(pfd[direction], mode);
        p->pid = pid;
    
        return p;
    }
    
    rd_cue_wait(&cue);
    
    /* 
     * child runs the command, and returns exit code 127 if exec fails.
     */
        
    close(pfd[direction]);
    devnull = open("/dev/null", O_RDWR, 0);
    
    dup2(pfd[!direction], STDOUT_FILENO);
    dup2(devnull, STDERR_FILENO);
    
    execl("/bin/sh", "sh", "-c", command, (char *) 0);
    _exit(127);
}

/**
 * close a pipe opened with rd_popen()
 */

int rd_pclose(rd_pipe_t * p, int timeout)
{
    int status;
    
    fclose(p->fp);
    status = process_table_get_status(p->pid, timeout);
    free(p);
    
    return status;
}
