#include <signal.h>
#include <pthread.h>
#include "process_table.h"

int rd_setup_signals(void (*handler)(int));

void rd_raise(int sig);
