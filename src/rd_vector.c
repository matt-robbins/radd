#include "rd_vector.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "rd_memory.h"

rd_vector_t * rd_vector_create(int nrows, int ncols)
{
    rd_vector_t * v = rd_malloc(sizeof(rd_vector_t));
    
    v->data = rd_calloc(nrows*ncols, sizeof(rd_value_t));
    v->nrows = nrows; v->ncols = ncols;
    
    return v;
}

void rd_vector_free(rd_vector_t * v)
{
    if (!v)
        return;
    
    free(v->data);
    free(v);
}

int rd_vector_assign(rd_vector_t * v, int ix, int N, ...)
{
    int k;
    va_list arglist;
    va_start(arglist, N);
        
    if (v->nrows * v->ncols < ix + N)
        return -1;
        
    for (k = ix; k < ix + N; k++)
    {        
        v->data[k].type = va_arg(arglist, int);
        
        switch (v->data[k].type)
        {
            case RD_TYPE_INT:
                v->data[k].d.i = va_arg(arglist, int);
                break;
            case RD_TYPE_DBL:
                v->data[k].d.f = va_arg(arglist, double);
                break;
            case RD_TYPE_STR:
                v->data[k].d.s = va_arg(arglist, char *);
                break;
        }
    }
    
    va_end(arglist);
    return 0;
}
