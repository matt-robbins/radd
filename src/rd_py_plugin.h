#ifndef RD_PY_PLUGIN_H
#define RD_PY_PLUGIN_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <radd/detect.h>
#include <Python.h>
#include <datetime.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

#include "detect.h"

int rd_py_detect(event_t * events, sample_t * buf, int nbuf, void * p, 
    void * s, sound_setup_t * setup, struct timeval time);
    
float rd_py_classify(
    sample_t * buf, int nbuf, int rate, void * p, void * s,
    char ** data, char *** labels);
    
void * rd_py_state_initialize(void * parameters, sound_setup_t * setup);

void * rd_py_parameter_initialize_classifier(const char * module_name);
void * rd_py_parameter_initialize_detector(const char * module_name);

extern void rd_py_cleanup(void * parameters, void * state);
extern char * rd_py_api_version(void);

sound_setup_t * rd_py_setup(void * parameters);   

extern void rd_py_data_free(void * data);

#endif
