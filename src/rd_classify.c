#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include <sqlite3.h>
#include <sndfile.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <assert.h>
#include <unistd.h>
#include "rd_sndfile.h"
#include "rd_memory.h"

#include "classifier.h"

static int file_classify(
    float * score, classifier_t * c, char * file, char ** data, char *** labels)
{
    SF_INFO info;
    memset(&info, 0, sizeof(info));
    SNDFILE * fp = sf_open(file, SFM_READ, &info);
    
    if (!fp)
    {
        fprintf(stderr,"couldn't open file! %s\n", file);
        char cwd[PATH_MAX];
    
        char * cd = getcwd(cwd, PATH_MAX - 1);
        if (cd)
            fprintf(stderr, "current dir = %s\n", cwd);
            
        return -1;
    }
    
    int N = info.frames;
    
    sample_t * buf = rd_malloc(N * sizeof(sample_t));
    
    sf_readf_fcn(fp, buf, N);
    sf_close(fp);
    
    *score = classifier_run(c, buf, N, info.samplerate, labels, data);
    free(buf);
    
    return 0;
}

static int rd_db_classify(sqlite3 * db, classifier_t * c)
{
    int result;
    char * err;
    extern int errno;
    
    result = sqlite3_exec(db, 
        "CREATE TABLE IF NOT EXISTS event_classes ("
        "id INTEGER PRIMARY KEY, event_id INTEGER,"
        "classifier TEXT, label TEXT, data TEXT, score REAL);",
        NULL, NULL, &err);
        
    if (result != SQLITE_OK)
    {
        fprintf(stderr, "couldn't create event_classes table! %s\n", err);
        return 1;
    }
            
    char * sql;
    int n = asprintf(&sql, "SELECT id,file FROM events "
        "WHERE id NOT IN ("
        "SELECT event_id FROM event_classes WHERE classifier = '%s') "
        "AND file IS NOT NULL;", c->name);
    
    assert(n >= 0);
    
    char ** table;
    int nr, nc;
    
    result = sqlite3_get_table(db, sql, &table, &nr, &nc, &err);
    free(sql);
    
    if (result != SQLITE_OK)
    {
        fprintf(stderr, "failed to get list of files...\n");
        return 1;
    }
    
    assert(nc == 2);
    
    char * isql = "INSERT INTO event_classes "
        "(event_id, classifier, label, data, score) VALUES "
        "(?,?,?,?,?);";
    
    char * usql = "UPDATE events SET score = ("
    "SELECT max(score) FROM event_classes WHERE event_id = ?) "
    "WHERE events.id = ?;";
        
    sqlite3_stmt * istmt;
    sqlite3_prepare_v2(db, isql, -1, &istmt, NULL);
    
    sqlite3_stmt * ustmt;
    sqlite3_prepare_v2(db, usql, -1, &ustmt, NULL);
    
    int k, ix;
    
    char * data = NULL;
    char ** labels = NULL;
    float score;
    
    result = sqlite3_exec(db, "BEGIN;", NULL, NULL, &err);
        
    if (result != SQLITE_OK)
    {
        fprintf(stderr, "couldn't begin transaction! %s\n", err);
        return 1;
    }
    
    for (k = 1; k < nr; k++)
    {
        char * fname = table[k * nc + 1];
        int event_id = atoi(table[k * nc]);
        
        fprintf(stderr, "classifying %s, id: %d\n", fname, event_id);

        result = file_classify(&score, c, fname, &data, &labels);
        if (result)
        {
            fprintf(stderr, "failed to load file %s\n", fname);
            continue;
        }
            
        sqlite3_bind_int(istmt, 1, event_id);
        sqlite3_bind_text(istmt, 2, c->name, -1, SQLITE_TRANSIENT);
        if (labels)
            sqlite3_bind_text(istmt, 3, labels[0], -1, SQLITE_TRANSIENT);
            
        sqlite3_bind_text(istmt, 4, data, -1, SQLITE_TRANSIENT);
        free(data);
        
        sqlite3_bind_double(istmt, 5, (double) score);
        
        sqlite3_step(istmt);
        sqlite3_reset(istmt);
        
        if (labels)
        {
            for (ix = 0; labels[ix] != NULL; ix++)
                free(labels[ix]);
            
            free(labels);
        }
        
        sqlite3_bind_int(ustmt, 1, event_id);
        sqlite3_bind_int(ustmt, 2, event_id);
        
        sqlite3_step(ustmt);
        sqlite3_reset(ustmt);
    }
    
    result = sqlite3_exec(db, "COMMIT;", NULL, NULL, &err);
    
    sqlite3_free_table(table);
    
    sqlite3_finalize(istmt);
    sqlite3_finalize(ustmt);
    
    return 0;
}

int main(int argc, char * argv[])
{
    char * path = "/tmp/save";
    char * dbname = "dump.db";    
    char * cpath = "/var/lib/classifiers/";
    char * cname = "other";
    char * fname = NULL;
    int train = 0;
    int result;
    sqlite3 * db;
    extern int errno;
    
    if (argc > 1)
        cname = argv[1];
    if (argc > 2)
        fname = argv[2];
    if (argc > 3)
        train = 1;
    
    classifier_t * c = classifier_create(cpath, cname);
    
    if (NULL == c)
    {
        fprintf(stderr, "couldn't create classifier \"%s\"\n", cname);
        exit(1);
    }
    
    result = classifier_initialize(c);
    
    if (result != 0)
    {
        fprintf(stderr, "failed to initialize classifier!\n"); exit(1);
    }
    
    if (fname)
    {
        char ** labels;
        char * data = NULL;
        float score;
        
        char line[LINE_MAX];
        
        while (fgets(line, sizeof(line), stdin)) 
        {
            char * ret = strrchr(line,'\n'); *ret = '\0';

            int res = file_classify(&score, c, line, &data, &labels);
            
            if (res != 0)
                break;
            
        
            if (train)
                fprintf(stdout, "%s %f\n", labels[0], score);
            else
                fprintf(stdout, "%f\n", score);
            
        }
        
        exit(0);
    }

    result = chdir(path);
    
    if (result != 0)
    {
        fprintf(stderr, "couldn't cd to data directory: %s\n", strerror(errno));
    }    
    
    result = sqlite3_open_v2(dbname, &db, SQLITE_OPEN_READWRITE, NULL);
    
    if (result != SQLITE_OK)
    {
        fprintf(stderr, "failed to open db: %d\n", result);
        exit(1);
    }
    
    rd_db_classify(db, c);
        
    sqlite3_close(db);
    exit(0);
}
