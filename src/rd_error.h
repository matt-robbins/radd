#ifndef RD_ERROR_H
#define RD_ERROR_H

#include <syslog.h>

#define RD_LOG_NAME run_detect

typedef enum {
    RD_ERROR_NO_ERROR = 0,
    RD_ERROR_FILE = -1,
    RD_ERROR_SYSTEM = -2,
    RD_ERROR_PLUGIN = -3,
    RD_ERROR_DB = -4,
    RD_ERROR_GPS = -5,
    RD_ERROR_IO = -6,
    RD_ERROR_LOCK = -7,
    RD_ERROR_MALLOC = -8,
    RD_ERROR_MISC = -9,
} rd_error_t;

char * rd_error_str(rd_error_t err);
int rd_log_error(char * message, rd_error_t err);

#endif
