#ifndef __CONFIG_STORE_H
#define __CONFIG_STORE_H

#include "rd_db.h"
#include "configure.h"

int config_store(
    rd_db_t * db, config_t * cfg, char * spec, char ** names);

#endif
