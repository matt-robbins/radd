#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/time.h>
#include <math.h>

#include "rd_memory.h"
#include "config.h"

#if HAVE_LIBJACK==1

#include <jack/jack.h>

typedef struct _thread_info {
    jack_client_t *client;
    unsigned int channels;
    int usec_offset;
    int rate;
    int click_on;
} jack_thread_info_t;

/* JACK data */
unsigned int nports;
jack_port_t **ports;
jack_default_audio_sample_t **in;
jack_nframes_t nframes;

int
process (jack_nframes_t nframes, void *arg)
{
    int chn, k;
    jack_thread_info_t * info = (jack_thread_info_t *) arg;
    struct timeval tv;
    
    gettimeofday(&tv, NULL);
    int offset = jack_frames_since_cycle_start(info->client);
    int frames_left = (info->rate * (1000000 - tv.tv_usec) / 1000000 - offset) % 1000000;
    
    if (frames_left < nframes)
    {
        fprintf(stderr, "tick: %ld.%.6d\n", tv.tv_sec+1, 0);
    }
    
    for (chn = 0; chn < nports; chn++)
    {
        in[chn] = jack_port_get_buffer (ports[chn], nframes);
    }
    
    for (k = 0; k < nframes; k++)
    {
        
        for (chn = 0; chn < nports; chn++)
        {
            if (info->click_on)
                in[chn][k] = ((float) sin(M_PI_2 * info->click_on * info->click_on / 100));
            else
                in[chn][k] = 0.0;
        }   
        
        if (info->click_on)
            info->click_on++;
            
        if (info->click_on > 100)
            info->click_on = 0;
            
        if (k == frames_left)
            info->click_on = 1;
    }
        

    return 0;
}

void
jack_shutdown (void *arg)
{
    fprintf (stderr, "JACK shutdown\n");
    // exit (0);
    abort();
}

void
setup_ports (int sources, char *source_names[], jack_thread_info_t *info)
{
    unsigned int i;
    size_t in_size;

    /* Allocate data structures that depend on the number of ports. */
    nports = sources;
    ports = (jack_port_t **) malloc (sizeof (jack_port_t *) * nports);
    in_size =  nports * sizeof (jack_default_audio_sample_t *);
    in = (jack_default_audio_sample_t **) malloc (in_size);

    /* When JACK is running realtime, jack_activate() will have
     * called mlockall() to lock our pages into memory.  But, we
     * still need to touch any newly allocated pages before
     * process() starts using them.  Otherwise, a page fault could
     * create a delay that would force JACK to shut us down. */
    
    memset(in, 0, in_size);

    for (i = 0; i < nports; i++) 
    {
        char name[64];

        sprintf (name, "input%d", i+1);

        if ((ports[i] = jack_port_register (info->client, name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0)) == 0) {
            fprintf (stderr, "cannot register input port \"%s\"!\n", name);
            jack_client_close (info->client);
            exit (1);
        }
    }

    for (i = 0; i < nports; i++) 
    {
        if (jack_connect (info->client, jack_port_name (ports[i]), source_names[i])) 
        {
            fprintf (stderr, "cannot connect input port %s to %s\n", jack_port_name (ports[i]), source_names[i]);
            jack_client_close (info->client);
            exit (1);
        } 
    }
}

int
main (int argc, char *argv[])
{
    jack_client_t *client;
    jack_thread_info_t thread_info;
    int c;
    int longopt_index = 0;
    extern int optind, opterr;
    int show_usage = 0;
    char *optstring = "n:f:b:B:hF";
    struct option long_options[] = {
        { "help", 0, 0, 'h' },
        { "repetitions", 1, 0, 'n' },
        { "bitdepth", 1, 0, 'b' },
        { "bufsize", 1, 0, 'B' },
        { 0, 0, 0, 0 }
    };
    
    memset (&thread_info, 0, sizeof (thread_info));
    opterr = 0;

    while ((c = getopt_long (argc, argv, optstring, long_options, &longopt_index)) != -1) {
        switch (c) {
        case 1:
            /* getopt signals end of '-' options */
            break;

        case 'h':
            show_usage++;
            break;
        default:
            fprintf (stderr, "error\n");
            show_usage++;
            break;
        }
    }
    
    jack_options_t options = JackNullOption | JackNoStartServer;
    jack_status_t status;

    if ((client = jack_client_open ("ticker", options, &status)) == 0) {
        fprintf (stderr, "jack server not running?\n");
        exit (1);
    }

    thread_info.client = client;
    thread_info.channels = argc - optind;

    jack_set_process_callback (client, process, &thread_info);
    jack_on_shutdown (client, jack_shutdown, &thread_info);

    if (jack_activate (client)) {
        fprintf (stderr, "cannot activate client");
    }
    
    thread_info.click_on = 0;
    thread_info.rate = jack_get_sample_rate(client);
    fprintf(stderr, "rate = %d\n", thread_info.rate);

    setup_ports (argc - optind, &argv[optind], &thread_info);
    
    jack_transport_start(client);

    pause();
        
    jack_transport_stop(client);
    jack_client_close (client);

    exit (0);
}

#else
int main(int argc, char * argv[])
{
    fprintf(stderr, "RADd built without JACK support.  Ticker disabled\n");
    return 0;
}

#endif
