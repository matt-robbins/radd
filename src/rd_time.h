#ifndef RD_TIME_T
#define RD_TIME_T

#include <sys/time.h>
#include <time.h>
#include <sys/types.h>

#define ONE_SEC_USECS 1000000
#define ONE_SEC_USECS_F 1000000.0
#define ONE_SEC_USECS_L 1000000ll

/**
 * time value containing the real time expressed in microseconds
 */

typedef int64_t rd_time_t;
typedef int rd_timediff_t;

typedef struct {
    float b;
    float c;
    float e;
    float e2;
    rd_time_t t0;
    rd_time_t t1;
    rd_timediff_t diff;
    rd_time_t per;
} rd_dll_t;

rd_time_t rd_get_time(void);
struct timeval rd_time_to_timeval(rd_time_t t);
rd_time_t timeval_to_rd_time(struct timeval t);
rd_time_t rd_samples_to_time(int samples, int rate);
int rd_time_to_samples(rd_time_t t, int rate);

void rd_dll_init(rd_dll_t * d, rd_time_t t, float bw);
rd_time_t rd_dll_update(rd_dll_t * d, rd_time_t in);
rd_time_t rd_dll_reset(rd_dll_t * d, rd_time_t t_in);

#endif
