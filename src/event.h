#include "detect.h"
#include "rd_time.h"

typedef struct rd_event_s {
    double score;
    rd_time_t time;
    rd_time_t duration;
    double freq;
    double bandwidth;
    int realtime;
    int rate;
    char * detector_name;
    char * detector_data;
    char * class_label;
    sample_t * samples;
    sample_t * padded_samples;
    int n_samples;
    int n_padded_samples;
} rd_event_t;

/**
 * free storage for an event
 */

void event_free(rd_event_t e);
int event_to_xml(char * buf, size_t l, rd_event_t * event);
int xml_to_event(rd_event_t * e, char * event_str);
