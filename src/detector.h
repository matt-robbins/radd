
#ifndef RD_DETECTOR_H
#define RD_DETECTOR_H

#include <dlfcn.h>
#include <limits.h>

#include "sound_buffer.h"
#include "detect.h"
#include "configure.h"
#include "event_buffer.h"
#include "rd_audio_driver.h"

/**
 * a detector "object" containing data and API function handles
 */

typedef struct {
    char * name;
    char * file_name;
    char * config_file;
    char * base_path;
    char * path;
    void * lib_handle;
    void * parameters;
    void * state;
    void * decimator;
    sample_t * buffer;
    sound_setup_t sound_setup;
    event_t events[RD_MAX_EVENTS];
    config_t * config;
    struct pad {
        int pre;
        int post;
    } pad;
    rd_parameter_initialize_fcn parameter_initialize;
    rd_state_initialize_fcn state_initialize;
    rd_detect_fcn detect;
    rd_cleanup_fcn cleanup;
    rd_setup_fcn setup;
    rd_data_to_str_fcn data_to_str;
    rd_data_free_fcn data_free;
    rd_api_version_fcn api_version;
    rd_version_fcn version;
} detector_t;

/**
 * additional event data for run_detect system use. not meant to be messed with
 * within a detector plugin.
 */

typedef struct {
    struct timeval time;
    int rate;
    sample_t * samples;
    int n_samples;
    char * author;
    detector_t * detector;
} event_info_t;

/**
 * create a new detector "object"
 */

detector_t * detector_create(char * path, char * name, char * config_file_override);

/**
 * free storage for a detector
 */

void detector_destroy(detector_t * detector);

/**
 * get audio engine params from detector
 */

int detector_sound_setup(detector_t * detector);

/**
 * free storage from detector
 */
 
void detector_cleanup(detector_t * detector);

/**
 * Low-level detector run.  Does not store any sample data or information about events
 */

int detector_run_buffer(detector_t * detector, event_t * events, sample_t * buf, int N, rd_time_t time);

/**
 * run the detector on a buffer of sound, and output events to the event buffer
 */

int detector_run(detector_t * detector, event_buffer_t * event_buffer, sound_buffer_t * sound_buffer);

/**
 * set up everything necessary for detector to run.
 */

int detector_initialize(detector_t * detector);

/**
 * convert detector-level sample-referenced events to absolute-time, 
 * serializable system-wide events
 */

int detector_event_convert(detector_t * det, event_t * de, rd_event_t * event, int N, rd_time_t ref);


#endif
