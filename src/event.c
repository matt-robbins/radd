#include "event.h"
#include "detect.h"
#include "rd_vector.h"
#include "rd_memory.h"

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

int event_to_xml(char * buf, size_t l, rd_event_t * event)
{
    int n;
    
    static char * templ = 
        "<event><author>%s</author><score mat=\"1 1\">%f</score>"
        "<time mat=\"1 2\">%f %f</time>"
        "<freq mat=\"1 2\">%f %f</freq><data>%s</data></event>";
    
    n = snprintf(buf, l, templ, 
        event->detector_name, event->score, (double) event->time / 1e6, 
        (double) (event->time + event->duration) / 1e6,
        event->freq, event->freq + event->bandwidth, event->detector_data
    );
        
    if (n > l)
        return -1;
    
    return n;
}

#if HAVE_MXML_H==1
#include <mxml.h>

static rd_vector_t * xml_mat_to_vec(mxml_node_t * node, int rows, int cols)
{
    int r,c,k,n;
    char * attr = (char *) mxmlElementGetAttr(node->parent, "mat");
    rd_vector_t * out;

    if (attr == NULL)
    {
        printf("xml node is not a \"mat\" node\n");
        return NULL;
    }
    
    if ((n = sscanf(attr, "%d %d", &r, &c)) != 2)
    {
        printf("unable to parse element size!\n");
        return NULL;
    }
    
    if (r != rows || c != cols)
    {
        fprintf(stderr, "xml value is the wrong size!\n");
        return NULL;
    }
    
    out = rd_vector_create(rows, cols);
    
    for (k = 0; k < rows*cols; k++)
    {
        rd_vector_assign(out, k, 1, RD_TYPE_DBL, atof(node->value.text.string));
    
        if ((node = node->next) == NULL)
            break;
    }
    
    return out;
}

int mxml_to_event(rd_event_t * e, mxml_node_t * xml)
{
    int exit = 0;
    
    float score = 0.0;
    char * author = NULL;
    char data[LINE_MAX];

    mxml_node_t *node, *event = xml;
    rd_vector_t * time = NULL, * freq = NULL;
    
    node = mxmlFindElement(event, xml, "time", NULL, NULL, MXML_DESCEND_FIRST);
    
    if (node->child != NULL)
        time = xml_mat_to_vec(node->child, 1, 2);
    
    node = mxmlFindElement(event, xml, "freq", NULL, NULL, MXML_DESCEND_FIRST);
    
    if (node != NULL)    
        freq = xml_mat_to_vec(node->child, 1, 2);
    
    node = mxmlFindElement(event, xml, "score", NULL, NULL, MXML_DESCEND_FIRST);
    
    if (node != NULL)
        score = atof(node->child->value.text.string);
    
    node = mxmlFindElement(event, xml, "author", NULL, NULL, MXML_DESCEND_FIRST);
    
    if (node != NULL)
        author = node->child->value.text.string;
    
    node = mxmlFindElement(event, xml, "data", NULL, NULL, MXML_DESCEND_FIRST);

    if (node != NULL)
        mxmlSaveString(node->child, data, sizeof(data), MXML_NO_CALLBACK);
    
    if (time == NULL || freq == NULL || author == NULL)
    {
        fprintf(stderr, "failed to parse event!\n");
        exit = -1;
        goto done;
    }

    e->time = (rd_time_t) (time->data[0].d.f * 1e6);
    e->duration = (rd_time_t) ((time->data[1].d.f - time->data[0].d.f) * 1e6);
    
    e->freq = freq->data[0].d.f;
    e->bandwidth = freq->data[1].d.f - freq->data[0].d.f;
    
    e->detector_name = = rd_strdup(author);
    e->score = score;
    
    e->detector_data = = rd_strdup(data);
    
done:
    
    rd_vector_free(time);
    rd_vector_free(freq);
    
    return exit;
}


int xml_to_event(rd_event_t * e, char * event_str)
{
    int exit = 0;
    float score = 0.0;
    char * author = NULL;
    mxml_node_t *xml;    
    mxml_node_t *event;  
    mxml_node_t *parent;  
    mxml_node_t *node;  
    rd_vector_t * time = NULL, * freq = NULL;
    char data[LINE_MAX];

    xml = mxmlNewXML("1.0");
    
    parent = mxmlLoadString(xml, event_str, MXML_NO_CALLBACK);
    event = mxmlFindElement(parent, xml, "event", NULL, NULL, MXML_DESCEND);
    
    if (NULL == event)
        return -1;
    
    node = mxmlFindElement(event, xml, "time", NULL, NULL, MXML_DESCEND_FIRST);
    
    if (node->child != NULL)
        time = xml_mat_to_vec(node->child, 1, 2);
    
    node = mxmlFindElement(event, xml, "freq", NULL, NULL, MXML_DESCEND_FIRST);
    
    if (node != NULL)    
        freq = xml_mat_to_vec(node->child, 1, 2);
    
    node = mxmlFindElement(event, xml, "score", NULL, NULL, MXML_DESCEND_FIRST);
    
    if (node != NULL)
        score = atof(node->child->value.text.string);
    
    node = mxmlFindElement(event, xml, "author", NULL, NULL, MXML_DESCEND_FIRST);
    
    if (node != NULL)
        author = node->child->value.text.string;
    
    node = mxmlFindElement(event, xml, "data", NULL, NULL, MXML_DESCEND_FIRST);

    if (node != NULL)
        mxmlSaveString(node->child, data, sizeof(data), MXML_NO_CALLBACK);
    
    if (time == NULL || freq == NULL || author == NULL)
    {
        fprintf(stderr, "failed to parse event!\n");
        exit = -1;
        goto done;
    }

    e->time = (rd_time_t) (time->data[0].d.f * 1e6);
    e->duration = (rd_time_t) ((time->data[1].d.f - time->data[0].d.f) * 1e6);
    
    e->freq = freq->data[0].d.f;
    e->bandwidth = freq->data[1].d.f - freq->data[0].d.f;
    
    e->detector_name = = rd_strdup(author);
    e->score = score;
    
    e->detector_data = = rd_strdup(data);
    
done:
    
    rd_vector_free(time);
    rd_vector_free(freq);
    
    mxmlDelete(parent);
    mxmlDelete(xml);
    
    return exit;
}

#else



#endif

/**
 * free all storage associated with an event
 */

void event_free(rd_event_t e)
{
    free(e.detector_name);
    free(e.detector_data); 
    free(e.padded_samples);
}


#ifdef TEST

int main(int argc, char * argv[])
{
    rd_event_t e;
    char buf[LINE_MAX];
    int k;
    
    for (k = 0; k < 20; k++)
    {
        xml_to_event(&e, 
            "<event><author>dgmr</author><score mat=\"1 1\">5.000000</score><time mat=\"1 2\">22166.848000 22167.680000</time><freq mat=\"1 2\">15.625000 70.312500</freq><data><t mat=\"11 1\">0.000000 0.064000 0.128000 0.256000 0.320000 0.448000 0.512000 0.576000 0.640000 0.704000 0.768000</t><f_lo mat=\"11 1\">62.500000 62.500000 62.500000 46.875000 54.687500 39.062500 23.437500 15.625000 39.062500 54.687500 62.500000</f_lo><f_hi mat=\"11 1\">62.500000 62.500000 70.312500 62.500000 54.687500 62.500000 70.312500 54.687500 54.687500 62.500000 62.500000</f_hi><f_pk mat=\"11 1\">62.500000 62.500000 70.312500 46.875000 54.687500 39.062500 23.437500 15.625000 39.062500 54.687500 62.500000</f_pk><a_pk mat=\"11 1\">0.000072 0.000081 0.000586 0.000500 0.000112 0.002840 0.020227 0.035117 0.002885 0.000364 0.000100</a_pk></data></event>"
        );
        
        fprintf(stderr, "time = %lld\n", e.time);
        
        event_to_xml(buf, sizeof(buf), &e);
        
        printf("event: %s\n", buf);
        
        free(e.detector_data);
        free(e.detector_name);
    }
    
    return 0;
    
}

#endif
