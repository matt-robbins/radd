/**\file
 * this file contains functions related to running a detector process.
 */
 
#include "detect.h"
#include "defaults.h"
#include "configure.h"
#include "classifier.h"
#include "event_store.h"
#include "event_buffer.h"
#include "process_table.h"
#include "rd_process.h"
#include "report.h"
#include "run_detect.h"
#include "rd_error.h"
#include "rd_log.h"
#include "rd_util.h"
#include "rd_audio_driver.h"
#include "rd_memory.h"

#include <assert.h>

/**
 * process data in a non-realtime thread. 
 */

static void * detector_thread (void *arg)
{
    detector_thread_info_t * info = (detector_thread_info_t *) arg;
    int result;
    int count = 0;
    
    int oldstate, oldtype;
    
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldstate);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldtype);
    
    /*
     * run data through detector when we have it
     */
    
    while (RD_TRUE) 
    {        
        if (info->sound_buffer->eof && sound_buffer_read_space(info->sound_buffer) == 0)
        {
            info->event_buffer->eof = 1;
            event_buffer_read_signal(info->event_buffer);
            break;
        }
        
        if (!info->sound_buffer->eof)
            sound_buffer_read_wait(info->sound_buffer);

        result = detector_run(
            info->detector, info->event_buffer, info->sound_buffer
        );
        count+=result;
        
        if (result != RD_ERROR_NO_ERROR)
            rd_log_message("*** Dropped %d events ****\n", count);
    }
    
    pause();
    return NULL;
}

/**
 * thread for storing events to database/filesystem. This is done in a separate
 * thread since it involves i/o delays that can be multiplexed with the detector's 
 * processing.
 */
 
static void
store_cleanup(void *arg)
{
    db_free();
}

static void * store_thread (void * arg)
{
    detector_thread_info_t * info = arg;
    int result, oldstate;
    
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldstate);
    
    pthread_cleanup_push(store_cleanup, NULL);
    
    while (1)
    {
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldstate);
        pthread_testcancel();
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldstate);
        
        if ((result = write_events(info->event_buffer, info->detector, info->classifier)) != RD_ERROR_NO_ERROR)
        {
            if (result == RD_ERROR_IO)
            {
                fprintf(stderr, "event buffer is EOF!\n");
                pthread_kill(info->main_tid, SIGTERM);
                break;
            }
            
            rd_log_error("failed to write events", result);
        }
    }
    
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldstate);
    pause();
    
    pthread_cleanup_pop(0);
    
    return NULL;
}


/**
 * stop a named detector process
 */

int stop_detector(char * det_name)
{
    return kill(process_table_get_pid(det_name), SIGTERM);
}

void detector_stop(int sig){
    raise(SIGTERM);
}

/**
 * restarts a named detector process
 */

int restart_detector(char * det_name)
{
    int restart;
    restart = config_get_value_int(NULL, "detector_reset_on_crash");
    
    stop_detector(det_name);

    if (restart)
        return 0;
    
    return launch_detector(det_name, 1);
}


/**
 * launch a detector process by forking.
 * This is a straightforward way to generalize to multiple detectors.  Each
 * child process gets its own connection to the audio server (JACK) and its
 * own set of audio and event ringbuffers.  Sychronization on the database
 * is maintained by a lock file, which also permits external processes to 
 * lock it out.
 */

int launch_detector(char * det_name, int do_fork)
{
    int pid, err;
    extern char * RunDetectProcessName;
    sigset_t RdDetectorSignalMask;
    
    pthread_t store_thread_id;
    pthread_t detector_thread_id;
    
    pthread_attr_t det_thread_attr;
    detector_thread_info_t info;
    char * driver_type;
    rd_audio_driver_t * driver;
    
    char * det_path, * cls_path;
    sound_setup_t * setup;
    
    info.main_tid = pthread_self();
    
    /*
     * make sure we have an appropriate detector path, then initialize the 
     * detector using it's name and the detector path.  
     */
    
    det_path = config_get_value_str(NULL, "detector_path");
    cls_path = config_get_value_str(NULL, "classifier_path");
    
    rd_log_message("creating detector \"%s\" ...\n", det_name);
    info.detector = detector_create(det_path, det_name, NULL);
    
    if (info.detector == NULL)
    {
        rd_log_message("couldn't create detector \"%s\".\n", det_name);
        return -1;
    }
    
    /*
     * each detector gets one classifier.  the classifier plugin has the
     * same name as the detector plugin, but resides in the classifiers
     * directory.  We can still run without a classifier, so this is an
     * optional step.
     */
    
    rd_log_message("creating classifier \"%s\" ...\n", det_name);
    info.classifier = classifier_create(cls_path, det_name);
    
    if (info.classifier == NULL)
    {
        rd_log_message("couldn't create classifier \"%s\".\n", det_name);
    }
    
    if (do_fork)
    {
        /*
         * the child process connects to the audio engine and runs the appropriate 
         * detector.  The parent returns the new pid to main() which keeps tabs on 
         * all of the detector processes and takes appropriate action if they crash.
         */
        
        pid = fork();
        
        if (pid < 0){
            rd_log_message("failed to fork() for detector \"%s\"\n", det_name);
            return 0;
        }
        
        /*
         * if we're the parent, return the new process's PID and add it to the table
         */
        
        if (pid > 0)
        {
            process_table_add(pid, det_name, -1, -1);
            
            detector_destroy(info.detector);
            return pid;
        }
        
        /* 
         * if we get here, we're a detector (child) process.  
         */
        
        rd_log_init(det_name);
            
        int chd = chdir(DEFAULT_RUNNING_DIR);
        if (chd != 0)
            rd_log_message("couldn't change dir to %s\n", DEFAULT_RUNNING_DIR);
        
        sigfillset (&RdDetectorSignalMask);
        err = pthread_sigmask(SIG_BLOCK, &RdDetectorSignalMask, NULL);
        
        if (err != 0){
            rd_log_message("couldn't set pthread sigmask!\n");
        }
        
        strncpy(RunDetectProcessName, det_name, strlen(RunDetectProcessName));
    }

    /*
     * create audio driver and get detector preferences for data buffer size, sample rate, history and latency
     */
    
    setup = &info.detector->sound_setup;
    
    driver_type = config_get_value_str(NULL, "driver_type");
    int chan = config_get_value_int(NULL, "driver_channel");
    float slack_s = config_get_value_float(NULL, "buffer_slack");
    
    if (slack_s < 1.0)
        slack_s = 1.0;
        
    int slack = (int) (slack_s * setup->rate / setup->N);
    
    if (slack < 1)
        slack = 1;
        
    int pad_history = info.detector->pad.pre/setup->N + 1;
    int pad_latency = info.detector->pad.post/setup->N + 1;
            
    driver = rd_audio_driver_create(
        driver_type, det_name, chan, setup->rate, setup->N, 
        setup->buffer_latency + pad_latency,
        setup->buffer_history + pad_history, slack
    );
    
    if (driver == NULL)
    {
        rd_log_message("couldn't create audio driver!\n");
        exit(1);
    }
    
    if (!driver->realtime)
    {
        config_add_entry(NULL, "offline", "1", CONFIG_ADD_MODE_UPDATE);
    }
        
    if (driver->rate != setup->rate)
    {
        rd_log_message("warning: Detector wanted rate %d, got %d\n", setup->rate, driver->rate);
        setup->rate = driver->rate;
    }
    
    if (driver->N != setup->N)
    {
        rd_log_message(
            "warning: Detector wanted buffer size %d, got %d.\n",
            setup->N, driver->N
        );
    
        setup->N = driver->N;
    }
    
    /*
     * do the full initialization
     */
    
    err = detector_initialize(info.detector);
    
    if (err != RD_ERROR_NO_ERROR)
    {
        rd_log_message("detector initialization failed!\n");
        exit(1);
    }
    
    if (info.classifier != NULL)
    {
        err = classifier_initialize(info.classifier);
        if (err != RD_ERROR_NO_ERROR)
            rd_log_message("classifier initialization failed!\n");
    }
    
    /*
     * give the detector its own copy of the configuration
     */
    
    info.config = config_get(NULL);
    
    /*
     * setup buffers for the detection thread and audio engine
     */
    
    info.sound_buffer = driver->sound_buffer;
    
    int eb_size = config_get_value_int(NULL, "event_buffer_size");
    if (eb_size == 0)
        eb_size = DEFAULT_EVENT_BUFFER_SIZE;
        
    fprintf(stderr, "event_buffer_size: %d\n", eb_size);
    
    info.event_buffer = event_buffer_create(eb_size, driver->realtime);
    
    pthread_mutex_init(&info.eb_lock, NULL);
    pthread_cond_init(&info.eb_ready, NULL);
    
    if (info.event_buffer == NULL)
    {
        rd_log_message("couldn't create event buffer!\n"); 
        exit(1);
    }
    
    info.desired_sample_rate = setup->rate;
    info.processing = 0;
    
    err = pthread_create(&store_thread_id, NULL, store_thread, (void *) &info);
    
    if (err)
    {
        rd_log_message("couldn't create event storage thread.\n"); 
        exit(1);
    }
    
    /* 
     * start detection thread
     */
    
    err = pthread_attr_init(&det_thread_attr);
    err = pthread_attr_setstacksize(&det_thread_attr, DEFAULT_DETECTOR_THREAD_STACK_SIZE);
    err = pthread_create(&detector_thread_id, &det_thread_attr, detector_thread, (void *) &info);
    
    if (err)
    {
        rd_log_message("couldn't create detection thread.\n"); 
        exit(1);
    }
    
    /*
     * start audio engine.  When this happens, data starts getting shipped to the 
     * detection thread, and events (possibly) get dumped to the buffer. wait in a 
     * loop until we're signaled to stop, at which point we disconnect from the auido
     * server and exit
     */
        
    err = rd_audio_driver_start(driver);
    
    if (err)
    {
        rd_log_message("couldn't connect to audio server.\n"); 
        exit(1);
    }
    
    /*
     * if we get here, everything worked ok.
     * wait forever, or until we receive a signal
     */
    
    rd_log_message("detector initialized and running.\n");
    err = 0;
    
    while (err != SIGTERM && err != SIGINT) {
        sigwait(&RdDetectorSignalMask, &err);
    }
    
    /*
     * clean up before exiting
     */
    
    rd_log_message(
        "detector \"%s\" terminating. disconnecting from audio engine...\n", 
        det_name
    );
    
    void * res;
    
    //rd_audio_driver_stop(driver);
        
    fprintf(stderr, "bringing down detector thread\n");
    err = pthread_cancel(detector_thread_id);
    assert(err == 0);
    pthread_join(detector_thread_id, &res);
    assert(res == PTHREAD_CANCELED);
        
    fprintf(stderr, "bringing down store thread\n");

    err = pthread_cancel(store_thread_id);
    assert(err == 0);
    pthread_join(store_thread_id, &res);
    assert(res == PTHREAD_CANCELED);
    
    event_buffer_destroy(info.event_buffer);
    detector_destroy(info.detector);
    classifier_destroy(info.classifier);
    
    fprintf(stderr, "bringing down audio driver thread\n");
        
    rd_audio_driver_free(driver);
    
    fprintf(stderr, "clearing other stuff\n");
        
    config_access(CONFIG_ACCESS_CLEAR);
    process_table_free(process_table());

    exit(0);
}

