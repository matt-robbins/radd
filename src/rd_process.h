#ifndef RD_PROCESS_H
#define RD_PROCESS_H

#include <stdio.h>

typedef struct pipe_s {
    int read;
    int write;
} rd_pfd_t;

typedef union {
    int pfd[2];
    rd_pfd_t pipe;
} rd_comm_t;

typedef struct {
    rd_pfd_t child;
    rd_pfd_t parent;
} rd_comm_duplex_t;

typedef struct {
    FILE * fp;
    int pid;
} rd_pipe_t;

int daemonize(char * log_name);

int rd_cue_init(rd_comm_t * p);
void rd_cue_send(rd_comm_t * p);
int rd_cue_wait(rd_comm_t * p);

int rd_system(char * command, int timeout);

rd_pipe_t * rd_popen(char * command, const char * mode);
int rd_pclose(rd_pipe_t * p, int timeout);

#endif
