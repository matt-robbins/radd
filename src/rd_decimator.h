#ifndef RD_DECIMATOR_H
#define RD_DECIMATOR_H

#include "detect.h"
#include "config.h"

#if HAVE_SPEEX_SPEEX_RESAMPLER_H==1
#include <speex/speex_resampler.h>
#ifdef FIXED_POINT
    #define speex_resampler_process_fcn speex_resampler_process_int
#else
    #define speex_resampler_process_fcn speex_resampler_process_float
#endif
#elif HAVE_FPC3_H==1
#include <fpc3.h>
#else
#define RD_NO_DECIMATOR
#endif

void * rd_decimator_create(int ratio, int quality);

int rd_decimator_run(void * decimator, sample_t * input, int n_in, 
    sample_t * output, int n_out);

void rd_decimator_destroy(void * decimator);

#endif
