#ifndef RD_MEMORY_H
#define RD_MEMORY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void *rd_malloc(size_t size);
void *rd_realloc(void *ptr, size_t size);
void *rd_calloc(int nmemb, size_t size);

void rd_asprintf(char **strp, const char * fmt,  ...);
char * rd_strdup(const char * s);

#endif
