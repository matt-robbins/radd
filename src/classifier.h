
#ifndef RD_CLASSIFIER_H
#define RD_CLASSIFIER_H

#include <dlfcn.h>
#include <limits.h>

#include "detect.h"
#include "configure.h"

/**
 * a classifier "object" containing data and API function handles
 */

typedef struct {
    char * name;
    char * file_name;
    char * config_file;
    char * path;
    void * lib_handle;
    void * parameters;
    void * state;
    rd_parameter_initialize_fcn parameter_initialize;
    rd_state_initialize_fcn state_initialize;
    rd_classify_fcn classify;
    rd_cleanup_fcn cleanup;
    rd_api_version_fcn api_version;
    rd_version_fcn version;
} classifier_t;


/**
 * create a new classifier "object"
 */

classifier_t * classifier_create(char * path, char * name);

/**
 * free storage for a classifier
 */

void classifier_destroy(classifier_t * classifier);

/**
 * free storage from classifier
 */
 
void classifier_cleanup(classifier_t * classifier);

/**
 * run classifier on a "clip" buffer.  return value is the score.
 */

float classifier_run(
    classifier_t * classifier, sample_t * buf, int nbuf, int rate,
    char *** labels, char ** data);

/**
 * allocate all storage necessary for classifier to run.  
 * read configuration if necessary
 */

int classifier_initialize(classifier_t * classifier);

#endif
