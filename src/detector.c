#include "detector.h"
#include "rd_error.h"
#include "rd_time.h"
#include "rd_util.h"
#include "rd_vector.h"
#include "rd_log.h"
#include "configure.h"
#include "rd_util.h"
#include "detect.h"
#include "rd_memory.h"

#include <setjmp.h>
#include <signal.h>
#include <assert.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#if HAVE_PYTHON==1
#include "rd_py_plugin.h"
#endif
/**
 * get detector-desired sound parameters.
 */

int detector_sound_setup(detector_t * detector)
{
    sound_setup_t * p = NULL;
    
    detector->sound_setup.rate = 2000;
    detector->sound_setup.N = 2048;
    detector->sound_setup.buffer_history = 10;
    detector->sound_setup.buffer_latency = 2;
    
    if (detector->setup)
        p = detector->setup(detector->parameters);
        

    memcpy(&detector->sound_setup, p, sizeof(sound_setup_t));

    p = &detector->sound_setup;
    
    return 0;
}

/**
 * free storage associated with a detector
 */

void detector_destroy(detector_t * detector)
{
    if (detector->cleanup && detector->parameters && detector->state)
        detector->cleanup(detector->parameters, detector->state);
    
    free(detector->name);
    free(detector->path);
    free(detector->base_path);
    free(detector->file_name);
    free(detector->config_file);
    
    free(detector->buffer);
    
    if (detector->lib_handle)
        dlclose(detector->lib_handle);    
    free(detector);
}

static char * data_to_str(void * data)
{
    return (char *) data;
}

/**
 * create a new detector object from a given plugin and config file
 */

detector_t * detector_create(char * path, char * name, char * config_file_override)
{
    char lib_name[PATH_MAX];
    char config_file[PATH_MAX];
    char * path_sep;
    
    detector_t * detector = rd_calloc(1, sizeof(detector_t));

    path_sep = (path[strlen(path) - 1] == '/') ? "" : "/";
    sprintf(lib_name, "%s%s%s.so", path, path_sep, name);
    
    if (config_file_override)
        sprintf(config_file, "%s", config_file_override);
    else
        sprintf(config_file, "%s%s%s.conf", path, path_sep, name);
    
    detector->base_path = rd_strdup(path);
    
    rd_asprintf(&detector->path, "%s%s%s", path, path_sep, name);
    
    detector->name = rd_strdup(name);

    detector->parameters = NULL;
    detector->state = NULL;
    
    detector->file_name = rd_strdup(lib_name);
    
    if (!detector->file_name)
        return NULL;
        
    /*
     * first, try the python version
     */
     
    int res;
    struct stat statbuf;
    
    setenv("PYTHONPATH", path, 0);
    
    char pyfile[PATH_MAX];
    sprintf(pyfile, "%s%s%s.py", path, path_sep, name);

    res = stat(pyfile, &statbuf);
    
    if (res == 0 && S_ISREG(statbuf.st_mode))
    {
#if HAVE_PYTHON==1

        rd_asprintf(&detector->config_file, "%s", name);
        
        detector->parameter_initialize = rd_py_parameter_initialize_detector;
        detector->state_initialize = rd_py_state_initialize;
        detector->detect = rd_py_detect;
        detector->cleanup = rd_py_cleanup;
        detector->setup = rd_py_setup;
        detector->data_to_str = data_to_str;
        detector->data_free = NULL;
        detector->api_version = rd_py_api_version;
        detector->version = NULL;

        detector->lib_handle = NULL;
        rd_log_message("Using Python detector %s\n", name);
        
#else 
        rd_log_message("Python support disabled, skipping python detector.\n");
#endif /* HAVE_PYTHON==1 */

    }
    /*
     * otherwise, use the dlopen() C plugin
     */
    else
    {
     
        detector->config_file = rd_strdup(config_file);
            
        detector->lib_handle = dlopen (detector->file_name, RTLD_NOW | RTLD_GLOBAL);
        
        if (!detector->lib_handle){
            fprintf(stderr, "couldn't open detector \"%s\": %s\n", detector->name, dlerror());
            return NULL;
        }
        
        /*
         * populate functions from the library
         */
        
        detector->parameter_initialize = dlsym(detector->lib_handle, "parameter_initialize");
        detector->state_initialize = dlsym(detector->lib_handle, "state_initialize");
        detector->detect = dlsym(detector->lib_handle, "detect");
        detector->cleanup = dlsym(detector->lib_handle, "cleanup");
        detector->setup = dlsym(detector->lib_handle, "setup");
        detector->data_to_str = dlsym(detector->lib_handle, "data_to_str");
        detector->data_free = dlsym(detector->lib_handle, "data_free");
        detector->api_version = dlsym(detector->lib_handle, "api_version");
        
        if (!detector->api_version)
        {
            rd_log_message("detector doesn't specify API version\n");
            dlclose(detector->lib_handle);
            return NULL;
        }
        
        if (strcmp(detector->api_version(), RD_API_VERSION) != 0)
        {
            rd_log_message("*** API version mismatch:  "
                "Detector has \"%s\", RADd has \"%s\" ***\n", 
                detector->api_version(), RD_API_VERSION);
            dlclose(detector->lib_handle);
            
            return NULL;
        }
        
        detector->version = dlsym(detector->lib_handle, "version");
        
        if (!detector->detect)
        {
            rd_log_message("detector lacks a detect() function!\n");
            dlclose(detector->lib_handle);
            return NULL;
        }
    }
    
    /*
     * install some default detector data handlers
     */
    
    if (!detector->data_to_str)
        detector->data_to_str = data_to_str;
        
    /*
     * read parameters from config file, and get audio setup request
     */
     
    char cwd[PATH_MAX];
    char * cd = getcwd(cwd, PATH_MAX - 1);
    
    if (!cd)
    {
        rd_log_message("%s couldn't get cwd!\n", detector->name);
    }
    
    res = chdir(detector->path);
    
    if (res != 0 && errno != ENOENT)
    {
        rd_log_message("%s couldn't chdir!\n", detector->name);
    }
     
    if (detector->parameter_initialize)
        detector->parameters = detector->parameter_initialize(detector->config_file);
        
    res = chdir(cwd);
        
    detector_sound_setup(detector);
    
    fprintf(stderr, "detector setup rate = %d, N = %d\n", detector->sound_setup.rate, detector->sound_setup.N);
    
    /*
     * pull detector configuration options into detector
     */
        
    detector->pad.pre = (int)
        (config_get_value_float(NULL, "%s:pre:clip_pad", detector->name) * detector->sound_setup.rate);
    detector->pad.post = (int)
        (config_get_value_float(NULL, "%s:post:clip_pad", detector->name) * detector->sound_setup.rate);
    
    return detector;
}

/**
 * this function initializes the detector plugin, initializing parameters, state, and 
 * retrieving the sound setup parameters from the detector.
 */

int detector_initialize(detector_t * detector)
{
    char cwd[PATH_MAX];
    char * cd = getcwd(cwd, PATH_MAX - 1);
    int res;
    
    if (!cd)
    {
        rd_log_message("%s couldn't get cwd!\n", detector->name);
    }
    
    res = chdir(detector->path);
    
    if (res != 0 && errno != ENOENT)
    {
        rd_log_message("%s couldn't chdir!\n", detector->name);
    }
    
    if (detector->state_initialize)
        detector->state = detector->state_initialize(detector->parameters, &detector->sound_setup);
    
    res = chdir(cwd);
    
    return RD_ERROR_NO_ERROR;
}

int detector_event_convert(detector_t * det, event_t * de, rd_event_t * event, int N, rd_time_t ref)
{
    int k;
    sound_setup_t setup = det->sound_setup;
    
    for (k = 0; k < N; k++)
    {
        event[k].time = ref + ONE_SEC_USECS * (rd_time_t) de[k].t_in / setup.rate;
        event[k].duration = ONE_SEC_USECS * (rd_time_t) de[k].t_dt / setup.rate;
        event[k].rate = setup.rate;
        event[k].freq = de[k].f_lo;
        event[k].bandwidth = de[k].f_df;
        event[k].detector_name = rd_strdup(det->name);
        event[k].score = de[k].score;
        event[k].class_label = strlen(de[k].label) ? rd_strdup(de[k].label) : NULL;
        event[k].detector_data = de[k].data ? rd_strdup(det->data_to_str(de[k].data)) : NULL;
        
        de[k].label[0] = '\0';
        
        if (det->data_free)
            det->data_free(de[k].data);
    }
    
    return 0;
}

/**
 * call detector on a buffer of data
 */

int detector_run_buffer(detector_t * det, event_t * events, sample_t * buf, int N, rd_time_t btime)
{
    int n_detected;
        
    n_detected = det->detect(
        events, buf, N, det->parameters, det->state, 
        &det->sound_setup, rd_time_to_timeval(btime)
    ); 
    
    return n_detected;
}


/**
 * This is where the detect() method is called from.  The parameters and state 
 * are retrieved from persistent storage, and the detector is run on the buffer of data.
 */

int detector_run(detector_t * det, event_buffer_t * eb, sound_buffer_t * sb)
{
    int n_detected = 0, n_written, n_dropped = 0, k;
    sample_t * buf;
    rd_time_t buf_time;
    rd_event_t event;
    
    /*
     * now, actually run the detector
     */
     
    if (!sb->ready)
    {
        sound_buffer_increment_read_pointer(sb);
        return 0;
    }
    
    buf = sound_buffer_peek(sb, &buf_time);
    
    n_detected = detector_run_buffer(
        det, det->events, buf, det->sound_setup.N, buf_time
    );
        
    /*
     * convert events to full events and put them 
     * in the ringbuffer
     */
    
    for (n_written = 0, k = 0; k < n_detected; k++)
    {
        detector_event_convert(det, det->events + k, &event, 1, buf_time);
        
        event.n_padded_samples = sound_buffer_read_ix(
            sb, &event.padded_samples, 
            det->events[k].t_in - det->pad.pre, 
            det->events[k].t_in + det->events[k].t_dt + det->pad.post
        );
                        
        event.samples = event.padded_samples + det->pad.pre;
        event.n_samples = det->events[k].t_dt;
            
        n_written += event_buffer_write(eb, &event, 1);
    }
    
    sound_buffer_increment_read_pointer(sb);
    
    n_dropped += n_detected - n_written;
    
    return n_dropped;
}


