#ifndef RD_DB_H
#define RD_DB_H

#include <sqlite3.h>
#include "rd_vector.h"
#include "rd_table.h"

#define RD_DB_TABLE_SIZE 50
#define RD_DB_MAX_SQL 4096
#define RD_DB_CREATE 1
#define RD_DB_NOCREATE 0

typedef struct code_node {
    char * name;
    char * sql;
    sqlite3_stmt * stmt;
    struct code_node * next;
} rd_db_stmt_t;
    
typedef struct {
    char * fullfile;
    sqlite3 * db;
    rd_table_t * stmts;
} rd_db_t;

rd_db_t * rd_db_create(char * file);
void rd_db_free(rd_db_t * db);
void rd_db_close(rd_db_t * db);
int rd_db_open(rd_db_t * db, int create);

rd_db_stmt_t * rd_db_add_stmt(rd_db_t * db, char * name1, char * name2, char * sql_fmt, ...);
rd_db_stmt_t * rd_db_get_stmt(rd_db_t * db, char * name1, char * name2);

void rd_db_stmt_list(rd_db_stmt_t * stmt);

void rd_db_stmt_disp(rd_db_stmt_t * stmt);

int rd_db_stmt_bind(rd_db_stmt_t * stmt, int ix, int n, ...);
int rd_db_stmt_bind_row(rd_db_stmt_t * stmt, rd_vector_t * v, int row, int ix);
int rd_db_stmt_exec(rd_db_stmt_t * stmt, rd_vector_t ** out);
int rd_db_stmt_reset(rd_db_stmt_t * stmt);

int rd_db_last_insert_row_id(rd_db_t * db);

int rd_db_exec(rd_db_t * db, char * sql, 
    int (*cb)(void *, int, char **, char **), void * arg, char ** err);

int rd_db_begin(rd_db_t * db);
int rd_db_commit(rd_db_t * db);
int rd_db_vacuum(rd_db_t * db);


#endif

