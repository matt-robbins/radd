#include "rd_filestream.h"
#include "rd_sndfile.h"
#include "rd_filestream.h"
#include "detect.h"
#include "rd_memory.h"

file_stream_t * file_stream_create(char * dir, char * type)
{
    file_stream_t * fs;
    int k, N;
    struct dirent ** namelist;
    char pat[64], path[PATH_MAX];
    sprintf(pat, "*.%s", type);
    struct stat statbuf;
    
    /*
     * see if we're dealing with a single file
     */
     
    sprintf(path, "%s.%s", dir,type);
    
    if (0 == stat(path, &statbuf))
    {
        fs = rd_calloc(1, sizeof(file_stream_t));
        fs->N = 1;
        fs->file_ix = 0;
        fs->fp = sf_open(path, SFM_READ, &fs->info);
        fs->rate = fs->info.samplerate;
        fs->names = rd_calloc(1, sizeof(char *));
        
        return fs;
    }
     
    /*
     * otherwise, we assume it's a directory, and scan it for files
     */
    
    N = scandir (dir, &namelist, NULL, alphasort);
    
    if (N < 0)
        return NULL;
    
    fs = rd_calloc(1, sizeof(file_stream_t));
    fs->path = rd_strdup(dir);
    fs->names = rd_malloc(N * sizeof(char *));
    
    for (fs->N = 0, k = 0; k < N; k++)
    {
        if (0 == fnmatch(pat, namelist[k]->d_name, 0))
            fs->names[fs->N++] = rd_strdup(namelist[k]->d_name);
        
        free(namelist[k]);
    }
    
    free(namelist);
    
    fs->file_ix = 0;
    fs->ix = 0;
        
    if (fs->fp == NULL)
    {
        sprintf(path, "%s/%s", fs->path, fs->names[fs->file_ix]);
        fs->fp = sf_open(path, SFM_READ, &fs->info);
    }
    
    fs->rate = fs->info.samplerate;
    
    return fs;
}


void file_stream_free(file_stream_t * fs)
{
    int k;
    
    for (k = 0; k < fs->N; k++)
        free(fs->names[k]);
    
    free(fs->names);
    free(fs->path);
    
    sf_close(fs->fp);
    free(fs);
}


int file_stream_read(file_stream_t * fs, sample_t * out, int N, rd_time_t * stamp)
{
    char path[PATH_MAX];
    int n, ix = 0;
    
    if (fs->fp == NULL)
    {
        sprintf(path, "%s/%s", fs->path, fs->names[fs->file_ix]);
        fs->fp = sf_open(path, SFM_READ, &fs->info);
    }
    
    while (ix < N)
    {
        n = sf_read_fcn(fs->fp, out, N - ix);
        ix += n;
        fs->ix += n;
        
        if (n == 0)
        {
            if (fs->file_ix == fs->N - 1)
                return ix;
            
            fs->file_ix++;
            sf_close(fs->fp);
            
            sprintf(path, "%s/%s", fs->path, fs->names[fs->file_ix]);
            fs->fp = sf_open(path, SFM_READ, &fs->info);
        }
    }
    
    *stamp = ((fs->ix - ix) * 1000000) / fs->rate;
    return ix;
}

#ifdef TEST

int main(int argc, char * argv[])
{
    int k, N;
    rd_time_t time;
    file_stream_t * fs;
    sample_t data[1000];
    
    if (argc < 2)
    {
        fputs("must supply path\n", stderr);
        exit(1);
    }
    
    fs = file_stream_create(argv[1], "wav");
    
    while (1)
    {
        N = file_stream_read(fs, data, sizeof(data)/sizeof(int), &time);
                
        if (N == 0)
            break;
        
        printf("%d: %d\n", fs->file_ix, data[0]);
        
    }
    
    file_stream_free(fs);
    
    return 0;
}

#endif
