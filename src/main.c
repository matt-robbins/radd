/**\mainpage
 * RADd (Realtime Acoustic Detection daemon) is a real-time detection 
 * engine intended for use in embedded linux systems.  It uses the JACK 
 * audio server to pass data through a set of detector plugins, and 
 * stores the resulting data in an sqlite database along with sound 
 * clips stored as audio files.  It can also run "offline" on data from
 * an audio file or directory of files.
 */
 
#include "process_table.h"
#include "rd_process.h"
#include "configure.h"
#include "report.h"
#include "run_detect.h"
#include "defaults.h"
#include "rd_error.h"
#include "rd_util.h"
#include "config.h"
#include "rd_signal.h"
#include "rd_log.h"
#include "event_store.h"
#include "rd_audio_driver.h"
#include "rd_memory.h"

#include <errno.h>
#include <syslog.h>

#if HAVE_LOCKFILE_H==1
    #include <lockfile.h>
#endif

int RunDetectRestart = 0;
int ExitOnDetectorDone = 1;
int RunDetectRealtime = 1;
char * RunDetectProcessName;

/** 
 * handle the condition when a detector crashes. This is meant to handle 
 * SIGCHLD signals, which are sent when one of the child processes dies.
 */

static void crash_handler(int sig)
{
    int pid, status;
    char * info;
    int is_detector;
    
    pid = waitpid (WAIT_ANY, &status, 0);
    
    if (pid < 0){
        rd_log_message("SIGCHLD raised, but no un-waited-for children exist\n"); return;
    }
    
    /*
     * if it was a crashed detector, optionally restart it
     */
    
    info = process_table_get_info(pid);
    
    if (NULL == info){
        fprintf(stderr, "process %d is not in process table.\n", pid); return;
    }
    
    is_detector = str_ismember(
        info, config_get_value_str(NULL, "detector"), DEFAULT_DETECTOR_SEP);
    
    /*
     * update the process status
     */
        
    process_table_set_status(pid, status);
    
    if (!is_detector)
        return;
    
    if (WIFEXITED(status))
        rd_log_message("\"%s\" process (%d) ended with exit code %d.\n", info, pid, WEXITSTATUS(status));
    if (WIFSIGNALED(status))
        rd_log_message("\"%s\" process (%d) terminated by signal %d.\n", info, pid, WTERMSIG(status));    
    
    if (config_get_value_int(NULL, "detector_reset_on_crash"))
        launch_detector(info, 1);
    
    process_table_remove(pid);
    
    if (ExitOnDetectorDone)
        raise(SIGTERM);
}

/**
 * Handler for SIGHUP and SIGTERM.  both will bring down all child processes,
 * SIGTERM stops the parent process, SIGHUP triggers a restart.
 */

static void clean_shutdown(int sig)
{
    int k, pid, err, status;
    process_table_t * table = process_table();
    extern int RunDetectRestart;
    
    sigset_t set, pendset;
        
    sigemptyset(&set);
    sigaddset(&set, SIGCHLD);
        
    for (k = 0; k < table->N; k++)
    {
        pid = table->entries[k].pid;
        
        if (pid == getpid())
            continue;
        
        rd_log_message("bringing down %d: %s\n", pid, table->entries[k].info);
        kill(pid, SIGTERM);
        
        pid = waitpid(pid, &status, 0);
        
        if (pid == -1){
            rd_log_message("process %d (%s) no longer running\n"); continue;
        }
        
        sigpending(&pendset);
        
        if (sigismember(&pendset, SIGCHLD))
            sigwait(&set, &err);
        
        status = WEXITSTATUS(status);
        rd_log_message("\"%s\" brought down: exit code %d\n", table->entries[k].info, status); 
    }
    
    if (sig == SIGHUP){
        RunDetectRestart = 1; return;
    }
        
    config_access(CONFIG_ACCESS_CLEAR);
    process_table_free(table);
    closelog();
    exit(0);
}

/**
 * signal dispatcher
 */
 
static void signal_handler(int sig_caught)
{
    switch (sig_caught)
    {
    case SIGINT:    
    case SIGTERM: 
    case SIGHUP:
        
        clean_shutdown(sig_caught);
        break;
    
    case SIGCHLD:
        crash_handler(sig_caught);
        break;
    
    default:         /* should normally not happen */
        break;
    }
}

/**
 * wrapper callback for token_iterate to call launch_detector
 */

static int _launch_detector(char * name, void * arg)
{
    return launch_detector(name, 1);
}

/**
 * main program - run the entire detector system.
 */

int main(int argc, char ** argv)
{
    extern int errno;
    int c, err, lfp = 0, detector_count = 0, pipe = 0;
    int daemon = 1;
    struct stat stat_buf;
    
    const char * config_file = DEFAULT_CONFIG_FILE;
    char * driver_type = RD_DEFAULT_DRIVER_ST;
    time_t config_last_mtime = 0;
        
    char * usage = 
    "Usage: run_detect [-d] [-f config_file] [-a audio driver] [detector]\n\
    -d : don't daemonize\n\
    -f : use [config_file] instead of the default: \"/etc/run_detect.conf\"\n\
    -v : print version and exit\n\
    -a : use [audio driver] (\"jack\" or \"pulse\"\n\
    -h : print this message and exit\n\
    For more information, talk to Matt.\n";
        
    RunDetectProcessName = argv[0];
    
    /*
     * handle input
     */
    
    opterr = 0;
     
    while ((c = getopt (argc, argv, "hvdpf:a:")) != -1)
    {
        switch (c)
        {
            case 'v':
                puts(PACKAGE_VERSION); 
                return 0;
            case 'd':
                daemon = 0;
                break;
            case 'f':
                config_file = optarg;
                break;
            case 'a':
                driver_type = optarg;
                break;
            case 'p':
                driver_type = "fd";
                pipe = 1;
                break;
            case 'h':
                puts(usage); 
                return 0;
            case '?':
            default:
                puts(usage);
                return 1;
        }
    }
    
    /* 
     * go into the background unless we're told not to
     */
    
    if (daemon) 
        lfp = daemonize("run_detect");

    rd_log_init("run_detect");   
     
    /*
     * we're the parent: set up our signal handling: See rd_signal.h
     */
    
    process_table_add(getpid(), "main", -1, -1);
    rd_setup_signals(signal_handler);

    /*
     * initialize configuration and read values from config file
     */
    
    err = config_init((char *) config_file, NULL, CONFIG_WARN_ON_INSANE);
        
    if (err)
    {
        rd_log_message("Error reading configuration file.  Cannot continue.\n"); 
        exit(1);
    }
    
    /*
     * additional input args can override config file values for detector and detector_config_file
     */
    
    if (optind < argc)
        config_add_entry(NULL, "detector", argv[optind++], CONFIG_ADD_MODE_UPDATE);
    
    if (optind < argc){
        fprintf(stderr, "Too many input arguments.\n"); return 1;
    }
    
    /*
     * ensure we have at least default values for a few of the configuration settings
     */
        
    config_add_entry(NULL, "driver_type", driver_type, CONFIG_ADD_MODE_UPDATE);
    config_add_entry(NULL, "detector_path", DEFAULT_DETECTOR_PATH, CONFIG_ADD_MODE_UNIQUE);
    config_add_entry(NULL, "classifier_path", DEFAULT_CLASSIFIER_PATH, CONFIG_ADD_MODE_UNIQUE);
    config_add_entry(NULL, "detector", "none", CONFIG_ADD_MODE_UNIQUE);
    config_add_entry(NULL, "store_path", DEFAULT_STORE_PATH, CONFIG_ADD_MODE_UNIQUE);
    config_add_entry(NULL, "detector_reset_on_crash", "0", CONFIG_ADD_MODE_UNIQUE);
    config_add_entry(NULL, "clip_pad", "0", CONFIG_ADD_MODE_UNIQUE);
    config_add_entry(NULL, "clip_format", "aiff", CONFIG_ADD_MODE_UNIQUE);
    config_add_entry(NULL, "resample_quality", "1", CONFIG_ADD_MODE_UNIQUE);
    
    if (pipe)
        config_add_entry(NULL, "pipe", "1", CONFIG_ADD_MODE_UNIQUE);
    
    
    /*
     * finally, check the configuration for sanity, if it is insane, we can't run.
     */
    
    err = config_sanity_check(NULL, "detector_path,detector");
    
    if (err < 0) {
        rd_log_message("configuration doesn't specify the detector.\n"); exit(1);
    }
    
    #if HAVE_LOCKFILE_H==1
    /*
     * clear out any stale lock file and create storage directory
     */
    
    lockfile_remove(EVENT_LOCK_FILE);
    #else
    rd_log_message("\nNOTE: RADd built without liblockfile support.\n\n");
    #endif /*HAVE_LOCKFILE_H==1*/
    
    #if HAVE_LIBSPEEXDSP!=1 && HAVE_LIBFPC!=1
    rd_log_message("\nNOTE: RADd built without any resampling library.  Cannot resample.\n\n");
    #endif
        
    err = mkdir(config_get_value_str(NULL, "store_path"), S_IRWXU);
    
    if (err && errno != EEXIST){
        rd_log_message("couldn't create storage path.\n"); exit(1);
    }
    
    err = mkdir(DEFAULT_FORWARD_PATH, S_IRWXU);
    
    if (err && errno != EEXIST){
        rd_log_message("couldn't create forwarding path."); exit(1);
    }
    
    if (pipe)
    {
        rd_log_message("running as pipeline component!\n");
        launch_detector(config_get_value_str(NULL, "detector"),0);
    }
    
    /* 
     * Launch all detectors as separate processes.  We keep track of them
     * in a table with their names.  We can use this later in the crash
     * handler for SIGCHLD to log information if a detector crashes.
     */
    
    detector_count = token_iterate(
        NULL, config_get_value_str(NULL, "detector"), 
        DEFAULT_DETECTOR_SEP, _launch_detector, NULL);
            
    if (detector_count <= 0)
    {
        rd_log_message("couldn't succesfully initialize any detectors.\n");
        exit(1);
    }
    
    /*
     * spin, waiting for the reset signal, which is used to load a refreshed config file.
     */
    
    while (!RunDetectRestart) 
    {
        sleep (1);
                
        if (stat(config_file, &stat_buf)){
            fprintf(stderr, "couldn't stat config file: did you move it?\n");
            continue;
        }
        
        if (config_last_mtime != 0 && config_last_mtime != stat_buf.st_mtime){
            rd_log_message("configuration file changed\n"); 
            rd_raise(SIGHUP);
        }
        
        config_last_mtime = stat_buf.st_mtime;
    }
    
    /* 
     * if we get here, we need to reset.  use exec to re-call run_detect with
     * the same input args it was initially run with.  Using exec means that the
     * process is overwritten and we start fresh.
     */
    
    rd_log_message("run_detect restarting...\n");
    
    if (daemon)
        f_ulock(lfp);
        
    if ((err = execvp(argv[0], argv))){
        rd_log_message("couldn't restart, quitting: %s\n", strerror(errno));
    }
    
    return 0;
}
