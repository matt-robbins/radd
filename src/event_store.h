#include <sqlite3.h>
#include <sndfile.h>
#include "detect.h"
#include "detector.h"
#include "classifier.h"
#include "event_buffer.h"
#include "rd_db.h"

#define EVENT_LOCK_FILE "/tmp/detect_write.lock"

int get_event_lock(void);
int get_event_lock_block(int timeout);
void relinquish_event_lock(void);

rd_db_t * db_access(char * path, char * filename);
int db_prune(rd_db_t * db, char * author, char * path);
void db_free(void);

int write_events(
    event_buffer_t * event_buffer, detector_t * detector, 
    classifier_t * classifier);
