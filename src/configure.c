/**\file
 * functions for storing persisent configuration table, implemented as an
 * in-memory sqlite3 database
 */

#include "configure.h"
#include "rd_util.h"
#include "rd_vector.h"
#include "rd_log.h"
#include "rd_memory.h"

/**
 * global configuration
 */
 
config_t * RdMainConfig;

/**
 * return a string showing the config error
 */

char * config_error_str(config_t * c)
{
    static char str[512] = "no error\n";
    
    switch (c->error){
        
        case CONFIG_ERROR_BAD_FILE:
            sprintf(str, "couldn't read config file '%s'\n", c->name); 
            break;
        case CONFIG_ERROR_ENTRY_EXISTS:
            sprintf(str, "entry already exists in configuration\n");
            break;
        case CONFIG_ERROR_NO_SUCH_ENTRY:
            sprintf(str, "no such configuration entry\n"); 
            break;
        case CONFIG_ERROR_NO_ERROR:
            sprintf(str, "no error\n"); 
            break;
        default:
            sprintf(str, "unknown error\n"); 
            break;
    
    }
    
    c->error = CONFIG_ERROR_NO_ERROR;
    
    return str;
}


/** 
 * create a configuration object.
 */

config_t * config_create()
{
    config_t * cfg = rd_malloc(sizeof(config_t));
    
    cfg->table = table_create(CONFIG_TABLE_SIZE);
    cfg->name = NULL;
    cfg->error_entry = NULL;

    return cfg;
}

/** 
 * free storage associated with a configuration object
 */

int config_free(config_t * cfg)
{
    if (cfg == NULL)
        return 0;
    
    table_free(cfg->table);
    
    free(cfg->name);
    free(cfg->error_entry);
    free(cfg);
    
    return 0;
}

static rd_value_t * str_to_value(char * value)
{
    rd_value_t * v = rd_malloc(sizeof(rd_value_t));
    int i;
    double f;
    
    if (strchr(value, '.') && (f = atof(value)))
    {
        v->type = RD_TYPE_DBL;
        v->d.f = f;
        return v;
    }
    
    if ((i = atoi(value)))
    {
        v->type = RD_TYPE_INT;
        v->d.i = i;
        return v;
    }
    
    v->type = RD_TYPE_STR;
    v->d.s = rd_strdup(value);
    
    return v;
}

static char * value_to_str(rd_value_t * value)
{
    char * out;
    
    if (value == NULL)
        return NULL;
    
    if (value->type == RD_TYPE_STR)
        return rd_strdup(value->d.s);
    
    if (value->type == RD_TYPE_INT)
    {
        rd_asprintf(&out, "%d", value->d.i);
    }
    
    if (value->type == RD_TYPE_DBL)
    {
        rd_asprintf(&out, "%f", value->d.f);
    }
    
    return out;
}


static void value_free(void * value)
{
    rd_value_t * v = (rd_value_t *) value;
    
    if (v->type == RD_TYPE_STR)
        free(v->d.s);
    
    free(v);
}

/**
 * add a value to the configuration object, with varying behaviour dependent on mode
 */

int config_add_value(config_t * c, char * key, char * value, config_add_mode_t mode)
{
    config_t * cfg = config_get(c);
    int result;
    
    rd_value_t * val = str_to_value(value);
    
    switch (mode)
    {
        case CONFIG_ADD_MODE_UPDATE:
            result = table_replace(cfg->table, key, val, value_free);
            break;
        case CONFIG_ADD_MODE_UNIQUE:
        default:
            result = table_insert(cfg->table, key, val, value_free);
            break;
    }
    
    cfg->error = CONFIG_ERROR_NO_ERROR;
        
    if (result == TABLE_ENTRY_EXISTS)
    {
        value_free(val);
        cfg->error = CONFIG_ERROR_ENTRY_EXISTS;
    }
    
    if (result == TABLE_FULL)
    {
        value_free(val);
        cfg->error = CONFIG_ERROR_TABLE_FULL;
    }
    
    return cfg->error;
}

int config_add_entry(config_t * c, char * key, char * value, config_add_mode_t mode)
{
    return config_add_value(c, key, value, mode);
}

/**
 * get a string value from the config
 */

static rd_value_t * config_get_value_internal(config_t * c, char * key)
{
    config_t * cfg = config_get(c);    
    return (rd_value_t *) table_get(cfg->table, key);
}

rd_value_t * config_get_value(config_t * c, char * key)
{
    return config_get_value_internal(c, key);
}

/**
 * allow a specific key, represented by "specifier:" to override the default
 * configuration value.  We use a va_list so you can specify the key just as 
 * you would printf arguments.  i.e. config_get_value("%s:%s", specifier, base_key)
 */

static rd_value_t * config_get_value_va(config_t * c, char * format, va_list arglist)
{
    rd_value_t * value = NULL;
    
    char key[CONFIG_MAX_ENTRY_LEN], * key_ptr = key;
    int N;
     
    N = vsnprintf(key, CONFIG_MAX_ENTRY_LEN - 1, format, arglist);
    
    if (N >= CONFIG_MAX_ENTRY_LEN){
        fprintf(stderr, "configuration entry too long!\n");
    }
    
    /*
     * now get the configuration value
     */
    
    while (key_ptr != NULL)
    {
        value = config_get_value_internal(c, key_ptr);
        
        if (value != NULL)
            break;
        
        if (value == NULL)
            key_ptr = strchr(key_ptr, CONFIG_NAMESPACE_SEP);
        
        if (key_ptr != NULL)
            key_ptr++;
    }

    return value;
}

/**
 * generate the code to call a function from a variable argument list
 */

#define VA_HELPER(c, out, in, fcn) \
va_list arglist; \
va_start(arglist, in); \
out = fcn(c, in, arglist); \
va_end(arglist);

/**
 * get a string value, with optional specifier.
 */

char * config_get_value_str(config_t * c, char * format, ...)
{
    rd_value_t * value;
    VA_HELPER(c, value, format, config_get_value_va)
    
    if (value == NULL)
        return NULL;
    
    if (value->type == RD_TYPE_STR)
        return value->d.s;
    
    return NULL;
}

char * config_get_value_strc(config_t * c, char * format, ...)
{
    rd_value_t * value;
    VA_HELPER(c, value, format, config_get_value_va)
    
    return value_to_str(value);
}

/**
 * get an integer value from the configuration
 * NOTE: this should perform a parse-error check.
 */

int config_get_value_int(config_t * c, char * format, ...)
{
    config_t * cfg = config_get(c);
    rd_value_t * value;
    
    VA_HELPER(cfg, value, format, config_get_value_va)
    
    if (value == NULL)
        return 0;
        
    if (value->type == RD_TYPE_INT)
        return value->d.i;
    
    if (value->type == RD_TYPE_DBL)
        return (int) value->d.f;
    
    return atoi(value->d.s);
}

/**
 * get a float value.
 * NOTE: this should perform a parse-error check.
 */

float config_get_value_float(config_t * c, char * format, ...)
{
    config_t * cfg = config_get(c);
    rd_value_t * value;
    
    VA_HELPER(cfg, value, format, config_get_value_va)
    
    if (value == NULL)
        return 0.0;
    
    if (value->type == RD_TYPE_DBL)
        return (float) value->d.f;
    
    if (value->type == RD_TYPE_INT)
        return (float) value->d.i;
        
    return atof(value->d.s);
}

/**
 * read a config file into the config object
 */

int config_read(config_t * c, char * config_file, char * names)
{
    FILE * fp;
    char line[LINE_MAX], name[64], value[64];
    int result;
    
    fp = fopen(config_file, "r");
    c->name = rd_strdup(config_file);
    
    if (!fp){
        c->error = CONFIG_ERROR_BAD_FILE; return -1;
    }
    
    while (fgets(line, LINE_MAX, fp) != NULL)
    { 
        /*
         * use the '#' character as a line comment
         */
        
        if ((line[0] == '#') || (strlen(line) < 3))
            continue;
        
        /*
         * break line into name/value pair and add it to the config
         */
                
        result = sscanf(line, "%s%s", name, value);
        
        if (result != 2) {
            fprintf(stderr, "warning: bogus line \"%s\" in \"%s\"\n", name, config_file);
            continue;
        }
        
        result = config_add_value(c, name, value, CONFIG_ADD_MODE_UNIQUE);
        
        if (result != 0)
            fprintf(stderr, "whoops! couldn't insert (%s, %s): %s\n", name, value, config_error_str(c));
    }
    
    fclose(fp);
    return 0;
}

/**
 * convinience routine provides a stored config object that can be referenced
 * from anywhere.
 */

inline config_t * config_access(config_access_type_t mode)
{
    config_t * config = RdMainConfig;
    
    switch (mode)
    {
        case CONFIG_ACCESS_GET:
            
            if (config == NULL)
                config = config_create();
            
            break;
            
        case CONFIG_ACCESS_CLEAR:
            
            if (config != NULL)
                config_free(config); 
            
            config = NULL; 
            break;
    }
    
    RdMainConfig = config;
    
    return RdMainConfig;
}

/**
 * get the stashed config, or pass a known pointer through
 */

inline config_t * config_get(config_t * in)
{
    if (in == NULL)
        return config_access(CONFIG_ACCESS_GET);
    else
        return in;
}

int _config_value_exists(char * name, void * config)
{
    config_t * c = (config_t *) config;
    rd_value_t * value = config_get_value(c, name);
        
    if (NULL == value){
        return -1;
    }
    
    return 0;
}

int config_sanity_check(config_t * cfg, char * names)
{
    config_t * c = config_get(cfg);
    int n, k;
    char * lnames = rd_strdup(names);
    
    if (c == NULL)
        return -1;
    if (names == NULL)
        return 0;
        
    k = token_iterate(&n, lnames, ":;, ", _config_value_exists, c);
    free(lnames);
    
    if (k < 0 || k != n)
        return -1;
    
    return 0;
}

/**
 * Create a new config object based on an input config file.
 * This is a shortcut to using config_create() and config_read()
 */

config_t * config_create_from_file(char * file, char * names)
{
    int result;
    
    config_t * c = config_create();
    result = config_read(c, file, names);
        
    if (result)
    {
        fprintf(stderr, "%s", config_error_str(c)); 
        config_free(c); c = NULL;
    }
    
    return c;
}


/**
 * return a single-string dump of a configuration object's contents
 */

char * config_dump(config_t *c)
{
    config_t * cfg = config_get(c);
    char * output = NULL;
    int i, n, N = 0;
    char ** names;
    char * val_str;
    rd_value_t ** values;
        
    /*
     * first pass to calculate storage requirement
     */
    
    n = table_get_all(cfg->table, &names, (void ***) &values);
    
    for (i = 0; i < n; i++)
    {
        val_str = value_to_str(values[i]);
        N += (strlen(names[i]) + strlen(val_str) + 3);
        free(val_str);
    }
    
    /*
     * now allocate and build string
     */
    
    output = rd_calloc(N, sizeof(char));
    
    for (i = 0; i < n; i++) 
    {
        strcat(output, names[i]);
        strcat(output, ";");
        
        val_str = value_to_str(values[i]);
        
        strcat(output, val_str);
        strcat(output, ",");
        
        free(val_str);
    }
    
    free(names); 
    free(values);
    
    return output;
}

void config_print(config_t * c)
{
    config_t * cfg = config_get(c);
    char * dump = config_dump(cfg);

    printf("%s\n", dump);
    free(dump);
}

/**
 * initialize stored config object
 */

int config_init(char * file, char * names, config_sanity_t sanity)
{
    int result;
    config_t * config;
    
    config_access(CONFIG_ACCESS_CLEAR);
    config = config_get(NULL);
        
    if (file == NULL){
        fprintf(stderr, "can't create configuration. no file specified.\n");
        config = NULL; 
        return CONFIG_ERROR_BAD_FILE; 
    }
    
    result = config_read(config, file, names); 
    
    if (result){
        fprintf(stderr, "couldn't read config file %s\n", file);
        return CONFIG_ERROR_BAD_FILE;
    }
        
    if (names != NULL)
        result = config_sanity_check(config, names);
    
    switch (sanity)
    {
        case CONFIG_FAIL_ON_INSANE:
            
            config_free(config); 
            config = NULL; 
            return CONFIG_ERROR_FAILED_SANITY;
            
        case CONFIG_WARN_ON_INSANE:
            break;
    }

    return result;
}

/**
 * return true if the configuration contains an entry with a specified key
 */

int config_has_entry(config_t * cfg, char * config_entry_name)
{
    config_t * c = config_get(cfg);
    char * value = config_get_value_str(c, config_entry_name);

    return !(value == NULL);
}
