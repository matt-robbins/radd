#ifndef RD_AUDIO_DRIVER_H
#define RD_AUDIO_DRIVER_H

#include "sound_buffer.h"
#include "config.h"

typedef enum {
    RD_AUDIO_JACK = 0,
    RD_AUDIO_PULSE,
    RD_AUDIO_ALSA,
    RD_AUDIO_OSS,
    RD_AUDIO_FILES,
    RD_AUDIO_FD,
    RD_AUDIO_SOCK
} rd_audio_type_t;

#define RD_AUDIO_TYPES_INIT {\
    "jack", \
    "pulse", \
    "alsa", \
    "oss", \
    "files", \
    "fd", \
    "sock" }

#if HAVE_LIBJACK==1
    #define RD_DEFAULT_DRIVER RD_AUDIO_JACK
    #define RD_DEFAULT_DRIVER_ST "jack"
#else
    #define RD_DEFAULT_DRIVER RD_AUDIO_ALSA
    #define RD_DEFAULT_DRIVER_ST "alsa"
#endif

typedef struct rd_audio_driver_s {
    rd_audio_type_t type;
    char * typename;
    char * typearg;
    int rate;
    int N;
    int channel;
    sound_buffer_t * sound_buffer;
    void * decimator;
    sample_t * input_buffer;
    int ix;
    rd_dll_t dll;
    int decimate;
    int filter_length;
    float bandwidth;
    void * data;
    int realtime;
} rd_audio_driver_t;

rd_audio_driver_t * 
rd_audio_driver_create(
    char * type, char * name, int channel, int rate, int N,
    int latency, int history, int slack
);

int rd_audio_driver_start(rd_audio_driver_t * ad);

void rd_audio_driver_stop(rd_audio_driver_t * ad);

void rd_audio_driver_free(rd_audio_driver_t * ad);

#endif
