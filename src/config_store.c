#include "rd_db.h"
#include "configure.h"
#include "rd_log.h"
#include "rd_memory.h"
#include <assert.h>

int config_store(
    rd_db_t * db, config_t * cfg, char * spec, char ** names)
{
    char * err, * val;    
    int st;
        
    if (names == NULL)
        return -1;
            
    static char ConfigTable[] = 
    "CREATE TABLE IF NOT EXISTS config("
    "   application TEXT, name TEXT, value TEXT, "
    "UNIQUE(application, name));";
    
    char * isql_t = "INSERT OR REPLACE INTO config VALUES('%s','%s','%s');";
    char * isql = NULL;
    
    st = rd_db_exec(db, ConfigTable, NULL, NULL, &err);
    if (st != SQLITE_OK)
    {
        printf("couldn't insert config table! %s\n", err); 
        sqlite3_free(err);
    }
    rd_db_commit(db);
    
    int k = 0;    
    rd_db_begin(db);
    
    while (names[k] != NULL)
    {            
        val = config_get_value_strc(cfg, "%s:%s", spec, names[k]);
        rd_asprintf(&isql, isql_t, spec, names[k], val);
                
        st = rd_db_exec(db, isql, NULL, NULL, &err);
        
        free(isql);
        k++;
    }
    
    rd_db_commit(db);
    
    return 0;
}

#ifdef TEST

int main(int argc, char * argv[])
{
    
    
}


#endif
