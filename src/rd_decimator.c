
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#include "rd_decimator.h"
#include "rd_log.h"
#include "rd_memory.h"


#if HAVE_SPEEX_SPEEX_RESAMPLER_H==1

typedef struct{
    SpeexResamplerState * s;
    int16_t * ibuf;
    int16_t * obuf;
} rd_decimator_t;

void * rd_decimator_create(int ratio, int quality)
{
    int err;
    int nchan = 1;
    int out_rate = 1;
    rd_decimator_t * d = rd_calloc(1, sizeof(rd_decimator_t));
    
    d->s = speex_resampler_init(
        nchan, ratio, out_rate, quality, &err);
                
    return (void *) d;
}

int 
rd_decimator_run(void * decimator, sample_t * input, 
    int n_in, sample_t * output, int n_out)
{
    uint32_t spx_N = n_in;
    uint32_t spx_M = n_out;
    rd_decimator_t * d = decimator;
    SpeexResamplerState * s = d->s;

    #if FIXED_POINT==32
    d->ibuf = rd_realloc(d->ibuf, n_in * sizeof(int16_t));
    d->obuf = rd_realloc(d->obuf, n_out * sizeof(int16_t));
    int k;
    
    for (k = 0; k < n_in; k++)
        d->ibuf[k] = input[k] >> 16;
    
    int spx_out = speex_resampler_process_fcn(
        s, 0, d->ibuf, &spx_N, d->obuf, &spx_M);
    
    for (k = 0; k < spx_M; k++)
        output[k] = d->obuf[k] << 16;
    
    #else
    int spx_out = speex_resampler_process_fcn(
        s, 0, input, &spx_N, output, &spx_M);
    #endif
        
    if (spx_out != 0)
    {
		rd_log_message("resample failure!\n");
	}
    
    return spx_M;
}

void rd_decimator_destroy(void * decimator)
{
    rd_decimator_t * d = decimator;
    speex_resampler_destroy(d->s);
    free(d->ibuf);
    free(d->obuf);
    free(d);
}

#elif HAVE_LIBFPC==1

void * rd_decimator_create(int ratio, int quality)
{
    int factor = 20;
    float transition_band = 0.25;
    
    switch (quality)
    {
        case 0:
            factor = 10;
            transition_band = 0.5;
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            factor = 20;
            transition_band = 0.25;
            break;
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
            factor = 30;
            transition_band = 0.1;
            break;
        default:
            factor = 20;
            transition_band = 0.25;
            break;
    }

    fpc_decimator_t * d = decimator_create(
        ratio, factor * ratio, transition_band);
        
    return (void *) d;
}

int rd_decimator_run(void * decimator, sample_t * input, 
    int n_in, sample_t * output, int n_out)
{
    int N = decimator_run(
        (fpc_decimator_t *)decimator, input, n_in, output);
        
    return N;
}

void rd_decimator_destroy(void * decimator)
{
    decimator_destroy((fpc_decimator_t *) decimator);
}

#else

void * rd_decimator_create(int ratio, int quality)
{
    return NULL;
}

int rd_decimator_run(void * decimator, sample_t * input, 
    int n_in, sample_t * output, int n_out)
{
    memcpy(output, input, n_out * sizeof(sample_t));
    return n_out;
}

void rd_decimator_destroy(void * decimator){ }

#endif
