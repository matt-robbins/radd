#include "classifier.h"
#include "rd_error.h"
#include "rd_util.h"
#include "rd_log.h"
#include "configure.h"
#include "rd_memory.h"

#include "config.h"

#include <assert.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#if HAVE_PYTHON==1
#include "rd_py_plugin.h"
#endif

/**
 * create a new classifier "object"
 */

classifier_t * classifier_create(char * path, char * name)
{
    char * path_sep;    
    classifier_t * classifier = rd_calloc(1, sizeof(classifier_t));

    path_sep = (path[strlen(path) - 1] == '/') ? "" : "/";
    
    rd_asprintf(&classifier->path, "%s%s%s", path, path_sep, name);
    rd_asprintf(&classifier->file_name, "%s.so", classifier->path);
    rd_asprintf(&classifier->config_file, "%s.conf", classifier->path);

    classifier->name = rd_strdup(name);
    
    classifier->parameters = NULL;
    classifier->state = NULL;
    
    #if HAVE_PYTHON==1

    /*
     * first, try the python version
     */
    
    setenv("PYTHONPATH", path, 0);
    int res;
    
    char pyfile[PATH_MAX];
    sprintf(pyfile, "%s%s%s.py", path, path_sep, name);
    
    fprintf(stderr, "trying %s\n", pyfile);

    struct stat statbuf;
    res = stat(pyfile, &statbuf);
    
    if (res == 0 && S_ISREG(statbuf.st_mode))
    {
        rd_asprintf(&classifier->config_file, "%s", name);
        
        classifier->parameter_initialize = rd_py_parameter_initialize_classifier;
        classifier->state_initialize = rd_py_state_initialize;
        classifier->classify = rd_py_classify;
        classifier->cleanup = rd_py_cleanup;
        classifier->api_version = rd_py_api_version;
        classifier->version = NULL;

        classifier->lib_handle = NULL;
        rd_log_message("Using Python classifier %s\n", name);
        return classifier;
    }
    
#endif /* HAVE_PYTHON==1 */
    
    /*
     * try to open extension
     */
        
    classifier->lib_handle = dlopen(
        classifier->file_name, RTLD_NOW | RTLD_GLOBAL);
    
    if (!classifier->lib_handle)
    {
        fprintf(stderr, "couldn't open classifier \"%s\": %s\n", classifier->name, dlerror());
        classifier_destroy(classifier);
        return NULL;
    }
    
    /*
     * populate functions from the library
     */
    
    classifier->parameter_initialize = dlsym(classifier->lib_handle, "parameter_initialize");
    classifier->state_initialize = dlsym(classifier->lib_handle, "state_initialize");
    classifier->classify = dlsym(classifier->lib_handle, "classify");
    classifier->cleanup = dlsym(classifier->lib_handle, "cleanup");
    classifier->api_version = dlsym(classifier->lib_handle, "api_version");
    
    if (!classifier->api_version)
    {
        rd_log_message("classifier doesn't specify API version\n");
        dlclose(classifier->lib_handle);
        return NULL;
    }
    
    if (strcmp(classifier->api_version(), RD_API_VERSION) != 0)
    {
        rd_log_message("*** API version mismatch:  "
            "Classifier has \"%s\", RADd has \"%s\" ***\n", 
            classifier->api_version(), RD_API_VERSION);
        dlclose(classifier->lib_handle);
        
        return NULL;
    }
    
    classifier->version = dlsym(classifier->lib_handle, "version");
    
    return classifier;
}

/**
 * free storage for a classifier
 */

void classifier_destroy(classifier_t * classifier)
{
    if (!classifier)
        return;
        
    if (classifier->cleanup && classifier->parameters && classifier->state)
        classifier->cleanup(classifier->parameters, classifier->state);
    
    free(classifier->path);
    free(classifier->name);
    free(classifier->file_name);
    free(classifier->config_file);
    
    if (classifier->lib_handle)
        dlclose(classifier->lib_handle);    
    free(classifier);
}

/**
 * run classifier on a "clip" buffer.  return value is the score.
 */

float classifier_run(
    classifier_t * classifier, sample_t * buf, int nbuf, int rate,
    char *** labels, char ** data)
{
    *labels = NULL;
    *data = NULL;
    
    float score = classifier->classify(
        buf, nbuf, rate, classifier->parameters, classifier->state, data, labels
    ); 
    
    return score;
}

/**
 * allocate all storage necessary for classifier to run.  
 * read configuration if necessary
 */

int classifier_initialize(classifier_t * classifier)
{
    char cwd[PATH_MAX];
    int res = 0;
    char * cd = getcwd(cwd, PATH_MAX - 1);
    if (!cd)
    {
        rd_log_message("couldn't getcwd for classifier %s\n", classifier->name);
    }
    
    res = chdir(classifier->path);
    
    if (res)
    {
        rd_log_message("couldn't chdir for classifier %s\n", classifier->name);
    }
    
    if (classifier->parameter_initialize)
        classifier->parameters = classifier->parameter_initialize(classifier->config_file);
    
    if (classifier->state_initialize)
        classifier->state = classifier->state_initialize(classifier->parameters, NULL);
        
    res = chdir(cwd);
    
    if (res)
    {
        rd_log_message("couldn't chdir for classifier %s\n", classifier->name);
    }
    
    return RD_ERROR_NO_ERROR;
}
