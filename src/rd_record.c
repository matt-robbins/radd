/**\file
 * main program for recording audio, using and run_detect resampling audio driver
 */

#include "rd_util.h"
#include "rd_log.h"
#include "rd_process.h"
#include "rd_audio_driver.h"
#include "configure.h"
#include "config.h"
#include "defaults.h"
#include "detect.h"
#include "rd_sndfile.h"
#include "rd_memory.h"

#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <stdarg.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <time.h>
#include <sndfile.h>

#define xstr( s ) stringize(s)
#define stringize( x ) #x

#define BUFLEN 1024
#define DEFAULT_PATH /tmp
#define DEFAULT_NAME record

/**
 * signal handlers
 */
 
int Done = 0; 
int DisableChecks = 0;

static void shutdown_handler(int sig)
{
    rd_log_message("recieved signal %d, shutting down.\n", sig);
    Done = 1;
}

typedef struct parameters_s {
    int debug;
    int file_duration;
    int sample_rate;
    int buf_sec;
    int bit_depth;
    char strftime_format[128];
    char file_format[10];
    char path[PATH_MAX];
    SF_INFO info;
} parameters_t;

typedef struct state_s {
    SNDFILE * fp;
    time_t next_file_time;
    char file_name[PATH_MAX];
    char tmpfile_name[PATH_MAX];
} state_t;

void update_file_name(parameters_t * p, state_t * state)
{
    char date_str[128];
    
    state_t * s = (state_t *) state;
    
    strftime(date_str, sizeof(date_str) - 1, p->strftime_format, gmtime(&s->next_file_time));
    
    sprintf(s->file_name, "%s/%s.%s", p->path, date_str, p->file_format);
    sprintf(s->tmpfile_name, "%s.part", s->file_name);
    
    return;
}

static int local_abs(int a)
{
    if (a > 0)
        return a;
    else if (a == INT_MIN)
        return INT_MAX;
    else
        return -a;
}

int record(
    sample_t * buf, int nbuf, 
    void * parameters, void * state, struct timeval time)
{
    parameters_t * p = (parameters_t *) parameters;
    state_t * s = (state_t *) state;
    int file_start_ix, n_write;
    int64_t tmp;

    /*
     * handle time discontinuities: if the file start was more than
     * 2 file durations ago, then reset the new file start out in front
     * of us by one file duration.
     */

    if (local_abs((int) time.tv_sec - (int) s->next_file_time) > (2 * p->file_duration))
    {
        if (p->debug)
            fprintf(stderr,
                "time discontinuity! We're here: %d, next file: %d\n", 
                (int) time.tv_sec, (int) s->next_file_time
            );
        
        s->next_file_time = 
            p->file_duration * ((time.tv_sec / p->file_duration) + 1);
        
        return 0;
    }
    
    tmp = (((int64_t) time.tv_usec * p->sample_rate) / 1000000);
    file_start_ix = (s->next_file_time - time.tv_sec) * p->sample_rate - tmp;
    
    if (p->debug > 1)
        printf("file_start_ix = %d\n", file_start_ix);
        
    n_write = file_start_ix > nbuf ? nbuf : file_start_ix;
    
    if (s->fp == NULL)
    {
        if (p->debug) printf("file pointer is NULL!\n");
    }
    else
    {
        sf_write_fcn(s->fp, buf, (sf_count_t) n_write);
    }
    
    if (file_start_ix > nbuf)
        return 0;
    
    if (s->fp != NULL)
    {
        sf_close(s->fp);
        rename(s->tmpfile_name,s->file_name);
        
        if (p->debug)
            fprintf(stderr, "renaming to '%s'\n", s->file_name);
            
        update_file_name(p, s);
    }
    
    /*
     * open a new file and begin writing whatever part of the buffer is left
     */
    
    s->fp = sf_open(s->tmpfile_name, SFM_WRITE, &p->info); 
    
    if (NULL == s->fp){
        fprintf(stderr,  "error opening file %s: %s\n", s->tmpfile_name, sf_strerror(s->fp));
        return 0;
    }
    
    if (p->debug)
        fprintf(stderr, "writing to '%s'\n", s->tmpfile_name);
    
    sf_write_fcn(s->fp, buf + file_start_ix, (sf_count_t) nbuf - file_start_ix);
    
    s->next_file_time += p->file_duration;
    
    return 0;
}

void * state_initialize(void * parameters)
{
    state_t * state = rd_malloc(sizeof(state_t));
    parameters_t * p = (parameters_t *) parameters;
    struct timeval now;
        
    gettimeofday(&now, NULL);
    state->next_file_time = p->file_duration * ((now.tv_sec / p->file_duration) + 1);
    state->fp = NULL;
    
    update_file_name(p, state);
    
    return (void *) state;
}

void cleanup(void * parameters, void * state)
{
    state_t * s = (state_t *) state;
    sf_close(s->fp);
}

/**
 * call the specified report command on a schedule, and also in response 
 * to a filesystem-based trigger.
 */

int main(int argc, char * argv[])
{
    rd_audio_driver_t * driver;
    sound_buffer_t * buffer;
    rd_time_t t;
    parameters_t param;
    state_t * state;
    sample_t * buf;
    int c, daemon = 1, channel = 0;
    char * name = xstr(DEFAULT_NAME);
    char * path = xstr(DEFAULT_PATH);
    char * atype = "jack";
    
     char * usage = 
    "Usage: rd_record [-d] [-c channel] [-r sample rate] [-t file_duration] [-n name]\n\
    -d : don't daemonize\n\
    -c : record from channel <channel> (default: 0)\n\
    -r : record at sample reate <sample_rate> (default: 8000)\n\
    -t : record files of duration <file_duration> seconds (default: 60)\n\
    -p : record in location <path> (default: DEFAULT_PATH)\n\
    -b : record at bit-depth <b> (default: 24)\n\
    -f : record to files of specified format [flac,wav,aiff], (default: flac)\n\
    -F : use strftime format string for file names (default: %Y%m%d-%H%M%S)\n\
    -n : name the process <name> (default: DEFAULT_NAME), this lets\
    you run multiple instances...\n\
    -h : print this message and exit\n\
    For more information, talk to Matt.\n";
    
    param.debug = 0;
    param.sample_rate = 8000;
    param.buf_sec = 3;
    param.file_duration = 60;
    param.bit_depth = 24;
    strcpy(param.file_format, "flac");
    strcpy(param.strftime_format,"%Y%m%d-%H%M%S");
    
    /*
     * handle input
     */
    
    opterr = 0;
     
    while ((c = getopt (argc, argv, "hvdoc:r:t:p:n:a:b:B:f:F:")) != -1)
    {
        switch (c)
        {
            case 'v':
                puts(PACKAGE_VERSION); 
                return 0;
            case 'd':
                param.debug = 1;
                daemon = 0;
                break;
            case 'c':
                channel = atoi(optarg);
                break;
            case 'r':
                param.sample_rate = atoi(optarg);
                break;
            case 't':
                param.file_duration = atoi(optarg);
                break;
            case 'p':
                path = optarg;
                break;
            case 'n':
                name = optarg;
                break;
            case 'a':
                atype = optarg;
                break;
            case 'b':
                param.bit_depth = atoi(optarg);
                break;
            case 'B':
                param.buf_sec = (int) atof(optarg);
                break;
            case 'o':
                DisableChecks = 1;
                break;
            case 'h':
                puts(usage); 
                return 0;
            case 'f':
                strncpy(param.file_format, optarg, sizeof(param.file_format) - 1);
                break;
            case 'F':
                strncpy(param.strftime_format, optarg, sizeof(param.strftime_format) - 1);
                break;
            case '?':
            default:
                puts(usage);
                return 1;
        }
    }
    
    param.info.samplerate = param.sample_rate;
    param.info.channels = 1;
    
    param.info.format = 0;
    switch (param.bit_depth)
    {
        case 16:
        {
            param.info.format |= SF_FORMAT_PCM_16;
            break;
        }
        case 24:
        {
            param.info.format |= SF_FORMAT_PCM_24;
            break;
        }
        default:
        {
            rd_log_message("invalid bit depth specified\n");
            exit(1);
            break;
        }
    }
    
    if (!strcmp(param.file_format, "flac")) {
        param.info.format |= SF_FORMAT_FLAC;
    } else if (!strcmp(param.file_format, "wav")) {
        param.info.format |= SF_FORMAT_WAV;
    } else if (!strcmp(param.file_format, "aiff")) {
        param.info.format |= SF_FORMAT_AIFF;
    } else if (!strcmp(param.file_format, "ogg")) {
        param.info.format = SF_FORMAT_VORBIS | SF_FORMAT_OGG;
    }else {
        rd_log_message("invalid file format specified\n");
        exit(1);
    }
    
    if (!sf_format_check(&param.info))
    {
        rd_log_message("invalid sound file format, cannot continue\n");
        exit(1);
    }
    
    sprintf(param.path, "%s/%s", path, name);
    
    signal(SIGTERM, shutdown_handler);
    signal(SIGINT, shutdown_handler);
    
    if (daemon)
        daemonize(name);
    
    state = (state_t *) state_initialize(&param);
    
    driver = rd_audio_driver_create(
        atype, name, channel, param.sample_rate, BUFLEN, 1, 1, 
            param.buf_sec * param.sample_rate / BUFLEN);
    
    if (driver == NULL)
    {
        rd_log_message("couldn't start audio driver!\n");
        exit(1);
    }
    
    if (driver->rate != param.sample_rate)
    {
        rd_log_message(
            "warning: recorder wanted rate %d, got %d\n", 
                param.sample_rate, driver->rate);
        param.sample_rate = driver->rate;
    }
    
    if (driver->channel != channel)
    {
        rd_log_message(
            "warning: recorder wanted channel %d, got %d\n", 
                channel, driver->channel);
        channel = driver->channel;
    }
    
    buffer = driver->sound_buffer;
    
    rd_audio_driver_start(driver);
    
    while (!Done)
    {    
        sound_buffer_read_wait(buffer);
        
        while (sound_buffer_read_space(buffer) > 0 && !Done)
        {
            buf = sound_buffer_peek(buffer, &t);
            
            record(buf, BUFLEN, &param, state, rd_time_to_timeval(t));
            
            sound_buffer_increment_read_pointer(buffer);
        }
    }
    
    rd_audio_driver_stop(driver);
    //rd_audio_driver_free(driver);
    
    sf_close(state->fp);
    free(state);
    
    return 0;
}

