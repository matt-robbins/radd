#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <radd/detect.h>
#include <Python.h>

#include "rd_memory.h"

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

typedef struct parameters_s {
    char * module_name;
    char * module_path;
    int rate;
    int N;
    int history;
    int latency;
} parameters_t;

typedef struct state_s {
    PyObject *pName, *pModule, *pDict, *pFuncInit, *pFuncDetect;
    PyObject *pArgs, *pValue;
} state_t;

static PyObject *makearray(sample_t * data, size_t size) {
    npy_intp dims[1];
    dims[0] = size;
    
    int k;
    
    PyObject * a = PyArray_SimpleNew(1, dims, NPY_FLOAT);
        
    float * fdata = PyArray_DATA((PyArrayObject *) a);
    
    assert(fdata);
    
    for (k = 0; k < size; k++)
    {
        fdata[k] = (float) data[k];
    }
        
    return a;
}

int detect(event_t * events, sample_t * buf, int nbuf, void * parameters, 
    void * state, sound_setup_t * setup)
{
    state_t * s = state;
    Py_ssize_t k;
    int n_events = 0;
    Py_ssize_t pevent_size; 

    PyGILState_STATE gstate;
    gstate = PyGILState_Ensure();

    PyObject * data = makearray(buf, nbuf);
    PyObject * pevents = NULL, * pevent = NULL;
    
    s->pArgs = PyTuple_New(2);
    PyTuple_SetItem(s->pArgs, 0, data);
    PyTuple_SetItem(s->pArgs, 1, s->pValue);
    
    pevents = PyObject_CallObject(s->pFuncDetect, s->pArgs);
    Py_DECREF(data);
    
    if (!pevents)
    {
        fprintf(stderr, "python function failed to give output!\n"); 
        PyErr_Print(); exit(1);
    }
    
    if (!PyTuple_Check(pevents))
        fprintf(stderr, "must return a tuple!\n");
    
    pevent_size = PyTuple_Size(pevents);
    
    for (k = 0; k < pevent_size; k++)
    {
        pevent = PyTuple_GetItem(pevents, k);
        
        if (!PyTuple_Check(pevent))
        {
            fprintf(stderr, "events must be tuples!\n");
            continue;
        }
            
        if (6 != PyTuple_Size(pevent))
        {
            fprintf(stderr, "each event must have 6 elements!\n");
            continue;
        }

        events[n_events].t_in = PyInt_AsLong(PyTuple_GetItem(pevent, 0));
        events[n_events].t_dt = PyInt_AsLong(PyTuple_GetItem(pevent, 1));
        events[n_events].f_lo = PyFloat_AsDouble(PyTuple_GetItem(pevent, 2));
        events[n_events].f_df = PyFloat_AsDouble(PyTuple_GetItem(pevent, 3));
        events[n_events].score = PyFloat_AsDouble(PyTuple_GetItem(pevent, 4));
        events[n_events].data = rd_strdup(PyString_AsString(PyTuple_GetItem(pevent, 5)));
        
        n_events++; 
        Py_DECREF(pevent);  
    }
    
    Py_DECREF(pevents);
    
    PyGILState_Release(gstate);
    
    return n_events;
}


void * parameter_initialize(char * config_file) 
{
    parameters_t * p = rd_malloc(sizeof(parameters_t));
    int n;
    FILE * fp = fopen(config_file, "r");
    char * cp;
    p->rate = 8000;
    p->N = 1024;
    
    if (fp)
    {
        p->module_name = rd_calloc(64, sizeof(char));
        n = fread(p->module_name, sizeof(char), 63, fp);
        fclose(fp);
        
        p->module_name[n-1] = 0; 
    }
    else
    {
        p->module_name = rd_strdup("detect");
    }
    
    p->module_path = rd_strdup(config_file);
    
    if ((cp = strrchr(p->module_path, '.')) != NULL)
        *cp = 0;
        
    fprintf(stderr, "module_path = %s\n", p->module_path);
    fprintf(stderr, "module_name = %s\n", p->module_name);
    return p;
}

void init_numpy(void)
{
    import_array();
}

void * state_initialize(void * parameters)
{
    state_t * s = rd_malloc(sizeof(state_t));
    parameters_t * p = parameters;
    
    Py_Initialize();
    PyEval_InitThreads();
    PyEval_ReleaseLock();
    PyGILState_STATE gstate;
    gstate = PyGILState_Ensure();
    init_numpy();
    
    char * pyargv[1]; pyargv[0] = "blah";
    PySys_SetArgv(1,pyargv);
        
    s->pModule = PyImport_ImportModule(p->module_name);
    s->pName = PyString_FromString(p->module_name);
    
    if (s->pModule == NULL)
    {
        fprintf(stderr, "couldn't load python module \"%s\"!\n", p->module_name);
        PyErr_Print();
        exit(1);
    }
        
    s->pFuncInit = PyObject_GetAttrString(s->pModule, "init");
    
    if (!(s->pFuncInit && PyCallable_Check(s->pFuncInit))) {
        fprintf(stderr, "couldn't get init() function from module\n");
        exit(1);
    }
    
    s->pFuncDetect = PyObject_GetAttrString(s->pModule, "detect");
    
    if (!(s->pFuncDetect && PyCallable_Check(s->pFuncDetect))) {
        fprintf(stderr, "couldn't get detect() function from module\n");
        exit(1);
    }
    
    s->pArgs = PyTuple_New(1);
    PyTuple_SetItem(s->pArgs, 0, s->pName);
    
    s->pValue = PyObject_CallObject(s->pFuncInit, s->pArgs);
        
    if (!s->pValue)
    {
        PyErr_Print();
    }
    
    /*
     * if we get a {} back, we can grab special parameters
     */
     
    if (PyDict_Check(s->pValue))
    {
        
        PyObject * py_rate = PyDict_GetItemString(s->pValue, "rate");
        PyObject * py_N = PyDict_GetItemString(s->pValue, "N");
        PyObject * py_history = PyDict_GetItemString(s->pValue, "history");
        PyObject * py_latency = PyDict_GetItemString(s->pValue, "latency");

        if (py_rate && PyInt_Check(py_rate))
            p->rate = PyInt_AsLong(py_rate);
            
        if (py_N && PyInt_Check(py_N))
            p->N = PyInt_AsLong(py_N);
            
        if (py_history && PyInt_Check(py_history))
            p->history = PyInt_AsLong(py_history);
            
        if (py_latency && PyInt_Check(py_latency))
            p->latency = PyInt_AsLong(py_latency);
            
        Py_DECREF(py_rate);
        Py_DECREF(py_N);
        Py_DECREF(py_history);
        Py_DECREF(py_latency);
    }
        
    Py_DECREF(s->pName);
    //Py_DECREF(s->pArgs);
    
    PyGILState_Release(gstate);

    return s;
}

sound_setup_t * setup(void * parameters)
{
    static sound_setup_t setup;
    parameters_t * p = (parameters_t *) parameters;
    
    setup.rate = p->rate;
    setup.N = p->N;
    setup.buffer_history = p->history;
    setup.buffer_latency = p->latency;
    
    return &setup;
}

void cleanup(void * parameters, void * state){
    parameters_t * p = (parameters_t *) parameters;
    state_t * s = (state_t *) state;
    
    Py_Finalize();
    
    free(p); free(s);
}

char * api_version(void)
{
    static char * v = RD_API_VERSION;
    return v;
}

#ifdef TEST

int main (int argc, char * argv[])
{
    void * p = parameter_initialize("/tmp/detectors/python.conf");
    sound_setup_t * snd = setup(p);
    void * s = state_initialize(p);
    int k,j, n;
    
    sample_t * buf = rd_calloc(snd->N, sizeof(sample_t));
    
    event_t events[64];
    
    for (k = 0; k < 10; k++)
    {
        n = detect(events, buf, snd->N, p, s, snd);
        
        for (j = 0; j < n; j++)
        {
            free(events[j].data);
        }
    }
    
    cleanup(p, s);
    
    free(buf);
    
    return 0;
}

#endif

