/**\file
 * functions for storing events and metadata into the database (sqlite) and 
 * filesystem (aiff files)
 */

#include "detect.h"
#include "report.h"
#include "rd_error.h"
#include "rd_log.h"
#include "rd_util.h"
#include "rd_time.h"
#include "event_store.h"
#include "event_buffer.h"
#include "detector.h"
#include "config.h"
#include "configure.h"
#include "rd_db.h"
#include "config_store.h"
#include "rd_sndfile.h"
#include "rd_audio_driver.h"
#include "rd_decimator.h"
#include "rd_memory.h"

#include "defaults.h"

#include <stdlib.h>
#include <assert.h>

#if HAVE_LOCKFILE_H==1
    #include <lockfile.h>
#endif

rd_db_t * Db = NULL;

rd_db_t * event_db_init(rd_db_t * db)
{
    char * err;
    int st;
    
    static char Schema[] = 
    "CREATE TABLE IF NOT EXISTS events("
    "   id INTEGER NOT NULL PRIMARY KEY, tags VARCHAR,"
    "   score REAL, channel INT, time DATETIME, frac_time REAL,"
    "   freq REAL, duration REAL, bandwidth REAL, file TEXT,"
    "   rate REAL, data TEXT, author VARCHAR NOT NULL, created DATETIME);";
    
    /*
     * run the initialization sql against the database
     */
    
    rd_db_begin(db);
    st = rd_db_exec(db, Schema, NULL, NULL, &err);
    
    if (st != SQLITE_OK){
        printf("initialization of schema failed! %s\n", err); sqlite3_free(err);
    }
    
    rd_db_commit(db);
    
    /*
     * and prepare the statements for event adding
     */
    
    rd_db_add_stmt(db, "event", "insert",
        "INSERT INTO events VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,datetime('now'));"
    );
    rd_db_add_stmt(db, "event", "setfile",
        "UPDATE events SET file = ? WHERE id = ?;"
    );
    
    return db;
}

/**
 * maintain access to the database, initializing if necessary
 */

rd_db_t * db_access(char * path, char * name)
{
    char file[PATH_MAX];
    sprintf(file, "%s%s", path, name);
    
    /*
     * enforce singleton condition, and free the database on a NULL
     * path string
     */
    
    if (Db != NULL)
    {
        if (path == NULL)
        {
            rd_db_free(Db); 
            Db = NULL; 
            
            return NULL;
        }
        
        if (!fexist(file))
        {
            rd_db_free(Db);
            Db = NULL;
        }
        
        else return Db;
    }
    
    /*
     * initialize the database schema
     */
    
    Db = event_db_init(rd_db_create(file));
    return Db;
}

void db_free()
{
    if (Db) rd_db_free(Db);
}

static int db_prune_cb(void * arg, int cols, char ** values, char ** names)
{
    char * path = arg;
    char * file;
    
    if (cols < 1)
        return 1;
    
    rd_asprintf(&file, "%s/%s", path, values[0]);
    remove(file);
    free(file);
    
    return 0;
}

int db_prune(rd_db_t * db, char * author, char * path)
{
    int result, n_clips, n_events;
    char * err = NULL;
    char * sql;
    
    if (db == NULL)
        return RD_ERROR_DB;
    
    /*
     * for this 'author', get the specific config settings
     */
    
    n_clips = config_get_value_int(NULL, "%s:n_clips", author);
    n_events = config_get_value_int(NULL, "%s:n_events", author);
        
    /*
     * first, delete all but the top-scoring clips 
     */
    
    if (n_clips > 0)
    {
        rd_asprintf(&sql, 
            "SELECT file FROM events WHERE author = '%s' AND id NOT IN \
            (SELECT id FROM events WHERE author = '%s' ORDER BY score DESC, \
            time DESC LIMIT %d) AND file NOT NULL;", 
            author, author,
            n_clips
        );
        
        result = sqlite3_exec(db->db, sql, db_prune_cb, (void *) path, &err); 
        free(sql);
        
        if (result != SQLITE_OK){
            rd_log_message("failed to prune clips: %s\n", err); sqlite3_free(err); return RD_ERROR_DB;
        }
    }
    
    /*
     * next, delete all but top-scoring events
     */
    
    if (n_events > 0)
    {
        rd_asprintf(&sql, 
            "DELETE FROM events WHERE author = '%s' AND id NOT IN \
            (SELECT id FROM events WHERE author = '%s' ORDER BY \
            score DESC, time DESC LIMIT %d);", 
            author, author,
            n_events
        );
        
        result = sqlite3_exec(db->db, sql, NULL, NULL, &err); 
        free(sql);
        
        if (result != SQLITE_OK){
            rd_log_message("failed to prune events: %s\n", err); sqlite3_free(err); return RD_ERROR_DB;
        }       
    }
    
    return RD_ERROR_NO_ERROR;
}

static int get_sndfile_format(char * ext)
{
    int format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
    
    if (!strcmp(ext, "aiff"))
        format = SF_FORMAT_AIFF | SF_FORMAT_PCM_16;
    else if (!strcmp(ext, "flac"))
        format = SF_FORMAT_FLAC | SF_FORMAT_PCM_16;
    else if (!strcmp(ext, "ogg"))
		format = SF_FORMAT_OGG | SF_FORMAT_VORBIS;
    
    return format;
}

/**
 * write an audio file corresponding to a particular event
 */

static int write_event_file(rd_event_t * event, char * path, char * name, int id, char ** fname)
{
    char fpath[PATH_MAX];
    char * file_type;
    int sf_format, file_rate, n;
    SNDFILE * fp;
    SF_INFO sfinfo;
        
    if (NULL == event->samples)
        return RD_ERROR_MISC;
        
    int rate = config_get_value_int(NULL, "%s:clip_rate", name);
    file_rate = rate == 0 ? event->rate : rate;
    
    /*
     * set up to write a 16-bit, single channel file of our desired format
     */
    
    sfinfo.samplerate = file_rate; sfinfo.channels = 1;
    
    file_type = config_get_value_str(NULL, "clip_format");
    
    if (file_type == NULL)
        file_type = DEFAULT_FILE_TYPE;
        
    rd_asprintf(fname, "%s_%.10d.%s", name, id, file_type);    
    sprintf(fpath, "%s/%s", path, *fname);
    
    sf_format = get_sndfile_format(file_type);
    sfinfo.format = sf_format;
    
    fp = sf_open(fpath, SFM_WRITE, &sfinfo);
    
    if (!fp){
        rd_log_message("failed to write sound \"%s\"\n", fpath); return RD_ERROR_FILE;
    }
    
    /*
     * write the event data to the file
     */

    if (file_rate == event->rate)
    {
        n = sf_write_fcn(
            fp, event->padded_samples, 
            (sf_count_t) event->n_padded_samples
        );
        
        if (n != event->n_padded_samples)
        {
			rd_log_message("failed to write audio clip data to file\n");
		}
    }
    
    else if (file_rate < event->rate)
    {
        int quality = config_get_value_int(NULL, "resample_quality");
        int ratio = event->rate / file_rate;
        void * decimator;
        sample_t * buf = rd_calloc((event->n_padded_samples) / ratio, sizeof(sample_t));
        
        decimator = rd_decimator_create(ratio, quality);
        
        int N = rd_decimator_run(
            decimator, event->padded_samples, event->n_padded_samples, 
            buf, event->n_padded_samples/ratio);
            
        rd_decimator_destroy(decimator);
        
        sf_write_fcn(fp, buf, (sf_count_t) N);
        free(buf);
    }
    
    /*
     * clean up and finish
     */
    
    sf_close(fp);
    return RD_ERROR_NO_ERROR;
}

/**
 * classify an event based on its stored samples
 */

static int event_classify(
    rd_event_t * event, classifier_t * classifier, detector_t * detector, char ** label, char ** data)
{
    char ** clabels = NULL; 
    
    if (!classifier)
        return -1;
        
    int use_padded = config_get_value_int(NULL, "%s:classify_padded", classifier->name);
        
    int pad = detector->pad.pre;
    int ppad = detector->pad.post;
        
    sample_t * samples = event->samples;
    int n_samples = event->n_samples;
    
    if (!use_padded)
    {
        samples = samples + pad;
        n_samples -= (pad + ppad);
    }   
        
    event->score = classifier_run(classifier, 
        samples, n_samples, event->rate, &clabels, data) * 10.0;
                
    if (clabels)
    {
        *label = clabels[0];
        free(clabels);
    }
    
    return 0;
}

static int db_event_setfile(rd_db_t * db, int id, char * fname)
{
    rd_db_stmt_t * stmt = rd_db_get_stmt(db, "event", "setfile");
    
    int st = rd_db_stmt_bind(stmt, 1, 2,
        RD_TYPE_STR, fname,
        RD_TYPE_INT, id
    );
    
    if (st != 0)
    {
        fprintf(stderr, "couldn't bind values!\n");
        return -1;
    }
    
    st = rd_db_stmt_exec(stmt, NULL);
    
    if (st != SQLITE_DONE)
    {
        fprintf(stderr, "couldn't insert event: %s\n", sqlite3_errmsg(db->db));
        return -1;
    }
    
    return 0;
}

/**
 * execute the event insertion instructions against the db
 */

static int db_event_insert(
    rd_db_t * db, rd_event_t * event, char * fname, 
    char * cls_label, char * cls_data)
{
    int id, st;    
    rd_db_stmt_t * stmt = rd_db_get_stmt(db, "event", "insert");
    
    if (stmt == NULL)
        return RD_ERROR_DB;
    
    /*
     * bind the event parameters
     */
    
    struct timeval now = rd_time_to_timeval(rd_get_time());
    struct timeval etime = rd_time_to_timeval(event->time);

    int offline = config_get_value_int(NULL, "offline");
    
    if (!offline && abs(etime.tv_sec - now.tv_sec) > 60)
    {
        etime.tv_sec = now.tv_sec;
        etime.tv_usec = now.tv_usec;
    }

    st = rd_db_stmt_bind(stmt, 1, 12,
        RD_TYPE_STR, cls_label ? cls_label : event->class_label,
        RD_TYPE_DBL, (double) event->score,
        RD_TYPE_INT, 1,
        RD_TYPE_INT, etime.tv_sec,
        RD_TYPE_DBL, (double) etime.tv_usec / ONE_SEC_USECS,
        RD_TYPE_DBL, (double) event->freq,
        RD_TYPE_DBL, (double) event->duration / ONE_SEC_USECS,
        RD_TYPE_DBL, (double) event->bandwidth,
        RD_TYPE_STR, fname,
        RD_TYPE_INT, event->rate,
        RD_TYPE_STR, cls_data ? cls_data : event->detector_data,
        RD_TYPE_STR, event->detector_name ? event->detector_name : "unknown"
    );
    
    if (st != 0)
    {
        fprintf(stderr, "couldn't bind values!\n");
        return -1;
    }
    
    /*
     * and execute the statement against the database
     */
     
    rd_db_begin(db);
    st = rd_db_stmt_exec(stmt, NULL);
    
    if (st != SQLITE_DONE)
    {
        fprintf(stderr, "couldn't insert event: %s\n", sqlite3_errmsg(db->db));
        return -1;
    }
    
    id = rd_db_last_insert_row_id(db);
    rd_db_commit(db);
    
    return id;
}


/**
 * put a file in the filesystem to tell the reporting process to go now
 */

int notify(float score)
{
    FILE * fp = fopen(DEFAULT_REPORT_FLAG, "w");
    
    if (fp == NULL)
        return RD_ERROR_FILE;
    
    //fprintf(fp, "%f\n", score);
    fclose(fp);
    
    return RD_ERROR_NO_ERROR;
}

int write_events_pipe(event_buffer_t * event_buffer, detector_t * detector, 
    classifier_t * classifier)
{
    /*
     * get the events out of the circular buffer
     */
    
    int k;
    rd_event_t * events;

    int n_events = event_buffer_read_all(event_buffer, &events);
    char ** clabels, ** cdata;
    
    clabels = alloca(n_events * sizeof(char *));
    cdata = alloca(n_events * sizeof(char *));
    
    for (k = 0; k < n_events; k++)
    {       
        clabels[k] = NULL;
        cdata[k] = NULL;
        event_classify(&events[k], classifier, detector, &clabels[k], &cdata[k]);
                
        printf("%f %f %f %s\n", 
            (double)events[k].time/1000000.0, 
            (double)(events[k].time + events[k].duration)/1000000.0,
            events[k].score, 
            events[k].detector_data
        );
        
        event_free(events[k]);
        
        free(clabels[k]);
        free(cdata[k]);
    }
    
    if (n_events > 0)
        free(events);
    
    return 0;
}


/**
 * write events to sqlite database from main event buffer
 */
 
int write_events(event_buffer_t * event_buffer, detector_t * detector, 
    classifier_t * classifier)
{
    int k, n_events, id;
    char * store_path;
    float report_thresh, store_thresh, clip_thresh;
    rd_db_t * db;
    rd_event_t * events;
    char * file_name = NULL;
    
    char ** clabels, ** cdata;
    
    int pipe = config_get_value_int(NULL,"pipe");
    
    if (pipe)
    {
        return write_events_pipe(event_buffer, detector,classifier);
    }
    
    #if HAVE_LOCKFILE_H==1

    /*
     * check for  event storage lock. If we can't get it, the 
     * consequence is that we will start dropping events from the event
     * buffer.
     */
     
    int result;
     
    if ((result = lockfile_check(EVENT_LOCK_FILE, L_PID)) == 0)
    {
        rd_log_message("couldn't get lock! %d\n", result);
        return RD_ERROR_LOCK;
    }
    
    #endif
        
    /* 
     * open the database if we haven't already done so
     */
    
    store_path = config_get_value_str(NULL, "store_path");
        
    if ((db = db_access(store_path, DEFAULT_DB_FILENAME)) == NULL)
    {
        rd_log_message("couldn't initialize database!\n");        
        return RD_ERROR_FILE;
    }
    
    /*
     * get the events out of the circular buffer
     */
     
    n_events = event_buffer_read_all(event_buffer, &events);
        
    clabels = alloca(n_events * sizeof(char *));
    cdata = alloca(n_events * sizeof(char *));
    
    for (k = 0; k < n_events; k++)
    {       
        clabels[k] = NULL;
        cdata[k] = NULL;
        fprintf(stderr, "score from detector = %f\n", events[k].score);
        event_classify(&events[k], classifier, detector, &clabels[k], &cdata[k]);
        
        fprintf(stderr, "classified score = %f\n", events[k].score);
    }
    
    /*
     * store configuration settings
     */
    
    char *names[] = {
        "store_thresh", "report_thresh", "clip_thresh", 
        "pre:clip_pad", "post:clip_pad", "clip_format", 
        "n_events","n_clips",
        NULL
    };
    
    config_store(db, NULL, detector->name, names);
    
    /* 
     * store the events and their audio clips, and trigger rapid 
     * notification of high-scoring events.
     */
        
    store_thresh = config_get_value_float(NULL, "%s:store_thresh", detector->name);
    report_thresh = config_get_value_float(NULL, "%s:report_thresh", detector->name);
    clip_thresh = config_get_value_float(NULL, "%s:clip_thresh", detector->name);
    
    rd_db_begin(db);
    
    for (k = 0; k < n_events; k++)
    {
        if (events[k].score >= store_thresh)
        {
            rd_log_message("writing event, with time = %d.%.6d seconds, score %f, %s\n", 
                events[k].time/1000000, events[k].time%1000000, 
                events[k].score, events[k].detector_data
            );

            id = db_event_insert(db, &events[k], NULL, clabels[k], cdata[k]);
            
            if (id < 0){
                rd_log_error("Failed to insert event. Event will be lost\n", RD_ERROR_DB);
            }
            
            if (events[k].score >= clip_thresh)
            {
                write_event_file(&events[k], store_path, detector->name, id, &file_name);
                rd_log_message("writing file %s\n", file_name);
                db_event_setfile(db, id, file_name);
            }
            
            if (file_name)
                free(file_name);
            
            if (events[k].score >= report_thresh)
                notify(events[k].score);
        }
        
        /*
         * clean up event
         */
        
        event_free(events[k]);
        
        free(clabels[k]);
        free(cdata[k]);
    }
    
    if (n_events > 0)
        free(events);
    
    /*
     * trim the database down to the number of events and clips we want
     */
    
    db_prune(db, detector->name, store_path);
    
    rd_db_commit(db);
    rd_db_vacuum(db);
        
    if (n_events < 0)
        return RD_ERROR_IO;
    else
        return RD_ERROR_NO_ERROR;
}


#ifdef TEST

int main(int argc, char * argv[])
{
    rd_db_t * db = db_access("/tmp/", "test.db");
    detector_t * det = detector_create(DEFAULT_DETECTOR_PATH, "gillespie");
    event_t event;
    
    if (db == NULL)
    {
        fprintf(stderr, "couldn't access database!\n"); exit(1);
    }
    
    if (det == NULL)
    {
        fprintf(stderr, "couldn't create detector!\n"); exit(1);
    }
    
    db_detector_init(db, det);
    
    rd_db_begin(db);
    rd_db_commit(db);
    
    detector_destroy(det);
    rd_db_free(db);
    
    return 0;
}

#endif
