#include "rd_db.h"
#include "rd_error.h"
#include "rd_util.h"
#include "rd_table.h"
#include "rd_memory.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

rd_db_t * rd_db_create(char * file)
{
    rd_db_t * new = rd_calloc(1, sizeof(rd_db_t));
    new->fullfile = rd_strdup(file);
    
    rd_db_open(new, RD_TRUE);
    
    return new;
}

int rd_db_open(rd_db_t * db, int create)
{
    int status, flags;
    
    if (db->fullfile == NULL)
        return -1;
    
    if (db->db != NULL)
        return 0;
    
    db->stmts = table_create(RD_DB_TABLE_SIZE);
    
    flags = SQLITE_OPEN_READWRITE | (create ? SQLITE_OPEN_CREATE : 0);
    status = sqlite3_open_v2(db->fullfile, &db->db, flags, NULL);
    sqlite3_busy_timeout(db->db, 1000);
    
    return status;
}

void rd_db_close(rd_db_t * db)
{
    int status;
    table_free(db->stmts);
    
    status = sqlite3_close(db->db);
        
    if (status != SQLITE_OK)
        fprintf(stderr, "failed to close Db handle!\n");
    
    db->db = NULL;
    db->stmts = NULL;
}

void rd_db_free(rd_db_t * db)
{    
    rd_db_close(db);
    free(db->fullfile);
    free(db);
}

static void stmt_free(void * in)
{
    rd_db_stmt_t * tmp, * s = (rd_db_stmt_t *) in;
    
    while (s != NULL)
    {
        tmp = s->next;
        
        free(s->sql);
        free(s->name);
        sqlite3_finalize(s->stmt);
        free(s);
        
        s = tmp;
    }
}

void rd_db_stmt_disp(rd_db_stmt_t * stmt)
{
    rd_db_stmt_t * s = stmt;
    
    while (s != NULL)
    {
        printf("Statement: \"%s\":", s->name);
        printf("%s\n", sqlite3_sql(s->stmt));
    }
}

rd_db_stmt_t * rd_db_add_stmt(rd_db_t * db, char * name1, char * name2, char * sql_fmt, ...)
{
    char * tail, sql[RD_DB_MAX_SQL];
    int status;
    va_list arglist;
    rd_db_stmt_t * new = 0, ** old, * p;
    
    if (db->db == NULL)
        return NULL;
    
    va_start(arglist, sql_fmt);
    
    if (vsnprintf(sql, RD_DB_MAX_SQL, sql_fmt, arglist) >= RD_DB_MAX_SQL)
    {
        fprintf(stderr, "rd_db_add_stmt: sql is too long!\n");
    }
    
    va_end(arglist);
    tail = sql;
        
    if (NULL == table_get(db->stmts, name1))
        table_insert(db->stmts, name1, NULL, stmt_free);
    
    old = (rd_db_stmt_t **) table_getp(db->stmts, name1);
        
    while (strlen(tail) > 0)
    {
        new = rd_calloc(1, sizeof(rd_db_stmt_t));
        
        new->name = name2 ? rd_strdup(name2) : NULL;
        new->next = NULL;
        
        status = sqlite3_prepare_v2(db->db, tail, -1, &new->stmt, (const char **) &tail);
                
        if (status != SQLITE_OK)
        {
            printf("%s prepare failed! %s\n",name1, sqlite3_errmsg(db->db));
            printf("%s\n", tail);
            break;
        }
        
        new->sql = rd_strdup(sqlite3_sql(new->stmt));
        
        /* 
         * stick it on the tail of the list
         */
        
        if (*old == NULL)
        {
            *old = new;
        }
        
        else
        {
            p = *old;
            
            while (p->next != NULL)
                p = p->next;
            
            p->next = new;
        }
    } 
    
    return new;
}

rd_db_stmt_t * rd_db_get_stmt(rd_db_t * db, char * name1, char * name2)
{
    rd_db_stmt_t * stmt = table_get(db->stmts, name1);
    
    if (NULL == name2)
        return stmt;
    
    while (strcmp(stmt->name, name2) && stmt->next != NULL)
        stmt = stmt->next;

    return stmt;
}

int rd_stmt_bind_values(rd_db_stmt_t * stmt, rd_value_t * v, int ix, int n)
{
    int k, status; 
    
    for (k = 0; k < n; k++)
    {
        switch (v[k].type)
        {
            case RD_TYPE_INT:
                status = sqlite3_bind_int(stmt->stmt, k + ix, v[k].d.i);
                break;
            case RD_TYPE_DBL:
                status = sqlite3_bind_double(stmt->stmt, k + ix, v[k].d.f);
                break;
            case RD_TYPE_STR:
                status = sqlite3_bind_text(
                    stmt->stmt, k + ix, v[k].d.s, -1, SQLITE_TRANSIENT);
                break;
        }
        
        if (status != SQLITE_OK)
        {
            return RD_ERROR_DB;
        }
    }
    
    return 0;
}

int rd_db_stmt_bind_row(rd_db_stmt_t * stmt, rd_vector_t * v, int row, int ix)
{
    if (row > v->nrows)
        return -1;
    
    rd_value_t * val = &v->data[row * v->ncols];
    
    return rd_stmt_bind_values(stmt, val, ix, v->ncols);
}

int rd_db_stmt_bind(rd_db_stmt_t * stmt, int ix, int N, ...)
{
    int k;
    int status;
    char * val;
    va_list arglist;
    va_start(arglist, N);
    
    for (k = ix; k < ix + N; k++)
    {
        switch (va_arg(arglist, int))
        {
            case RD_TYPE_INT:
                status = sqlite3_bind_int(stmt->stmt, k, va_arg(arglist, int));
                break;
            case RD_TYPE_DBL:
                status = sqlite3_bind_double(stmt->stmt, k, va_arg(arglist, double));
                break;
            case RD_TYPE_STR:
                val = va_arg(arglist, char *);
                status = sqlite3_bind_text(stmt->stmt, k, val, -1, SQLITE_TRANSIENT);
                break;
        }
        
        if (status != SQLITE_OK)
        {
            fprintf(stderr, "while binding parameter %d, %d\n", k, status);
            return status;
        }
    }
    
    va_end(arglist);
    return 0;
}

void rd_db_stmt_list(rd_db_stmt_t * stmt)
{
    rd_db_stmt_t * p = stmt;
    int ix = 0;
    
    while (p != NULL)
    {
        printf("%d: %s: \"%s\"\n", ix++, p->name, sqlite3_sql(p->stmt));
        p = p->next;
    }
}

int rd_db_stmt_exec(rd_db_stmt_t * stmt, rd_vector_t ** out)
{
    int k, ncols, result = SQLITE_OK;
    rd_db_stmt_t * st = stmt;
    rd_vector_t * v = NULL;
        
    while (st != NULL)
    {        
        result = SQLITE_OK;
        
        while (result != SQLITE_DONE)
        {
            result = sqlite3_step(st->stmt);
            
            if (result == SQLITE_ROW)
            {
                ncols = sqlite3_column_count(st->stmt);
                
                if (out != NULL)
                {
                    v = rd_vector_create(1, ncols);
                    
                    for (k = 0; k < ncols; k++)
                    {
                        switch (sqlite3_column_type(st->stmt, k))
                        {
                            case SQLITE_INTEGER:
                                rd_vector_assign(v, k, 1, 
                                    RD_TYPE_INT, sqlite3_column_int(st->stmt, k)
                                );
                                break;
                            case SQLITE_FLOAT:
                                rd_vector_assign(v, k, 1, 
                                    RD_TYPE_DBL, sqlite3_column_double(st->stmt, k)
                                );
                                break;
                            case SQLITE_TEXT:
                                rd_vector_assign(v, k, 1, 
                                    RD_TYPE_STR, rd_strdup((char *) sqlite3_column_text(st->stmt, k))
                                );
                        }
                    }
                    
                    *out = v;
                }
            }
            
            if (result != SQLITE_ROW && result != SQLITE_DONE)
            {
                break;
            }
        }
        
        sqlite3_reset(st->stmt);
        st = st->next;
    }
    
    return result;
}

int rd_db_stmt_reset(rd_db_stmt_t * stmt)
{
    rd_db_stmt_t * st = stmt;
    int result = 0;
    
    while (st != NULL)
    {
        result = sqlite3_reset(stmt->stmt); st = st->next;
    }
    
    return result;
}

int rd_db_exec(rd_db_t * db, char * sql, 
    int (*cb)(void *, int, char **, char **), void * arg, char ** err)
{
    return sqlite3_exec(db->db, sql, cb, arg, err);
}

int rd_db_begin(rd_db_t * db)
{
    return sqlite3_exec(db->db, "BEGIN;", NULL, NULL, NULL);
}

int rd_db_commit(rd_db_t * db)
{
    return sqlite3_exec(db->db, "COMMIT;", NULL, NULL, NULL);
}

int rd_db_vacuum(rd_db_t * db)
{
    return sqlite3_exec(db->db, "VACUUM;", NULL, NULL, NULL);
}

int rd_db_last_insert_row_id(rd_db_t * db)
{
    return sqlite3_last_insert_rowid(db->db);
}


