/**\file 
 * functions and types to maintain a table of child processes.
 */

#include "process_table.h"
#include "rd_error.h"
#include "rd_memory.h"

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>


static process_table_t * get_process_table(int mode);

/**
 * an external accessor for a singleton process table
 */

process_table_t * process_table()
{
    process_table_t * t = get_process_table(TABLE_GET);
    
    if (t == NULL)
        t = get_process_table(TABLE_CREATE);
    
    return t;
}

/**
 * internal accessor, which provides a mode input for clearing the table
 */

static process_table_t * get_process_table(int mode)
{
    static process_table_t * table = NULL;
    
    switch (mode){
        
        case TABLE_GET:
            if (table != NULL)
                break;
        
        case TABLE_CREATE:
            table = rd_calloc(1, sizeof(process_table_t));
            
            if (table == NULL){
                fprintf(stderr, "couldn't allocate process table!\n"); return NULL;
            }
            
            pthread_mutex_init(&table->inuse, NULL);
            table->N = 0;
            
            break;
        
        case TABLE_CLEAR:
            process_table_free(table);
            break;
        
    }
    
    return table;
}

/**
 * clear a table from memory
 */

void process_table_free(process_table_t * table)
{
    int k;
    
    if (table == NULL)
        return;
    
    pthread_mutex_destroy(&table->inuse);
    
    for (k = 0; k < table->N; k++)
        free(table->entries[k].info);
    
    free(table->entries);
    free(table); 
    table = NULL;
}

/**
 * external initializer for singleton table
 */

int process_table_init()
{
    process_table_t * t = get_process_table(TABLE_CREATE);
    
    return t == NULL ? 1 : 0;
}

/**
 * find a process by pid
 */

int process_table_find_entry(process_table_t * t, pid_t pid)
{
    int k;
    
    for (k = 0; k < t->N; k++){
        if (t->entries[k].pid == pid)
            return k;
    }
    
    return -1;
}

/**
 * prepare to add a new process to the table, by locking the mutex
 */

int process_table_lock(process_table_t * t)
{
    return pthread_mutex_lock(&t->inuse);
}

int process_table_unlock(process_table_t * t)
{
    return pthread_mutex_unlock(&t->inuse);
}

/**
 * add a new process to the table
 */

int process_table_add(pid_t pid, char * info, int read, int write)
{
    process_table_entry_t * entries;
    process_table_t * t = get_process_table(TABLE_GET);
        
    process_table_lock(t);
    
    entries = rd_realloc(t->entries, ++t->N * sizeof(process_table_entry_t));
    
    if (entries == NULL)
    {
        process_table_unlock(t);
        return RD_ERROR_MALLOC;
    }
    
    t->entries = entries;
    
    t->entries[t->N - 1].comm[0] = read;
    t->entries[t->N - 1].comm[1] = write;
    t->entries[t->N - 1].pid = pid;
    t->entries[t->N - 1].info = rd_strdup(info);
    t->entries[t->N - 1].status = -1;
    pthread_cond_init(&t->entries[t->N - 1].status_ready, NULL);

    process_table_unlock(t);
    
    return RD_ERROR_NO_ERROR;
}


/**
 * remove an entry from the proceess table.  This is slow because we're not using
 * a linked list like we should be, but so far, this is never called.
 */

int process_table_remove(pid_t pid)
{
    int k, j;
    process_table_entry_t * entries;
    process_table_t * t = get_process_table(TABLE_GET);
    
    k = process_table_find_entry(t, pid);
        
    if (k < 0)
        return -1;
    
    process_table_lock(t);
    
    free(t->entries[k].info);
    
    for (j = k; j < t->N - 1; j++)
        t->entries[j] = t->entries[j + 1];
        
    entries = rd_realloc(t->entries, --t->N * sizeof(process_table_entry_t));
    
    process_table_unlock(t);
    
    if (entries == 0)
        return RD_ERROR_MALLOC;
    
    t->entries = entries;
    return RD_ERROR_NO_ERROR;
}

/**
 * return information (value) about a process with pid (key)
 */

char * process_table_get_info(pid_t pid)
{
    int k;
    process_table_t * t = get_process_table(TABLE_GET);
    
    process_table_lock(t);
    k = process_table_find_entry(t, pid);
    process_table_unlock(t);
    
    if (k < 0)
        return NULL;
    
    return t->entries[k].info;
}

/**
 * attempt to find the pid of a process whose "info" is info
 */

int process_table_get_pid(char * info)
{
    int k;
    process_table_t * t = get_process_table(TABLE_GET);
    
    for (k = 0; k < t->N; k++){
        if (!strcmp(t->entries[k].info, info))
            return t->entries[k].pid;
    }
    
    return -1;
}

/**
 * set the status of a process table entry
 */

int process_table_set_status(pid_t pid, int status)
{
    int k;
    process_table_t * t = get_process_table(TABLE_GET);
    
    for (k = 0; k < t->N; k++)
    {
        if (t->entries[k].pid == pid)
        {
            process_table_lock(t);
            
            t->entries[k].status = status;
            
            pthread_cond_signal(&t->entries[k].status_ready);
            process_table_unlock(t);
            return 0;
        }
    }
    
    return -1;
}

/**
 * get the exit status associated with a process
 */

int process_table_get_status(pid_t pid, int timeout)
{
    int k, err = 0, status = -1;
    process_table_t * t = get_process_table(TABLE_GET);
    struct timespec tp;
    
    for (k = 0; k < t->N; k++)
    {
        if (t->entries[k].pid != pid)
            continue;
            
        if (t->entries[k].status != -1)
        {
            status = t->entries[k].status;
            process_table_remove(pid);
            return status;
        }

        process_table_lock(t);
        
        err = clock_gettime(CLOCK_REALTIME, &tp);
        
        if (err)
            timeout = 0;
        tp.tv_sec += timeout;

        if (timeout <= 0)
            err = pthread_cond_wait(&t->entries[k].status_ready, &t->inuse);
        else
            err = pthread_cond_timedwait(&t->entries[k].status_ready, &t->inuse, &tp);
        
        status = t->entries[k].status;
        process_table_unlock(t);
        process_table_remove(pid);
        
        if (err == ETIMEDOUT)
            return -2;
        
        return status;
    }
    
    return -1;
}

int process_table_get_count(void)
{
	process_table_t * t = get_process_table(TABLE_GET);
	
	return t->N;
}


/**
 * dump a list of all processes and their associated info
 */

char * process_table_dump()
{
    int k;
    static char str[1024];
    char * p;
    
    process_table_t * t = get_process_table(TABLE_GET);
    p = str;
    
    for (k = 0; k < t->N; k++){
        p += sprintf(p, "%d,%s;", t->entries[k].pid, t->entries[k].info);
    }
    
    printf("%s\n",str);
    return str;
}


