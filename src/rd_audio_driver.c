#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <limits.h>
#include <assert.h>

#include "rd_memory.h"
#include "rd_audio_driver.h"
#include "rd_decimator.h"
#include "rd_util.h"
#include "rd_log.h"
#include "rd_filestream.h"
#include "configure.h"
#include "config.h"

#ifndef MIN
    #define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#endif

typedef struct files_info_s {
    file_stream_t * fs;
    pthread_t read_thread_id;
    sample_t * buf;
    int done;
} files_info_t;

typedef struct fd_info_s {
    int fd;
    pthread_t read_thread_id;
    sample_t * buf;
    int N;
} fd_info_t;

static int 
audio_process(sample_t * in, int N, rd_time_t timestamp, rd_audio_driver_t * driver);

#if HAVE_LIBJACK==1
#include <jack/jack.h>

typedef struct jack_info_s {
    jack_port_t * input_port;
    jack_client_t * client;
    sample_t * buf;
    char * port_name;
    int rate;
    int N;
} jack_info_t;

static int 
_jack_callback(jack_nframes_t nframes, void * arg)
{
    rd_audio_driver_t * driver = (rd_audio_driver_t *) arg;
    jack_info_t * jack_info = (jack_info_t *) driver->data;
    jack_default_audio_sample_t * in;
    static rd_time_t timestamp;
    
    /**
     * get time stamp, and convert to sample_t, then call generic
     * audio processing function.
     */
    
    int offset = jack_frames_since_cycle_start(jack_info->client);
    
    timestamp = rd_get_time() - (1000000 * offset) / jack_info->rate;
    
    in = jack_port_get_buffer(jack_info->input_port, nframes);
    
    #if FIXED_POINT==32
    for (int k = 0; k < nframes; k++)
        jack_info->buf[k] = (sample_t) (1<<31) * in[k];
    #else
        memcpy(jack_info->buf, in, nframes * sizeof(sample_t));
    #endif
    
    audio_process(
        jack_info->buf, nframes, timestamp, driver
    );
   
    return 0;      
}

static void *
driver_create_jack(rd_audio_driver_t * driver, char * name, int * native_rate, int * native_n)
{
    jack_options_t options = JackNullOption | JackNoStartServer;
    jack_status_t status;
    jack_port_t * port;
    int k;
    const char ** ports;
    char * client_name;
    jack_info_t * info = rd_calloc(1, sizeof(jack_info_t));
    
    jack_client_t * client = jack_client_open (name, options, &status, JackNoStartServer);
    
    if (client == NULL) 
    {
        fprintf (stderr, "jack_client_open() failed, status = 0x%2.0x\n", status);
        
        if (status & JackServerFailed)
            fprintf (stderr, "Unable to connect to JACK server\n");
        
        return NULL;
    }
    
    ports = jack_get_ports(client, NULL, NULL, JackPortIsPhysical|JackPortIsOutput);
    
    if (ports == NULL) {
        fprintf(stderr, "no ports from which to capture.\n");
        jack_client_close(client);
        return NULL;
    }
    
    for (k = 0; ports[k] != NULL; k++) {};
    
    if (driver->channel < 0)
        driver->channel = 0;
    else if (driver->channel >= k)
        driver->channel = k - 1;
    
    info->port_name = rd_strdup(ports[driver->channel]);    
    free(ports);
    
    client_name = jack_get_client_name(client);
    jack_set_process_callback(client, _jack_callback, driver);
    
    info->rate = *native_rate = jack_get_sample_rate(client);
    info->N = *native_n = jack_get_buffer_size(client);
    
    port = jack_port_register(
        client, "input",
        JACK_DEFAULT_AUDIO_TYPE,
        JackPortIsInput, 0);
        
    if (port == NULL){
        fprintf(stderr, "can't create input port for %s\n", client_name);
        return NULL;
    }
    
    info->buf = rd_calloc(info->rate, sizeof(sample_t));
    info->client = client;
    info->input_port = port;
    
    driver->data = info;
    
    return (void *) info;
}

static int
driver_start_jack(rd_audio_driver_t * driver)
{
    int status;
    jack_info_t * info = (jack_info_t *) driver->data;
    const char * input;
    
    if (jack_activate (info->client)) 
    {
        fprintf (stderr, "cannot activate client");
        return -1;
    }
    
    input = jack_port_name(info->input_port);
    status = jack_connect (info->client, info->port_name, input);
    
    if (status == EEXIST)
        fprintf (stderr, "cannot connect input port: already connected\n");
    else if (status != 0)
        fprintf (stderr, "cannot connect input port: 0x%x\n", status);
    
    return 0;
}

static int 
driver_stop_jack(rd_audio_driver_t * driver)
{
    jack_info_t * info = (jack_info_t *) driver->data;
    return jack_deactivate(info->client);
}

static inline double timeval_to_double(struct timeval v)
{
    return (double) v.tv_sec + (double) v.tv_usec / 1e6;
}

static int
driver_free_jack(rd_audio_driver_t * driver)
{
    jack_info_t * info = (jack_info_t *) driver->data;
    
    free(info->port_name);
    free(info->buf);
    jack_client_close(info->client);
    free(info);
    
    return 0;
}

#else // JACK

static void *
driver_create_jack(rd_audio_driver_t * driver, char * name, int * native_rate, int * native_n)
{
    rd_log_message("RADd built without JACK support.  Jack audio driver disabled\n");
    exit(1);
}

static int driver_start_jack(rd_audio_driver_t * driver){ return 0; }
static int driver_free_jack(rd_audio_driver_t * driver){ return 0; }
static int driver_stop_jack(rd_audio_driver_t * driver){ return 0; }

#endif // HAVE_JACK


#if HAVE_ALSA_ASOUNDLIB_H==1
#include <alsa/asoundlib.h>

typedef struct alsa_info_s {
    snd_pcm_t * h;
    sample_t * buf;
   void * native_buf;
    pthread_t read_thread_id;
    int nchan;
    snd_pcm_format_t format;
    int ch;
    int N;
} alsa_info_t;


static void *
driver_create_alsa(rd_audio_driver_t * driver, char * arg, int * rate, int N)
{
    alsa_info_t * info = rd_calloc(1, sizeof(alsa_info_t));
    int err;
    char * devname = arg;
    snd_pcm_hw_params_t *hw_params;
    
    if (devname == NULL)
        devname = "default";
    
    if ((err = snd_pcm_open (&info->h, devname, SND_PCM_STREAM_CAPTURE, 0)) < 0) 
    {
        fprintf(stderr, 
            "cannot open audio device %s (%s)\n", devname,snd_strerror(err));
        free(info);
        return NULL;
    }
    
    if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0) 
    {
        fprintf (stderr, "cannot allocate hardware parameter structure (%s)\n",
             snd_strerror (err));
        exit (1);
    }
             
    if ((err = snd_pcm_hw_params_any (info->h, hw_params)) < 0) 
    {
        fprintf (stderr, "cannot initialize hardware parameter structure (%s)\n",
             snd_strerror (err));
        exit (1);
    }

    if ((err = snd_pcm_hw_params_set_access (info->h, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) 
    {
        fprintf (stderr, "cannot set access type (%s)\n",
             snd_strerror (err));
        exit (1);
    }

    info->format = SND_PCM_FORMAT_S32_LE;
    if ((err = snd_pcm_hw_params_set_format (info->h, hw_params, info->format)) < 0)
    {
        info->format = SND_PCM_FORMAT_S24_LE;
        if ((err = snd_pcm_hw_params_set_format (info->h, hw_params, info->format)) < 0) 
        {
            info->format = SND_PCM_FORMAT_S16_LE;
            if ((err = snd_pcm_hw_params_set_format (info->h, hw_params, info->format)) < 0) 
            {
                fprintf (stderr, "cannot set sample format (%s)\n",
                     snd_strerror (err));
                exit (1);
            }
        }
    }

    if ((err = snd_pcm_hw_params_set_rate_near (info->h, hw_params, (unsigned int *) rate, 0)) < 0) 
    {
        fprintf (stderr, "cannot set sample rate (%s)\n",
             snd_strerror (err));
        exit (1);
    }

    err = 0;
    unsigned int chan = driver->channel + 1;
    if ((err = snd_pcm_hw_params_set_channels_near (info->h, hw_params, &chan)) < 0) 
    {
        fprintf (stderr, "cannot set channel count (%s)\n",
             snd_strerror (err));
        exit (1);
    }
    
    info->nchan = chan;
    
    fprintf(stderr, "channels = %d\n", chan);
    info->ch = driver->channel;
    if (info->ch > info->nchan - 1)
        info->ch = info->nchan -1;
    if (info->ch < 0)
        info->ch = 0;
    
    if ((err = snd_pcm_hw_params (info->h, hw_params)) < 0) 
    {
        fprintf (stderr, "cannot set parameters (%s)\n",
             snd_strerror (err));
        exit (1);
    }

    snd_pcm_hw_params_free (hw_params);
    
    info->buf = rd_calloc(N, sizeof(sample_t));
    info->native_buf = rd_calloc(N * info->nchan, sizeof(int32_t));
    info->N = N;
    
    return (void *) info;
}

static void * alsa_read_thread(void * arg)
{
    int n;
    rd_audio_driver_t * driver = arg;
    alsa_info_t * info = driver->data;
    
    int oldstate, oldtype;
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldstate);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldtype);
    
    rd_time_t timestamp;
            
    while (1)
    {
        n = snd_pcm_readi (info->h, info->native_buf, info->N);
        
        timestamp = rd_get_time();
        
        for (int k = 0; k < info->N; k++)
        {
            int32_t samp;
            #ifndef FIXED_POINT
            float scale = (float)(1<<31);
            #endif
            
            if (info->format == SND_PCM_FORMAT_S32_LE)
            {
                int32_t * bp = info->native_buf;
                samp = (int32_t) bp[k*info->nchan + info->ch];
            }
            else if (info->format == SND_PCM_FORMAT_S24_LE)
            {
                int32_t * bp = info->native_buf;
                samp = ((int32_t) bp[k*info->nchan + info->ch]) << 8;
                #ifndef FIXED_POINT
                scale = (float)(1<<31);
                #endif
            }
            else
            {
                int16_t * bp = info->native_buf;
                samp = (int32_t) bp[k*info->nchan + info->ch];
                #ifndef FIXED_POINT
                scale = (float) (1<<15);        
                #endif        
            }
            
            #ifdef FIXED_POINT
            info->buf[k] = samp;
            #else
            info->buf[k] = (float) samp / scale;
            #endif
        }
        
        if (n != info->N)
        {
            driver->sound_buffer->eof = 1;
            sound_buffer_read_signal(driver->sound_buffer);
            break;
        }
        
        audio_process(
            info->buf, n, timestamp, driver
        );
    }
    
    return NULL;
}

static int
driver_start_alsa(rd_audio_driver_t * driver)
{
    int err = 0;
    alsa_info_t * info = driver->data;
    
    if ((err = snd_pcm_prepare (info->h)) < 0) 
    {
        fprintf (stderr, "cannot prepare audio interface for use (%s)\n",
             snd_strerror (err));
        exit (1);
    }
    
    err = pthread_create(&info->read_thread_id, NULL, alsa_read_thread, (void *) driver);
    
    return err;
}

static int
driver_stop_alsa(rd_audio_driver_t * driver)
{
    alsa_info_t * info = driver->data;
    pthread_cancel(info->read_thread_id);

    void * res;
    pthread_join(info->read_thread_id, &res);    
    return 0;
}

static void driver_free_alsa(rd_audio_driver_t * driver)
{
    alsa_info_t * info = driver->data;
    
    snd_pcm_close(info->h);
    free(info->buf);
    
    free(info);
}

#else // ALSA_H

static void *
driver_create_alsa(rd_audio_driver_t * driver, char * arg, int * rate, int N)
{
    rd_log_message("RADd built without ALSA support.  ALSA audio driver disabled\n");
    exit(1);
}

static int driver_start_alsa(rd_audio_driver_t * driver){ return 0; }
static int driver_free_alsa(rd_audio_driver_t * driver){ return 0; }
static int driver_stop_alsa(rd_audio_driver_t * driver){ return 0; }

#endif // ALSA_H

static void *
driver_create_fd(rd_audio_driver_t * driver, char * arg, int N)
{
    driver->realtime = 1;
    
    fd_info_t * info = rd_calloc(1, sizeof(fd_info_t));
        
    info->buf = rd_calloc(N, sizeof(sample_t));
    info->N = N;
    
    if (arg == NULL)
    {
        info->fd = STDIN_FILENO;
    }
    
    return (void *) info;
}

static void * fd_read_thread(void * arg)
{
    int n;
    rd_audio_driver_t * driver = arg;
    fd_info_t * info = driver->data;
    
    rd_time_t timestamp;
    
    int oldstate, oldtype;
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldstate);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldtype);
            
    while (1)
    {
        timestamp = rd_get_time();
        
        n = fread(info->buf, sizeof(sample_t), info->N, stdin);
        
        if (n != info->N)
        {
            driver->sound_buffer->eof = 1;
            sound_buffer_read_signal(driver->sound_buffer);
            break;
        }
        
        sound_buffer_write_wait(driver->sound_buffer);

        audio_process(
            info->buf, n, timestamp, driver
        );
    }
    
    return NULL;
}

static int
driver_start_fd(rd_audio_driver_t * driver)
{
    int err = 0;
    fd_info_t * info = driver->data;
    
    err = pthread_create(&info->read_thread_id, NULL, fd_read_thread, (void *) driver);
    
    return err;
}

static int
driver_stop_fd(rd_audio_driver_t * driver)
{
    fd_info_t * info = driver->data;
    pthread_cancel(info->read_thread_id);

    void * res;
    pthread_join(info->read_thread_id, &res);    
    return 0;
}

static void driver_free_fd(rd_audio_driver_t * driver)
{
    fd_info_t * info = driver->data;
    free(info->buf);
    free(info);
}


static void *
driver_create_files(rd_audio_driver_t * driver, char * arg, int * native_rate)
{
    files_info_t * info = rd_calloc(1, sizeof(files_info_t));
    char * buf = NULL, * ext, * path;
        
    path = strtok_r(arg, ".", &buf);
    ext = strtok_r(NULL, ".", &buf);
    
    if (ext == NULL)
        ext = "wav";
    
    info->fs = file_stream_create(path, ext);
    
    if (info->fs == NULL)
    {
        free(info); 
        fprintf(stderr,"couldn't create FILES driver!\n"); return NULL;
    };
    
    driver->realtime = 0;
    driver->data = info;
    *native_rate = info->fs->info.samplerate;
    
    info->buf = rd_malloc(driver->N * sizeof(sample_t));
    
    return (void *) info;
}

static void * read_thread(void * arg)
{
    int n;
    rd_audio_driver_t * driver = arg;
    files_info_t * info = driver->data;
    file_stream_t * fs = info->fs;
    rd_time_t timestamp;
            
    int oldstate,oldtype;
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldstate);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldtype);
    
    while (1)
    {
        n = file_stream_read(fs, info->buf, driver->N, &timestamp);
                
        if (n == 0)
        {
            driver->sound_buffer->eof = 1;
            sound_buffer_read_signal(driver->sound_buffer);
            break;
        }
        
        sound_buffer_write_wait(driver->sound_buffer);

        audio_process(
            info->buf, n, timestamp, driver
        );
    }
    
    return NULL;
}

static int
driver_start_files(rd_audio_driver_t * driver)
{
    int err;
    files_info_t * info = driver->data;
    
    err = pthread_create(&info->read_thread_id, NULL, read_thread, (void *) driver);
    return err;
}

static int
driver_stop_files(rd_audio_driver_t * driver)
{
    files_info_t * info = driver->data;
    pthread_cancel(info->read_thread_id);

    void * res;
    pthread_join(info->read_thread_id, &res);    
    return 0;
}

static void driver_free_files(rd_audio_driver_t * driver)
{
    files_info_t * info = driver->data;
    file_stream_free(info->fs);
    free(info->buf);
    free(info);
}


/**
 * stuff that we do for audio from any source
 */

static int 
audio_process(sample_t * in, int N, rd_time_t timestamp, rd_audio_driver_t * driver)
{
    int nwrite, nleft, input_n;
    sample_t * wp;
    int ixi = 0; // index into *in
    static rd_time_t time = 0;
        
    /*
     * if our buffer size is smaller than the system buffer, then we
     * will have to iterate, so we take chunks until ixi >= N. 
     * 
     * Depending on the decimation ratio, we also need to make sure we
     * accumulate enough samples such that we use them all, and there
     * are enough to generate at least one driver->N sized buffer of
     * output
     */
     
    input_n = driver->N * driver->decimate;
    
    while (ixi < N)
    {
        /*
         * either write the rest of the buffer * in, or use as much of
         * it as will fit in the driver's input buffer
         */
         
        nwrite = MIN(N - ixi, input_n - driver->ix);
        
        memcpy(driver->input_buffer + driver->ix, in + ixi, nwrite * sizeof(sample_t));
        driver->ix += nwrite;
        ixi += nwrite;
        
        nleft = N - ixi;
        
        /*
         * do nothing if we haven't filled the input buffer
         */
         
        if (driver->ix < input_n)
            continue;
                    
        if (!sound_buffer_write_space(driver->sound_buffer))
        {
            rd_log_message("xrun!\n");
            break;
        }
        
        wp = sound_buffer_get_write_pointer(driver->sound_buffer);
        
        if (wp == NULL)
        {
            fprintf(stderr, "sound buffer write pointer is NULL! BAD\n");
            return -1;
        }
        
        int n = rd_decimator_run(
            driver->decimator, driver->input_buffer, input_n, wp, driver->N
        );
        
        if (n != driver->N)
        {
			rd_log_message("decimation failed!\n");
			exit(1);
		}
        
        /*
         * calculate the time at the beginning of the input buffer based
         * on the last timestamp and how many samples we used.
         */
                
        time = timestamp - 
            ((rd_time_t) ((input_n - nwrite) - (ixi - nwrite)) * ONE_SEC_USECS_L) 
            / (driver->rate * driver->decimate);
            
        time = rd_dll_update(&driver->dll, time);
        
        sound_buffer_increment_write_pointer(driver->sound_buffer, time);
        //print_sound_buffer(driver->sound_buffer);
        
        /*
         * now, copy the *rest* of the *in buffer into the driver's 
         * input buffer, if necessary.
         */
         
        if (nleft < input_n)
        {
            memcpy(driver->input_buffer, in + ixi, nleft * sizeof(sample_t));
            driver->ix = nleft;
        }
        else
            driver->ix = 0;
        
    }
    
    return 0;      
}

static inline int
rate_supported(int native_rate, int requested_rate)
{
    float ratio;
    
    if (native_rate < requested_rate)
        return 0;
    
    ratio = (float) native_rate / requested_rate;
    
    return (int) ratio == native_rate / requested_rate;
}

static int parse_driver_type(char * in, char ** type, char ** arg)
{
    char * buf;
    char *types[] = RD_AUDIO_TYPES_INIT;
    int k;
        
    *type = strtok_r(in, ":", &buf);
    *arg = strtok_r(NULL, "", &buf);
            
    for (k = 0; k < sizeof(types)/sizeof(char *); k++)
        if (!strcmp(*type, types[k]))
            return k;
    
    return -1;
}

/**
 * create an audio driver, taking requested rate and buffer size, and 
 * returning an audio driver object with actual rate and buffer size.
 */

rd_audio_driver_t * 
rd_audio_driver_create(
    char * type, char * name, int channel, int rate, int N,
    int latency, int history, int slack
)
{
    int native_rate = rate, native_n, k;
    rd_audio_driver_t * ad = rd_calloc(1, sizeof(rd_audio_driver_t));
    
    ad->type = parse_driver_type(type, &ad->typename, &ad->typearg);
    ad->channel = channel > 0 ? channel : 0;
    ad->rate = rate;
    ad->N = next_pow2(N);
    ad->data = NULL;
    ad->realtime = 1;
        
    fprintf(stderr, "audio driver: %s (%s)\n", ad->typename, ad->typearg);
    
    if (ad->type == -1)
    {
        free(ad); return NULL;
    }
    
    for (k = ad->type; k >= 0; k--)
    {
        switch (k)
        {
            case RD_AUDIO_JACK:
                fprintf(stderr, "creating jack driver\n");
                ad->data = driver_create_jack(ad, name, &native_rate, &native_n);
                break;
            
            case RD_AUDIO_ALSA:
                fprintf(stderr, "creating ALSA driver\n");
                native_rate = ad->rate;
                ad->data = driver_create_alsa(ad, ad->typearg, &native_rate, ad->N);
                break;
            
            case RD_AUDIO_FILES:
                fprintf(stderr, "creating directory driver\n");
                ad->data = driver_create_files(ad, ad->typearg, &native_rate);
                break;
                
            case RD_AUDIO_FD:
                fprintf(stderr, "creating file descriptor driver\n");
                ad->data = driver_create_fd(ad, ad->typearg, ad->N);
                native_rate = rate;
                break;
            
            default:
                break;
        }
        
        if (ad->data != NULL)
        {
            ad->type = k; break;
        }
    }
    
    if (ad->data == NULL)
    {
        free(ad); return NULL;
    }
        
    if (ad->rate > native_rate)
        ad->rate = native_rate;
        
    #if HAVE_LIBSPEEXDSP==1 || HAVE_LIBFPC==1
    ad->decimate = native_rate / ad->rate;
    ad->rate = native_rate / ad->decimate;
        
    ad->filter_length = 20 * ad->decimate;
    ad->bandwidth = 0.25;
    #else
    ad->decimate = 1;
    ad->rate = native_rate;
    ad->filter_length = 1;
    ad->bandwidth = 0.0;
    
    fprintf(stderr, "*** decimation disabled.  Recompile with libfpc or libspeexdsp\n");
    #endif
    
    fprintf(stderr, "decimation factor = %d, final rate = %d\n", ad->decimate, ad->rate);
    fprintf(stderr, "buffer latency = %d frames\n", latency);
    fprintf(stderr, "buffer history = %d frames\n", history);
    
    rd_dll_init(&ad->dll, ((rd_time_t) N * ONE_SEC_USECS_L) / rate, 0.0001);
    
    ad->sound_buffer = sound_buffer_create(
        ad->N, latency+1, history, slack, ad->rate
    );
    
    if (ad->sound_buffer == NULL)
        return NULL;
    
    ad->decimator = NULL;
    ad->input_buffer = NULL;
    
    int decimator_quality = config_get_value_int(NULL, "resample_quality");
    fprintf(stderr, "resample quality = %d\n", decimator_quality);
    
    ad->decimator = rd_decimator_create(ad->decimate, decimator_quality);
    
    ad->input_buffer = rd_calloc(ad->decimate * ad->N, sizeof(sample_t));
    ad->ix = 0;
    return ad;
}

/**
 * start the driver, feeding a sound buffer into its callback
 */

int rd_audio_driver_start(rd_audio_driver_t * ad)
{
    switch (ad->type)
    {
        case RD_AUDIO_JACK:
            driver_start_jack(ad);
            break;
        
        case RD_AUDIO_ALSA:
            driver_start_alsa(ad);
            break;
        
        case RD_AUDIO_FILES:
            driver_start_files(ad);
            break;
            
        case RD_AUDIO_FD:
            driver_start_fd(ad);
            break;
            
        default:
            break;
    }
    
    return 0;
}

/**
 * stop the driver from running
 */

void rd_audio_driver_stop(rd_audio_driver_t * ad)
{
    switch (ad->type)
    {
        case RD_AUDIO_JACK:
            driver_stop_jack(ad);
            break;
        
        case RD_AUDIO_ALSA:
            driver_stop_alsa(ad);
            break;
        
        case RD_AUDIO_FILES:
            driver_stop_files(ad);
            break;
            
        case RD_AUDIO_FD:
            driver_stop_fd(ad);
            break;
            
        default:
            break;
    }
    
    ad->sound_buffer->eof = 1;
    
    return;
}

/**
 * free the storage associated to an audio driver
 */

void rd_audio_driver_free(rd_audio_driver_t * ad)
{
    rd_audio_driver_stop(ad);
    
    switch (ad->type)
    {
        case RD_AUDIO_JACK:
            driver_free_jack(ad);
            break;
        
        case RD_AUDIO_ALSA:
            driver_free_alsa(ad);
            break;
        
        case RD_AUDIO_FILES:
            driver_free_files(ad);
            break;
            
        case RD_AUDIO_FD:
            driver_free_fd(ad);
            break;
            
        default:
            break;
    }
    
    sound_buffer_destroy(ad->sound_buffer);
    rd_decimator_destroy(ad->decimator);
    
    free(ad->input_buffer);
    free(ad);
    
    return;
}

/*
 * TEST CODE FOLLOWS ---------------------------------------
 */

#ifdef TEST
#include <sndfile.h>

int main(int argc, char * argv[])
{
    int status;
    int rate = 44100;
    int k, n = 0, samples = 80000;
    
    rd_time_t t;
    SNDFILE * fp;
    SF_INFO sfinfo;
    
    if (argc > 1)
        rate = atoi(argv[1]);
    
    rd_audio_driver_t * d = rd_audio_driver_create(RD_AUDIO_PULSE, "test", 0, rate, 1024, 0);
    
    if (d == NULL)
    {
        fprintf(stderr, "failed to create audio driver... \n");
        return 1;
    }
    
    sound_buffer_t * sb = d->sound_buffer;
    sample_t * buf = rd_calloc(sb->K, sizeof(sample_t));
    
    if ((status = sound_buffer_become_reader(sb)) != 0)
        fprintf(stderr, "failed to become sound buffer reader...%s\n",strerror(status));
    
    sfinfo.samplerate = rate; 
    sfinfo.channels = 1;
    sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
    
    fp = sf_open("audio_driver_test.wav", SFM_WRITE, &sfinfo);
    
    fprintf(stderr, "about to start driver...\n");
    
    rd_audio_driver_start(d);
    
    while (n < samples)
    {    
        sound_buffer_wait(sb);
        
        //fprintf(stderr, "get some samples!\n");
        
        while (sound_buffer_read_space(sb) > 0)
        {
            sound_buffer_read(sb, buf, &t);
        }
    }
    
    sf_close(fp);
    
    rd_audio_driver_stop(d);
    //rd_audio_driver_free(d);
    free(buf);
}

#endif
