#ifndef RD_SOUND_BUFFER_H
#define RD_SOUND_BUFFER_H

#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#include "detect.h"
#include "rd_time.h"

typedef struct sound_buffer {
    struct segment * segments;
    int N;
    int K;
    unsigned int smask;
    unsigned int dmask;
    unsigned int rp;
    unsigned int wp;
    sample_t * data;
    unsigned int latency;
    unsigned int history;
    int rate;
    int ready;
    int count;
    int eof;
    pthread_mutex_t rcond_lock;
    pthread_mutex_t wcond_lock;
    pthread_cond_t rcond;
    pthread_cond_t wcond;
} sound_buffer_t;

/**
 * make a new sound buffer and allocate storage
 */

sound_buffer_t * sound_buffer_create(
    int K, int latency, int history, int free, int rate
);

/**
 * clean up a sound buffer
 */

void sound_buffer_destroy(sound_buffer_t * sb);

/**
 * check how many buffers are available for writing
 */

int sound_buffer_write_space(sound_buffer_t * sb);

/**
 * ditto for reading.
 */

int sound_buffer_read_space(sound_buffer_t * sb);

/**
 * write a single buffer to the head of the sample buffer and 
 * increment the write pointer
 */

int sound_buffer_write(sound_buffer_t * sb, sample_t * in, rd_time_t time);

sample_t * sound_buffer_get_write_pointer(sound_buffer_t * sb);
void sound_buffer_write_userdata(sound_buffer_t * sb, void * userdata);

/**
 * move to next sub-buffer
 */
 
int sound_buffer_increment_read_pointer(sound_buffer_t * sb);
int sound_buffer_increment_write_pointer(sound_buffer_t * sb, rd_time_t time);


/**
 * return a pointer to the current sub-buffer up for reading, without 
 * incrementing the read pointer.  Also store the timestamp
 */

sample_t * sound_buffer_peek(sound_buffer_t * sb, rd_time_t * time);
void * sound_buffer_read_userdata(sound_buffer_t * sb);


/**
 * Return a copy of the current sub-buffer up for reading.  Also store the timestamp.
 */
 
int sound_buffer_read(sound_buffer_t * sb, sample_t * out, rd_time_t * time);

/**
 * read a segment of buffer out to an allocated array.  Returns the number of samples
 * in the array.  This function will correctly handle wrapping around the buffer
 * edges, provided that ix2 - ix1 is less than the number of samples in the buffer.
 */

int sound_buffer_read_ix(sound_buffer_t * sb, sample_t ** out, int ix1, int ix2);

int sound_buffer_read_time(
    sound_buffer_t * b, sample_t ** out, rd_time_t time, rd_time_t dur, int * corrupt);

void print_sound_buffer(sound_buffer_t * sb);

/**
 * condition signalling functions.
 */
 
int sound_buffer_read_wait(sound_buffer_t * sb);
int sound_buffer_read_signal(sound_buffer_t * sb);

int sound_buffer_write_wait(sound_buffer_t * sb);
int sound_buffer_write_signal(sound_buffer_t * sb);

#endif
