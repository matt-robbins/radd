/**\file
 * library containing convenience routines for all run_detect detectors.
 * I'm not sure if this is really necessary, but it seems like getting 
 * parameters from configurations files is a coomon enough task that it's
 * worth pulling out into a common library.
 */
 
#include "libdetect.h" 
#include "rd_log.h"
#include "rd_memory.h"

FILE * config_file_open(char * config_file)
{
    FILE * fp = fopen(config_file, "r");
    return fp;
}

void config_file_close(FILE * fp)
{
    fclose(fp);
}
 
int get_parameter_value_str(char * out, FILE * fp, char * p_name)
{
    char line[LINE_MAX], name[64], value[64];
    int n;
    
    fseek(fp, 0, SEEK_SET);
    
    while (fgets(line, LINE_MAX, fp) != NULL)
    {
        if (line[0] == '#')
            continue;
        
        n = sscanf(line, "%s %s", name, value);
                
        if (n < 2 || (strcmp(p_name, name) != 0))
            continue;
                
        memcpy(out, value, strlen(value) + 1);
        return 0;
    }
    
    return -1;
}

int get_parameter_value_float(float * out, FILE * fp, char * p_name)
{
    char value[32];
    
    if (get_parameter_value_str(value, fp, p_name) != 0)
        return -1;
    
    *out = atof(value);
    return 0;
}

int get_parameter_value_int(int * out, FILE * fp, char * p_name)
{
    char value[32];
        
    if (get_parameter_value_str(value, fp, p_name) != 0)
        return -1;
    
    *out = atoi(value);

    return 0;
}

int get_parameter_value_short(short int * out, FILE * fp, char * p_name)
{
    char value[32];
        
    if (get_parameter_value_str(value, fp, p_name) != 0)
        return -1;
    
    *out = (short int) atoi(value);

    return 0;
}

int get_parameter_value_frac(float * out, FILE * fp, char * p_name)
{    
    return get_parameter_value_float(out, fp, p_name);
}

