import numpy as np
import gc

radd_rate = 48000
radd_N = 1024
radd_history = 0
radd_latency = 0

def init(name):
    n = 100
    stuff = {}
    stuff['sigbuf'] = np.zeros(n)
    stuff['rate'] = 48000
    
    chirp = np.zeros(n)
    
    for i in range(chirp.size):
        chirp[i] = np.sin((np.pi/2)*i*i/chirp.size)
        
    chirp = chirp - np.mean(chirp)
    chirp = chirp / np.linalg.norm(chirp)
    
    stuff['chirp'] = chirp
    stuff['corr_tail'] = np.zeros(n-1)
    stuff['av_tail'] = np.zeros(n-1)
    stuff['norm_tail'] = np.zeros(n-1)
        
    return stuff
    
def moving_sum(a, n=3) :
    apad = np.concatenate((a,np.zeros(n-1)))
    ret = np.cumsum(apad, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret
    
def classify(data, rate, param):
    
    corr = np.correlate(data, param['chirp'], 'full')
    n = 100
    
    data_sq = np.multiply(data, data)
    dav = moving_sum(data,n=n)/n
    dnm = moving_sum(data_sq,n=n)
    
    norm_factor = np.sqrt(dnm - n * np.multiply(dav, dav))
    
    maxcorr = np.max(corr)
    ix = np.argmax(corr)
    
    maxnorm = np.max(norm_factor)
    nix = np.argmax(norm_factor)
    return (maxcorr / maxnorm, "hi", "chirp")
    
    
def detect(data, param, date): 
    ix = 0
    trig = False 
    maxcorr = 0
    
    n = 100
    nm1 = n - 1
    data_sq = np.multiply(data, data)
    
    # compute moving average and moving squared sum
    dav = moving_sum(data,n=n)/n
    dnm = moving_sum(data_sq,n=n)
    
    #compute correlation
    corr = np.correlate(data, param['chirp'], 'full')

    # store and retrieve tails
    dav[:nm1] = dav[:nm1] + param['av_tail']
    param['av_tail'] = dav[-nm1:]
    
    dnm[:nm1] = dnm[:nm1] + param['norm_tail']
    param['norm_tail'] = dnm[-nm1:]
    
    corr[:nm1] = corr[:nm1] + param['corr_tail']
    param['corr_tail'] = corr[-nm1:]
    
    #compute normalization vector (same size as corr)
    norm_factor = np.sqrt(dnm - n * np.multiply(dav, dav))
    
    maxcorr = np.max(corr)
    ix = np.argmax(corr)
    
    maxnorm = np.max(norm_factor)
    nix = np.argmax(norm_factor)
        
    #~ print maxcorr,ix, maxnorm, nix, corr.size, dav.size
        
    if maxcorr > 0 and (maxcorr / maxnorm > 0.9):
        return ((ix - n,n,0.0,param['rate']/2.0,10.0*maxcorr/maxnorm,'tick'),) 
    else:
        return ()

