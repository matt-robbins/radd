# README #

RADd (Realtime Acoustic Detection daemon) is a real-time detection engine intended for use in embedded linux systems.  It uses the JACK or pulse audio server (via ALSA adapter) to pass data through a set of detector plugins, and stores the resulting data in an sqlite database along with sound clips stored as audio files.  It can also run "offline" on data from an audio file or directory of files.

RADd includes a set of Python APIs that use numpy to provide a friendly scientific computing environment.  By default, it will be built with Python support, but you can disable Python if your system does not support it.

_Caveat: when using Python plugins for both the detector and classifier, Python requires the use of a Global Interpreter Lock, or GIL.  This means that we can't take advantage of RADd's multi-threaded architecture.  Typically, the detector is a simpler algorithm and can be written in C.  This will allow the Python classifier to run in a separate thread._

### How do I get set up? ###

You'll need a linux machine, with a working build system, (e.g. sudo apt-get install build-essential)
RADd requires several other libraries to build. In ubuntu packages you'll need at least the following
*libsqlite3-dev*, *libsndfile1-dev*, and *libspeexdsp-dev*

for realtime work, you'll also need:
*libjack-dev* and *libasound2-dev*

To use Python:
*python-dev*, and *python-numpy*, or if you prefer, you can use numpy installed via pip or easy_install.

finally, the packages provide some extra functionality:
*libmxml-dev* and *liblockfile1-dev*

To install everything necessary on an Ubuntu (or Debian-based) system

```
sudo apt install libsqlite3-dev libsndfile1-dev libspeexdsp-dev libjack-dev libasound2-dev libmxml-dev liblockfile1-dev
```

RADd can also use libfpc, a DSP library that can be built to compute exclusively with fixed-point arithmetic.  This allows you to run more efficiently on low-power embedded systems without a floating point unit.  As of now, you have to build libfpc from source.

To build, it should be sufficient to use the standard linux commands:
```shell
$ ./autogen.sh && ./configure && make && sudo make install
```
If you need to disable Python:
```shell
$ ./autogen.sh && ./configure --disable-python && make && sudo make install
```

### How do I use it? ###

The best way to get started is probably to use the tick_check.so plugin that gets built along with RADd, along with the rd_jack_ticker executable.  This requires that we build RADd with JACK support, but allows us to accurately measure timing precision, and get something up and running right away.

Let's look at the RADd configuration file that's installed by default in ```/usr/local/etc/radd.conf```:

```
detector_path /usr/local/lib/radd/detectors/
classifier_path /usr/local/lib/radd/classifiers/
store_path /tmp/save/

detector tick_check

# audio clip padding in seconds
clip_pad 0.2
pre:clip_pad 0.1
#post:clip_pad 0.1
clip_format wav

classify_padded 1

report_thresh 11.0
clip_thresh 1.0
store_thresh 1.0
event_buffer_size 10

n_clips -1
n_events -1
resample_quality 5
driver_channel 0
```

We can then point RADd to this configuration file, and run it in the foreground to see what's going on.  You'll need to start JACK in a separate terminal, preferably running at 48000 Hz, then:

```
$ radd -d -a jack -f /usr/local/etc/radd.conf
```

You should see something like this:

```
creating detector "tick_check" ...
creating classifier "tick_check" ...
couldn't open classifier "tick_check": /usr/local/lib/radd_classifiers/tick_check.so: cannot open shared object file: No such file or directory
couldn't create classifier "tick_check".
tick_check couldn't chdir!
in dummy parameter_initialize()
audio driver: jack ((null))
creating jack driver
decimation factor = 1, final rate = 48000
buffer latency = 20 frames
buffer history = 11 frames
resample quality = 5
event_buffer_size: 10
detector initialized and running.
```

Then, if we run ```rd_jack_ticker tick_check:input```, we should start seeing output like this:
```
score from detector = 10.000000
classified score = 10.000000
writing event, with time = 1474475794.999382 seconds, score 10.000000, (null)
writing file tick_check_0000000008.wav
score from detector = 10.000000
classified score = 10.000000
writing event, with time = 1474475795.999798 seconds, score 10.000000, (null)
writing file tick_check_0000000009.wav
```

Note the "with time = " is measured in seconds since midnight Jan 1, 1970, UTC, or standard UNIX time.  Also, note that the fractional part of the second is very close to the second boundary.  This is because rd_jack_ticker tries to estimate exactly which samples correspond to second boundaries, and outputs an FM chirp starting on precisely those samples, almost exactly once per second.  Since we are directly connecting the output with our input, there is zero extra latency, so the detector detects events precisely on the second boundaries.  

If you have a means of looping back the audio output to the input, e.g. via a cable, you can also do the same experiment, except running ```rd_jack_ticker system:playback_1 system:playback_2```.  This routes the ticker output to the hardware outputs, and RADd automatically connects to hardware inputs.  You should see the event times move slightly back in time relative to the second boundaries.  In this way, we are effectively measuring the hardware latency.

We can also verify this using the ```rd_record``` utility.  rd_record is designed to record, with precise time information, everything that our detector sees.

```
$ rd_record -d -a jack -p /tmp -t 10 -r 48000
$ rd_jack_ticker record:input
```

This will record a sequence of 10-second files in the directory ```/tmp/record/``` whose timestamps lie on precise 10-seconds boundaries.  Opening one of the files in an audio editor will show "ticks" lying exactly on the second boundaries.

### running in offline mode ###

We can then pass one or all of the files we recorded with rd_record back through the detector.  Since we don't particularly care about the audio events we're detecting, we can just delete the whole directory.

```
$ rm /tmp/save/*
$ radd -d -a files:/tmp/record/.flac 

```

Again, we should see in the terminal output that the event times are very close to the second boundaries.

We can explore the output directory as follows:

```
$ ls /tmp/save
dump.db                    tick_check_0000000010.wav  tick_check_0000000020.wav
tick_check_0000000001.wav  tick_check_0000000011.wav  tick_check_0000000021.wav
tick_check_0000000002.wav  tick_check_0000000012.wav  tick_check_0000000022.wav
tick_check_0000000003.wav  tick_check_0000000013.wav  tick_check_0000000023.wav
tick_check_0000000004.wav  tick_check_0000000014.wav  tick_check_0000000024.wav
tick_check_0000000005.wav  tick_check_0000000015.wav  tick_check_0000000025.wav
tick_check_0000000006.wav  tick_check_0000000016.wav  tick_check_0000000026.wav
tick_check_0000000007.wav  tick_check_0000000017.wav  tick_check_0000000027.wav
tick_check_0000000008.wav  tick_check_0000000018.wav  tick_check_0000000028.wav
tick_check_0000000009.wav  tick_check_0000000019.wav

```

If you are familiar with SQL, you can inspect the output database /tmp/save/dump.db by running ```sqlite3 /tmp/save/dump.db "SELECT * FROM events;"```.

### Checking Python ###

If you compiled RADd with Python support, and want to try using the python detector and classifier that come with RADd, you can either edit the configuration file, and replace ```tick_check``` with ```py_tick_check```, or override the detector name on the command line, like so:
```
radd -d -a jack -f /usr/local/etc/radd.conf py_tick_check
```

# The APIs #

You probably built RADd because you want to run your own detector/classifier algorithms, and you're probably looking for something other than quadratic chirps.  To do that, you will need to compile your own plugin, or write your own Python modules.  In either case, RADd looks for a specific set of functions and objects in your code that it will call at the proper time.  For the most part, the classifier and detector APIs are identical, except that detectors implement `detect()`, and classifiers implement `classify()`

## The C API ##

RADd is written in C, so the core API is defined that way as well. 

First, we must define an "event". A detector really should only care about relative time offset to the current block of samples. The basic five elements of an event are time/duration (t_in, t_dt), and frequency/bandwidth (f_lo, f_df), and some kind of score. In addition, it is helpful for debugging, or for labeling, etc., to store some additional data along with each event. 

Here is the definition of an event as it appears in the file detect.h:

```C
typedef int offset_t; // in samples
typedef float freq_t; // in Hz
typedef float score_t; // between 0.0

typedef struct {
    offset_t t_in;
    offset_t t_dt;
    freq_t f_lo;
    freq_t f_df;
    score_t score;
    char label[RD_MAX_LABEL];
    void * data;
} event_t;
```

`t_in` and `t_dt` are both defined to have units of samples, rather than seconds, and offset_t is an integer type

`f_lo` and `f_df` are defined in Hz as float.

`score` is also a 32-bit float, expected to be between 0.0 and 1.0.

`label` is a string.  A detector can choose to specify a label.  If not, it can leave this field alone.  The label will be used in the database row for the event.

# Functions

## parameter_initialize
```C
void * parameter_initialize(char * config_file);
```

#### Description
Read parameters from a file, or set defaults.

#### Input
`config_file` – a string containing the path to a configuration file.

#### Output
pointer to an allocated object (typically a struct), or NULL on failure.

## sound_setup
```C
sound_setup * setup(void * parameters);
```

#### Description
This is a request to the sound engine to receive these settings.  The sound engine requires buffer sizes `N` to be powers of two, and sample rates to be integer divisions of the hardware sample rate.  A requested buffer size will be increased to the nearest power of 2, and the sample rate increased to the nearest integer factor of the hardware sample rate. It is the responsibility of the developer to verify that their settings were honored when the state_initialize() callback is called.

#### Input
`parameters` – a pointer to the parameters object returned by parameter_initialize

#### Output
pointer to an allocated or static sound_setup object, defining desired sample rate and block size, and two other parameters that control the relative time span within which the detector intends to return events.  `buffer_history` is the number of segments of size `N` behind the current segment that RADd should keep to make sure that it can read valid audio data for returned events.  `buffer_latency` is the number of segments of size `N` _ahead_ of the current segment that RADd should make sure are valid. For instance, if the detector can returns events that began as long as 5000 samples ago, and `N` is 1024, `buffer_history` should be set to 5.

```C
typedef struct {
  int rate;
  int N;
  int buffer_history;
  int buffer_latency;
} sound_setup_t;
```

## state_initialize
```C
void * state_initialize(void * parameters, sound_setup_t * setup);
```

#### Description
state_initialize() should for the most part allocate all memory to be used by the detector, and pre-compute any necessary intermediate results (analysis window functions, any synthesized matching signals, etc.)

#### Input
`parameters` – a pointer to the parameters object returned by parameter_initialize

`setup` - a pointer to the `sound_setup_t` object returned by the `setup` function.  _Note: the values of the setup object may be different from those requested in `setup()`, and the values passed here are the true values that you should use when allocating memory, etc._ 

#### Output
pointer to an allocated object (typically a struct), or NULL on failure.

## detect
```C
int detect(event_t * events, sample_t * buf, int nbuf, void * parameters, void * state, sound_setup * setup, struct timeval time);
typedef int32_t sample_t;
```

#### Description
detect() is the main detection processing callback. detect() will be called in sequence on each new block of data arriving from the audio input device.

#### Input
`buf` – a block of data, typically N samples long, as directed by sound_setup.

`nbuf` - the number of samples per block. This should be the same as N.

`parameters` – a pointer to the parameters object returned by parameter_initialize

`state` – a pointer to the object returned by state_initialize

`setup` – a sound_setup object containing the actual values used by the audio engine.

`time` – the approximate time (as reported by the operating system) when the first sample of this block was received at the audio hardware input. This is purely for reference, the developer need only specify event times in samples relative to the first sample of buf, but the `time` parameter allows the detector to make use of absolute time in the decision process.

#### Output
`events` – this is an “output” parameter. Events is a pointer to an array of objects of type event_t. The time/frequency/score/label values are written to the k'th event by setting the values of events[k].

The return value of detect() is the number of events being returned in the event array. The developer must not write more than RD_MAX_EVENTS into the event array. Note that this design allows the developer not to have to allocate any memory for events, except for any data returned with each event.

## classify
```C
float classify(sample_t * buf, int nbuf, int rate, void * parameters, void * state,
    char ** data, char *** labels);
```
#### Description
`classify()` is the classification method.  When events are detected, RADd stores the audio data for the event and passes it to `classify()`, which then assigns a score and set of labels.  Classifiers can optionally return debug data as a string.

# Input
`buf` – a block of data.

`nbuf` - the number of samples in `buf`.  This will depend on the detector, and padding settings.

`rate` - the sample rate of the data.

`parameters` - a pointer to the parameter object returned by `parameter_initialize()`

`state` - a pointer to the state object returned by `state_initialize()`

#### Output

`score` - this is the main `float` value returned by `classify()`.  It should be some measure of how well the event matches some criterion.  It will overwrite the detector score when stored to the output database.

`data` - a pointer to a string.  If the classifier wants to return debug data, it should allocate a new string at `*data`.

`labels` - a pointer to a zero-terminated set of string pointers.  `classify()` should allocate a `char **` to `*labels` one larger than the number of labels to be returned.  The last label should be a `NULL` pointer.

## cleanup
```C
void cleanup(void * parameters, void * state);
```

#### Description
cleanup() allows the developer to free memory allocated during parameter_initialize() and state_initialize()

#### Input
`parameters` – a pointer to the parameters object returned by parameter_initialize

`state` – a pointer to the object returned by state_initialize

# The Python API

in Python, things get simpler.  There is only one `init` function which is analogous to `state_initialize()` in the C API.  The `setup()` function is also not required.  Instead, your module must define top-level integers `radd_rate`, `radd_N`, `radd_history` and `radd_latency`, equivalent to the `sound_setup_t` values.  They will be modified by RADd after initializing the audio driver.

## Init
```Python
def init(name):
```
#### description
`init()` does everything `state_initialize()` should do.  Allocate whatever objects, models, etc..
#### Inputs
`name` - the name of the detector/classifier.  This is the same as the name of the module, so is not really necessary.
#### Outputs
`state` - this function should return an object containing everything the `detect()` or `classify()` functions might need.  This can be any Python object.

## Detect
```Python
def detect(data, state, timestamp_dt):
```

#### Description
`detect` is called each time a new segment of audio arrives from the audio driver.
#### Inputs
`data` - a 1-D numpy array of audio sample data.

`state` - the state object returned from `init()`

`timestamp_dt` - a Python datetime object specifying the exact time the first sample in `data` was recorded.

#### Ouputs
`events` - `detect()` must output a tuple of events.  Each event must be a tuple of values that looks like this `(t_in, t_dt, f_lo, f_df, score, label)`
They should have the same type as the members of the `event_t` struct listed in the C API.

## classify
```Python
def classify(data, rate, state):
```
#### Description
`classify()` takes the audio data from an event, and returns a score and (set of) label(s).
#### Inputs
`data` - a 1-D numpy array of audio data

`rate` - the sample rate of the data

`state` - the state object returned by `init()`

### Outputs
`classify()` should return a 3-tuple, `(score, data, label)`

`score` - a final score determined by the classifier. This is used by RADd to prune all by the highest scoring events, and is written to the database, depending on RADd's configured score threshold.

`data` - a string object representing any debugging data the developer wants to save.  May be a zero-length string, or None.

`label` - the class label.  
